require('ts-node').register({
    project: 'test/tsconfig.json'
});

const tsConfig = require("./tsconfig.json");
const baseUrl = "./test"; // Either absolute or relative path. If relative it's resolved to current working directory.

require("tsconfig-paths").register({
    baseUrl,
    paths: tsConfig.compilerOptions.paths
});
