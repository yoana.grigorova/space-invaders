/**
 * Created by Georgi Dimov on 29.8.2019 г..
 */
import { EventEmitter } from '@egt/egt-library/dist/events/EventEmitter'
import { Events } from './Events'

export class MainModel extends EventEmitter {
    protected _currentScore
    protected _livesLeft = 5;

    constructor() {
        super()
        this.reset()
    }

    get currentScore(): number {
        return this._currentScore
    }

    get livesLeft(): number {
        return this._livesLeft
    }

    reset() {
        this._currentScore = 0
        this._livesLeft = 3
    }

    updateLives(isSuccessful: boolean) {
        if (this._livesLeft > 0 && isSuccessful) {
            this._livesLeft--
            this.fire(Events.UPDATE_HUD)
        }
    }

    addHitAttempt(isSuccessful: boolean) {
        if (isSuccessful) {
            this._currentScore += 20;
            this.fire(Events.UPDATE_HUD);
        }
    }

}