//********** TWEENMAX HACK ************//

import Resource = PIXI.loaders.Resource

declare var gsap: any
declare var TimelineMax: any
declare var TweenMax: any
declare var TweenLite: any
declare var Back: any
declare var Quad: any
declare var Linear: any
declare var Elastic: any
declare var Cubic: any
declare var Power2: any

try {
    window['gsap'] = TweenMax
    TweenMax.TimelineMax = TimelineMax
    TweenMax.TweenMax = TweenMax
    TweenMax.TweenLite = TweenLite
    TweenMax.Back = Back
    TweenMax.Quad = Quad
    TweenMax.Linear = Linear
    TweenMax.Elastic = Elastic
    TweenMax.Cubic = Cubic
    TweenMax.Power2 = Power2
} catch (e) {
    console.log('ERROR:', e)
}

//********** TWEENMAX HACK ************//

import { LayerManager } from '@egt/egt-library/dist/LayerManager'
import { MobileView } from '@egt/egt-library/dist/MobileView'
import * as log from '@egt/egt-library/dist/utils/log'
import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import * as Bottle from 'bottlejs'
import { HUDController } from './hud/HUDController'
import { HUDModel } from './hud/HUDModel'
import { HUDView } from './hud/HUDView'
import { MainModel } from './MainModel'
import { Resources } from './Resources'
import {ShipModel} from './spaceship/ShipModel';
import {ShipController} from './spaceship/ShipController';
import {ShipView} from './spaceship/ShipView';
import {BulletModel} from './bullet/BulletModel';
import {BulletController} from './bullet/BulletController';
import {BulletView} from './bullet/BulletView';
import {EnemyModel} from './enemy/EnemyModel';
import {EnemyController} from './enemy/EnemyController';
import {EnemyView} from './enemy/EnemyView';
import {ShieldsModel} from './shields/ShieldsModel';
import {ShieldsController} from './shields/ShieldsController';
import {ShieldsView} from './shields/ShieldsView';

declare global {
    export interface Window {
        bottle: Bottle
        egtGlobal: any
        com: any
    }
}

window.bottle = new Bottle('game')
const bottle = window.bottle

declare var egtGlobal: any

if (typeof egtGlobal === 'undefined') {
    window.egtGlobal = {}
}

@log.decorator
export class Main extends MobileView {
    log
    warn

    constructor() {
        super()

        this.createGroups()

        this.setupBottle()

        this.createControllers()

        bottle.container.Resources.load()
    }

    public createGroups() {
        let groups = [
            'ui',
            'hud',
            'spaceship',
            'bullet',
            'enemies',
            'shields'
        ]

        LayerManager.createGroups(...groups)
    }

    protected setupBottle() {
        bottle.factory('HUDController', (container: Bottle.IContainer) => {
            return new HUDController(container.HUDView, container.HUDModel)
        })

        bottle.factory('HUDView', (container: Bottle.IContainer) => {
            return new HUDView()
        })

        bottle.factory('HUDModel', (container: Bottle.IContainer) => {
            return new HUDModel(container.MainModel)
        })

        bottle.factory('Resources', (container: Bottle.IContainer) => {
            return new Resources()
        })

        bottle.factory('MainModel', (container: Bottle.IContainer) => {
            return new MainModel()
        })

        bottle.factory('ShipController', (container: Bottle.IContainer) => {
            return new ShipController(container.ShipView, container.ShipModel);
        })

        bottle.factory('ShipView', (container: Bottle.IContainer) => {
            return new ShipView()
        })

        bottle.factory('ShipModel', (container: Bottle.IContainer) => {
            return new ShipModel(container.MainModel);
        })

        bottle.factory('BulletController', (container: Bottle.IContainer) => {
            return new BulletController(container.BulletView, container.BulletModel);
        })

        bottle.factory('BulletModel', (container: Bottle.IContainer) => {
            return new BulletModel(container.MainModel);
        })

        bottle.factory('EnemyController', (container: Bottle.IContainer) => {
            return new EnemyController(container.EnemyView, container.EnemyModel);
        })

        bottle.factory('EnemyView', (container: Bottle.IContainer) => {
            return new EnemyView()
        })

        bottle.factory('EnemyModel', (container: Bottle.IContainer) => {
            return new EnemyModel(container.MainModel);
        })

        bottle.factory('ShieldsController', (container: Bottle.IContainer) => {
            return new ShieldsController(container.ShieldsView, container.ShieldsModel);
        })

        bottle.factory('ShieldsView', (container: Bottle.IContainer) => {
            return new ShieldsView()
        })

        bottle.factory('ShieldsModel', (container: Bottle.IContainer) => {
            return new ShieldsModel(container.MainModel);
        })
    }

    protected createControllers() {
        bottle.container.HUDController
        bottle.container.ShipController
        bottle.container.BulletController
        bottle.container.EnemyController
        bottle.container.ShieldsController
    }

    protected onRotationChange(): void {
    }
}
