import { Main } from '../Main'
import { MainModel } from '../MainModel'

export class BulletModel {
    protected _mainModel: MainModel

    constructor(mainModel: MainModel) {
        this._mainModel = mainModel
    } 
}