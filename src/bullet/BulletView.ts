import { LayerManager, MobileView } from '@egt/egt-library'
const WIDTH = 1280, HEIGHT = 720;
export class BulletView extends PIXI.Sprite {

    protected _bulletVelocity: number = 15;
    protected _renderer: PIXI.SystemRenderer;
    public onStage: Boolean;

    constructor(x, y) {
        super(PIXI.Texture.from("../assets/bullet.png"));

        this.width = 70;
        this.height = 70;
        this.anchor.set(0.5);
        this.y = y;
        this.x = x;
        this._bulletVelocity = 10;
        this.onStage = true;

        LayerManager.stage.addChild(this)
        LayerManager.addToGroup(this, 'bullet');

        // PIXI.ticker.shared.add(this.shoot, this);
    }

    shoot(){
        this.y -= this._bulletVelocity;

        if(this.y < 0){
            this.remove();
        }
    }

    enemyShoot(){
        this.y += this._bulletVelocity;

        if(this.y > HEIGHT){
            this.remove();
        }
    }

    remove(){
        LayerManager.stage.removeChild(this);
        PIXI.ticker.shared.remove(this.shoot, this);
        this.onStage = false;
    }
}