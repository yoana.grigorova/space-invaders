import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import { Events } from '../Events'
import { BulletModel } from './BulletModel'
import { BulletView } from './BulletView'

export class BulletController extends EventHandler {
    protected _view: BulletView
    protected _model: BulletModel

    constructor(view: BulletView, model: BulletModel) {
        super()

        this._view = view
        this._model = model
    }

    protected [Events.SHOOT](memo) {
        let bullet = new BulletView(memo.x, memo.y);
        PIXI.ticker.shared.add(() => {
            bullet.shoot();
            if(bullet.onStage){
                this.fire(Events.HIT_CHECK, bullet);
                this.fire(Events.SHIELD_HIT_CHECK, bullet);
            }
        }, bullet);
    } 

    protected [Events.ENEMY_SHOOT](memo) {
        let bullet = new BulletView(memo.x, memo.y);
        PIXI.ticker.shared.add(() => {
            bullet.enemyShoot();
            if(bullet.onStage){
                this.fire(Events.SHIP_HIT_CHECK, bullet);
                this.fire(Events.SHIELD_HIT_CHECK, bullet);
            }
        }, bullet);
    } 
}