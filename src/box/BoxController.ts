/**
 * Created by Georgi Dimov on 29.8.2019 г..
 */
import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import { Events } from '../Events'
import { BoxModel } from './BoxModel'
import { BoxView } from './BoxView'

export class BoxController extends EventHandler {
    protected _view: BoxView
    protected _model: BoxModel

    constructor(view: BoxView, model: BoxModel) {
        super()

        this._view = view
        this._model = model
    }

    protected [Events.RESOURCES_LOADED]() {
        this._view.init()

        this._view.reset(this._model.velocity, this._model.boxAreaSize)
    }

    protected [Events.GO_TO_NEXT_LEVEL]() {
        this._view.reset(this._model.velocity, this._model.boxAreaSize)
    }

    protected [Events.GAME_OVER]() {
        this._view.gameOver()
    }

    protected [Events.GAME_BUTTON_CLICKED]() {
        this._model.addHitAttempt(this._view.hitTest())
    }

    protected [Events.RESET_BUTTON_CLICKED]() {
        this._view.reset(this._model.velocity, this._model.boxAreaSize)
    }
}