/**
 * Created by Georgi Dimov on 29.8.2019 г..
 */
import { Main } from '../Main'
import { MainModel } from '../MainModel'

export class BoxModel {
    protected _mainModel: MainModel

    constructor(mainModel: MainModel) {
        this._mainModel = mainModel
    }

    get velocity(): number {
        return 3 + this._mainModel.currentScore * 2
    }

    get boxAreaSize(): PIXI.Point {
        const size = 100 - this._mainModel.currentScore * 4
        return new PIXI.Point(size, size)
    }

    addHitAttempt(isSuccessful: boolean) {
        this._mainModel.addHitAttempt(isSuccessful)
    }
}