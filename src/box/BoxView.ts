import { LayerManager, MobileView } from '@egt/egt-library'

/**
 * Created by Georgi Dimov on 29.8.2019 г..
 */

export class BoxView extends PIXI.Container {

    protected _box: PIXI.Graphics
    protected _boxArea: PIXI.Graphics
    protected _boxVelocity: number = 0
    protected _boxAreaSize: PIXI.Point
    protected _renderer: PIXI.SystemRenderer

    public init() {
        this._renderer = (window as any).app.renderer

        this._box = new PIXI.Graphics()
        this._box.beginFill(0xffff00)
        this._box.drawRect(0, 0, 50, 50)
        this._box.endFill()

        this._box.y = 150

        this._boxArea = new PIXI.Graphics()

        this.addChild(this._box, this._boxArea)

        LayerManager.stage.addChild(this)
        LayerManager.addToGroup(this, 'spaceship')

        PIXI.ticker.shared.add(this.onTick, this)
    }

    reset(boxVelocity: number, boxAreaSize: PIXI.Point) {
        this._box.x = 0
        this._boxVelocity = boxVelocity
        this._boxAreaSize = boxAreaSize
        this._boxArea.visible = true

        this._boxArea.clear()
        this._boxArea.lineStyle(4, 0x0000ff, 1, 1)
        this._boxArea.drawRect(0, 0, boxAreaSize.x, boxAreaSize.y)

        this._boxArea.x = (this._renderer.width - this._boxArea.width) / 2
        this._boxArea.y = this._box.y - (boxAreaSize.y - this._box.height) / 2
    }

    hitTest(): boolean {
        return this._box.x >= this._boxArea.x && this._box.x + this._box.width <= this._boxArea.x + this._boxAreaSize.x
    }

    gameOver() {
        this._boxVelocity = 0
        this._boxArea.visible = false
    }

    protected onTick() {
        if (this._boxVelocity != 0) {
            this._box.x += this._boxVelocity

            if (this._box.x <= 0 || this._box.x + this._box.width >= this._renderer.width) {
                this._boxVelocity *= -1
            }
        }
    }
}