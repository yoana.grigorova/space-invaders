import { LayerManager } from '@egt/egt-library'
import TimelineMax = gsap.TimelineMax

export class HUDView extends PIXI.Container {
    protected _livesLeft: PIXI.Text
    protected _score: PIXI.Text

    init() {
        this._livesLeft = new PIXI.Text("", {
            fill: 0xffffff,
            fontSize: 20
        })

        this._score = new PIXI.Text("", {
            fill: 0xffffff,
            fontSize: 20
        })

        this._livesLeft.position.set(200, 30)
        this._score.position.set(500, 30)

        this.addChild(this._livesLeft, this._score)

        LayerManager.stage.addChild(this)
        LayerManager.addToGroup(this, 'hud')
    }

    set currentScore(value: number) {
        console.log(LayerManager);
        this._score.text = "Score: " + value.toString();
    }

    set livesLeft(value: number) {
        this._livesLeft.text = "Lives: " + value.toString();
    }

    gameOver(callback){
        let bg = LayerManager.stage.children.shift()
        let children = [...LayerManager.stage.children]
        children.forEach(child => LayerManager.stage.removeChild(child));
        // LayerManager.groups.lenght = 0;
        LayerManager.stage.addChild(bg);
        console.log(LayerManager);
        let tl = new TimelineMax();
        tl.set("#result", {text: "GAME OVER"})
        .fromTo("#result", 1,{
            css:{color:"white"},
            opacity: 0,
            scale:0,
        }, {
            css:{fontSize: 48, fontWeight: "bold"},
            opacity: 1,
            scale: 5
        }).to("#result", 2, {
            css:{fontSize: 0, fontWeight: "bold"},
            onComplete: () => {
                return callback();
            }
        }, "+=1.5")
    }
    
}