/**
 * Created by Georgi Dimov on 29.8.2019 г..
 */
import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import { Events } from '../Events'
import { HUDModel } from './HUDModel'
import { HUDView } from './HUDView'
import TimelineMax = gsap.TimelineMax

export class HUDController extends EventHandler {
    protected _view: HUDView
    protected _model: HUDModel

    constructor(view: HUDView, model: HUDModel) {
        super()

        this._view = view
        this._model = model
    }

    protected [Events.RESOURCES_LOADED]() {
        this._view.init()

        this.update()
    }

    protected [Events.UPDATE_HUD]() {
        this.update()
        if(this._model.livesLeft === 0){
            this.fire(Events.GAME_OVER);
        }

    }

    protected [Events.GAME_OVER]() {
        this._view.gameOver(()=>{
            this.fire(Events.LOAD_GAME);
            this._model.restart();

        });
    }

    protected [Events.RESET_BUTTON_CLICKED]() {
        this.update()
    }

    protected update() {
        this._view.currentScore = this._model.currentScore
        this._view.livesLeft = this._model.livesLeft
    }

    protected [Events.LOAD_GAME]() {
        let start = new TimelineMax();

        start.fromTo("#start", 0.5, {
            scale: 0,
            opacity: 0,
        }, {
                scale: 1,
                opacity: 0.7,
                onComplete: () => {
                    document.getElementById("start").addEventListener("mouseover", zoomIn);
                    document.getElementById("start").addEventListener("mouseout", zoomOut);
                }
            });

        function zoomIn() {
            start.to("#start", 0.5, {
                scale: 1.5,
                opacity: 1
            })
        }

        function zoomOut() {
            start.to("#start", 0.5, {
                scale: 1,
                opacity: 0.7
            })
        }

        document.getElementById("start").addEventListener("mouseup", (event) => {
            start.fromTo("#start", 0.5, {
                scale: 1,
                opacity: 1
            }, {
                opacity: 0,
                scale:0,
                onStart: () => {
                    document.getElementById("start").removeEventListener("mouseover", zoomIn);
                    document.getElementById("start").removeEventListener("mouseout", zoomOut);
                },
                onComplete: () => { this.fire(Events.RESOURCES_LOADED)}
            })
        })
    }
}