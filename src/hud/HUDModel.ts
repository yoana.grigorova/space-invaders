/**
 * Created by Georgi Dimov on 29.8.2019 г..
 */
import { MainModel } from '../MainModel'

export class HUDModel {
    protected _mainModel: MainModel

    constructor(mainModel: MainModel) {
        this._mainModel = mainModel
    }

    get currentScore(): number {
        return this._mainModel.currentScore
    }

    get livesLeft(): number {
        return this._mainModel.livesLeft
    }

    restart(){
        this._mainModel.reset();
    }
}