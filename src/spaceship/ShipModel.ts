import { Main } from '../Main'
import { MainModel } from '../MainModel'

export class ShipModel {
    protected _mainModel: MainModel

    constructor(mainModel: MainModel) {
        this._mainModel = mainModel
    }

    updateLives(isSuccessful: boolean) {
        this._mainModel.updateLives(isSuccessful)
    }

    restart(){
        this._mainModel.reset();
    }
}