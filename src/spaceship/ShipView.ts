import { LayerManager, MobileView } from '@egt/egt-library'
import TimelineMax = gsap.TimelineMax

const WIDTH = 1280, HEIGHT = 720;
export class ShipView extends PIXI.Container {

    protected _ship: PIXI.Sprite;
    protected _shipVelocity: number = 10;
    protected _renderer: PIXI.SystemRenderer;
    protected lives: number = 3;
    protected _onStage: boolean = true;

    public init() {
        this._renderer = (window as any).app.renderer

        this._ship = PIXI.Sprite.fromImage("../assets/shooter.png");

        this._ship.anchor.set(0.5);

        this._ship.width = 60;
        this._ship.height = 60;
        
        this._ship.y = HEIGHT - (this._ship.height / 2);
        this._ship.x = WIDTH / 2;

        this._shipVelocity = 10;

        this.addChild(this._ship);

        LayerManager.stage.addChild(this)
        LayerManager.addToGroup(this, 'spaceship');

        this.restart();

        // PIXI.ticker.shared.add(this.onTick, this)
    }

    get x(){
        return this._ship.x;
    }

    get y(){
        return this._ship.y;
    }

    get onStage(){
        return this._onStage;
    }

    set x(value){
        this._ship.x = value;
    }

    set velocity(vx:number){
        this._shipVelocity=vx;
    }

    get velocity(){
        return this._shipVelocity;
    }

    getWidth(){
        return this._ship.width;
    }

    getHeight(){
        return this._ship.height;
    }

    moveLeft(){
        if(this._ship.x> this._ship.width/2){
            this._ship.x-=this._shipVelocity;
        }
    }

    moveRight(){
        if(this._ship.x < WIDTH - this._ship.width/2){
            this._ship.x+=this._shipVelocity;
        }
    }

    hitTest(bullet){
        if ((bullet.x >= this._ship.x - (this._ship.width / 2) && bullet.x <= this._ship.x + (this._ship.width / 2)) &&
            bullet.y >= this._ship.y - (this._ship.height / 2) && bullet.y <= this._ship.y + (this._ship.height / 2)) {
            bullet.remove();
            this.lives--;
            if(this.lives === 0){
                this._onStage = false;
                this.removeChild(this._ship);
            }
            this.explode();
            return true;
        }
        return false;
    }

    explode() {
        let explosion = PIXI.Sprite.fromImage("../assets/explosion1.png");
        explosion.texture.baseTexture.width = 256;
        explosion.texture.baseTexture.height = 256;
        explosion.anchor.set(0.5);
        let container = new PIXI.Container();
        container.addChild(explosion);

        container.x = this._ship.x;
        container.y = this._ship.y;
        container.scale.set(1.2);

        LayerManager.stage.addChild(container);

        let rect = new PIXI.Rectangle(0, 0, 64, 64);
        explosion.texture.frame = rect;

        let frames = 0;
        rect.y = 0;
        rect.x = -rect.width;

        PIXI.ticker.shared.add(function animation() {
            if (frames < 16) {
                if (frames % 4 === 0 && frames !== 0) {
                    rect.y += rect.height;
                    rect.x = 0;
                } else {
                    rect.x += rect.width;
                }
                explosion.texture.frame = rect;
                frames++;
            } else {
                PIXI.ticker.shared.remove(animation);
                LayerManager.stage.removeChild(container);
            }
        }, this)
    }

    wonGame(callback){
        let bg = LayerManager.stage.children.shift()
        LayerManager.stage.children.forEach(child => LayerManager.stage.removeChild(child));
        LayerManager.stage.addChild(bg);
        let tl = new TimelineMax();
        tl.set("#result", {text: "YOU WIN"})
        .fromTo("#result", 1,{
            css:{color:"white"},
            opacity: 0,
            scale:0,
        }, {
            css:{fontSize: 48, fontWeight: "bold"},
            opacity: 1,
            scale: 5
        }).to("#result", 2, {
            css:{fontSize: 0, fontWeight: "bold"},
            onComplete: () => {
                return callback();
            }
        }, "+=1.5")
    }

    restart(){
        this.lives = 3;
        this._onStage = true;
    }
   
}