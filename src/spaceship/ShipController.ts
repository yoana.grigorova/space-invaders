import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import { Events } from '../Events'
import { ShipModel } from './ShipModel'
import { ShipView } from './ShipView'
const WIDTH = 1280, HEIGHT = 720;

export class ShipController extends EventHandler {
    protected _view: ShipView
    protected _model: ShipModel

    constructor(view: ShipView, model: ShipModel) {
        super()

        this._view = view
        this._model = model

    }

    protected [Events.RESOURCES_LOADED](): void {
        this._view.init();
        let self = this;

        let left = this.keyboard(37),
            right = this.keyboard(39),
            space = this.keyboard(32);

        left.press = () => {
            event.preventDefault();
            if (this._view.x >= 20) {
                this._view.velocity = -10;
            } else {
                this._view.velocity = 0;
            }
        }

        left.release = () => {
            this._view.velocity = 0;
        }

        right.press = () => {
            event.preventDefault();
            this._view.velocity = 10;
        }

        right.release = () => {
            this._view.velocity = 0;
        }

        space.press = () => {
            event.preventDefault();
            if (this._view.onStage) {
                this.fire(Events.SHOOT, { x: this._view.x, y: this._view.y - (this._view.getHeight() / 2) });
            }
        }

        let state = play;
        PIXI.ticker.shared.add(delta => gameLoop(delta), this);

        function gameLoop(delta) {
            state(delta);
        }

        function play(delta) {
            if ((left.isDown && self._view.x - self._view.getWidth() / 2 > 0) || (right.isDown && self._view.x + self._view.getWidth() / 2 < WIDTH)) {
                self._view.x = self._view.velocity + self._view.x;
            }
        }
    }

    keyboard(keyCode) {
        var key = {
            code: keyCode,
            isDown: false,
            isUp: true,
            press: null,
            release: null,
            downHandler: event => {
                if (event.keyCode === key.code) {
                    if (key.isUp && key.press) key.press();
                    key.isDown = true;
                    key.isUp = false;
                }
            },
            upHandler: event => {
                if (event.keyCode === key.code) {
                    if (key.isDown && key.release) key.release();
                    key.isDown = false;
                    key.isUp = true;
                }
            }
        };

        window.addEventListener(
            "keydown", key.downHandler.bind(key), false
        );

        window.addEventListener(
            "keyup", key.upHandler.bind(key), false
        );

        return key;
    }


    protected [Events.GO_LEFT]() {
        this._view.moveLeft();
    }

    protected [Events.GO_RIGHT]() {
        this._view.moveRight();
    }

    protected [Events.SHIP_HIT_CHECK](bullet) {
        let hit = this._view.hitTest(bullet);
        if (hit) {
            this._model.updateLives(hit);
        }
    }

    protected [Events.WON_GAME]() {
        this._view.wonGame(()=>{
            this.fire(Events.LOAD_GAME);
            this._model.restart();
        });
    }
}