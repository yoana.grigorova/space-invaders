/**
 * Created by Georgi Dimov on 28.8.2019 г..
 */
import { EventEmitter } from '@egt/egt-library/dist/events/EventEmitter'
import { Events } from './Events'

export class Resources extends EventEmitter {

    public load() {
        this.fire(Events.LOAD_GAME);
    }

   
}