/**
 * Created by Georgi Dimov on 28.8.2019 г..
 */
import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import { Events } from '../Events'
import { UIModel } from './UIModel'
import { UIView } from './UIView'

export class UIController extends EventHandler {
    protected _view: UIView
    protected _model: UIModel

    constructor(view: UIView, model: UIModel) {
        super()

        this._view = view
        this._model = model
    }

    protected [Events.RESOURCES_LOADED]() {
        this._view.init()

        this._view.gameButton.on("mousedown", this.onGameButtonClicked, this)
        this._view.resetButton.on("click", this.onResetButtonClicked, this)

        this._view.reset()
    }

    protected [Events.GAME_OVER]() {
        this._view.gameOver()
    }

    protected onGameButtonClicked() {
        this.fire(Events.GAME_BUTTON_CLICKED)
    }

    protected onResetButtonClicked() {
        this._model.reset()
        this._view.reset()

        this.fire(Events.RESET_BUTTON_CLICKED)
    }
}