/**
 * Created by Georgi Dimov on 28.8.2019 г..
 */
import { DeviceOrientation } from '@egt/egt-library'
import { LayerManager } from '@egt/egt-library/dist/LayerManager'
import { MobileView } from '@egt/egt-library/dist/MobileView'

export class UIView extends MobileView {

    public gameButton: PIXI.Graphics
    public resetButton: PIXI.Graphics

    public init() {
        this.gameButton = new PIXI.Graphics()
        this.gameButton.beginFill(0xff0000)
        this.gameButton.drawRect(0, 0, 100, 60)
        this.gameButton.endFill()
        this.gameButton.interactive = true

        this.resetButton = new PIXI.Graphics()
        this.resetButton.beginFill(0x00ff00)
        this.resetButton.drawRect(0, 0, 100, 60)
        this.resetButton.endFill()
        this.resetButton.interactive = true

        this.addChild(this.gameButton, this.resetButton)

        LayerManager.stage.addChild(this)
        LayerManager.addToGroup(this, 'ui')

        this.onRotationChange()
    }

    reset() {
        this.gameButton.visible = true
        this.resetButton.visible = false
    }

    gameOver() {
        this.gameButton.visible = false
        this.resetButton.visible = true
    }

    protected onRotationChange(): void {
        // we can reposition the buttons based on orientation
        if (MobileView.deviceOrientation == DeviceOrientation.Landscape) {
            this.gameButton.position.set(200, 500)
            this.resetButton.position.set(400, 500)
        } else {
            this.gameButton.position.set(200, 300)
            this.resetButton.position.set(400, 300)
        }
    }
}