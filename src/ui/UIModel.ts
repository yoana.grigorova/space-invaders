/**
 * Created by Georgi Dimov on 29.8.2019 г..
 */
import { MainModel } from '../MainModel'

export class UIModel {
    protected _mainModel: MainModel

    constructor(mainModel: MainModel) {
        this._mainModel = mainModel
    }

    reset() {
        this._mainModel.reset()
    }
}