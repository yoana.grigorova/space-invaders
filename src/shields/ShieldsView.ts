import { LayerManager, MobileView } from '@egt/egt-library'

export class ShieldsView extends PIXI.Container {

    public _shields: PIXI.Sprite[];
    protected _boxVelocity: number = 1;
    protected _renderer: PIXI.SystemRenderer;

    public init() {
        this._renderer = (window as any).app.renderer;

        this.y = 450;
        this.x = 30;
        this._shields = [];

        for (let i = 0; i < 8; i++) {
            let shield: any = PIXI.Sprite.fromImage("../assets/shield.png");
            shield.width = 100;
            shield.height = 100;
            shield.x = i * (shield.width + 60);
            shield.lives = 20;

            this._shields.push(shield);
        }

        this.addChild(...this._shields);

        LayerManager.stage.addChild(this);
        LayerManager.addToGroup(this, 'shields');
    }

    hitTest(bullet): boolean {
        let hit = false;
        this._shields.forEach((shield: any, index) => {
            if ((bullet.y >= shield.getGlobalPosition().y && bullet.y <= shield.getGlobalPosition().y + shield.height) &&
                (bullet.x >= shield.getGlobalPosition().x && bullet.x <= shield.getGlobalPosition().x + shield.width)) {
                bullet.remove();
                shield.lives--;
                if (shield.lives === 0) {
                    this._shields.splice(index, 1);
                    this.removeChild(shield);
                }
                hit = true;
            }
        });
        return hit;
    }
}