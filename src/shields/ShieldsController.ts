import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import { Events } from '../Events'
import { ShieldsModel } from './ShieldsModel'
import { ShieldsView } from './ShieldsView'

export class ShieldsController extends EventHandler {
    protected _view: ShieldsView
    protected _model: ShieldsModel

    constructor(view: ShieldsView, model: ShieldsModel) {
        super()

        this._view = view
        this._model = model
    }

    protected [Events.RESOURCES_LOADED]() {
        this._view.init()
    }

    protected [Events.SHIELD_HIT_CHECK](bullet) {
        let hit = this._view.hitTest(bullet);
    } 
}