/**
 * Created by Georgi Dimov on 28.8.2019 г..
 */
import { EventEmitter } from '@egt/egt-library/dist/events/EventEmitter'

const prefix = EventEmitter.handlerPrefix + "Event"

export class Events {
    public static readonly GAME_BUTTON_CLICKED = prefix + "GameButtonClicked"
    public static readonly RESET_BUTTON_CLICKED = prefix + "ResetButtonClicked"
    public static readonly RESOURCES_LOADED = prefix + "ResourcesLoaded"
    public static readonly GO_TO_NEXT_LEVEL = prefix + "GoToNextLevel"
    public static readonly UPDATE_HUD = prefix + "UpdateHud"
    public static readonly GAME_OVER = prefix + "GameOver"
    public static readonly GO_RIGHT = prefix + "GoRight"
    public static readonly GO_LEFT = prefix + "GoLeft"
    public static readonly SHOOT = prefix + "Shoot"
    public static readonly ENEMY_SHOOT = prefix + "EnemyShoot"
    public static readonly HIT_CHECK = prefix + "HitCheck"
    public static readonly SHIP_HIT_CHECK = prefix + "ShipHitCheck"
    public static readonly SHIELD_HIT_CHECK = prefix + "ShieldHitCheck"
    public static readonly WON_GAME = prefix + "WonGame"
    public static readonly LOAD_GAME = prefix + "LoadGame"

}