import { Main } from '../Main'
import { MainModel } from '../MainModel'

export class EnemyModel {
    protected _mainModel: MainModel

    constructor(mainModel: MainModel) {
        this._mainModel = mainModel
    } 

    addHitAttempt(isSuccessful: boolean) {
        this._mainModel.addHitAttempt(isSuccessful)
    }
}