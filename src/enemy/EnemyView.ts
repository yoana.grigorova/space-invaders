import { LayerManager, MobileView } from '@egt/egt-library'

export class EnemyView extends PIXI.Container {

    public _enemies: PIXI.Sprite[];
    protected _boxVelocity: number = 1
    protected _renderer: PIXI.SystemRenderer

    public init() {
        this._renderer = (window as any).app.renderer;

        this.y = 70;
        this._enemies = [];

        let rowCount = 8;
        let rows = 3;
        for (let i = 0; i < rows * rowCount; i++) {
            let row = Math.floor(i / rowCount);
            let enemy;
            if (row === 0) {
                enemy = PIXI.Sprite.fromImage("../assets/boss.png");
            } else {
                enemy = PIXI.Sprite.fromImage("../assets/target.png");
            }

            enemy.height = enemy.width = 50;
            enemy.x = (i % rowCount) * (enemy.width + 20);
            enemy.y = row * 60;
            enemy.liveCount = 3;
            enemy.lives = [];

            for (let i = 0; i < enemy.liveCount; i++) {
                let live = this.renderLives(enemy.x + (i * 15), enemy.y + enemy.height);
                enemy.lives.push(live);
            }

            this._enemies.push(enemy);
        }

        this.addChild(...this._enemies);
        LayerManager.stage.addChild(this)
        LayerManager.addToGroup(this, 'enemies')

        PIXI.ticker.shared.add(this.onTick, this)
    }

    explode(enemy) {
        let explosion = PIXI.Sprite.fromImage("../assets/explosion1.png");
        explosion.texture.baseTexture.width = 256;
        explosion.texture.baseTexture.height = 256;
        let container = new PIXI.Container();
        container.addChild(explosion);

        container.x = enemy.getGlobalPosition().x;
        container.y = enemy.getGlobalPosition().y;
        container.scale.set(1.2);

        LayerManager.stage.addChild(container);

        let rect = new PIXI.Rectangle(0, 0, 64, 64);
        explosion.texture.frame = rect;

        let frames = 0;
        rect.y = 0;
        rect.x = -rect.width;

        PIXI.ticker.shared.add(function animation() {
            if (frames < 16) {
                if (frames % 4 === 0 && frames !== 0) {
                    rect.y += rect.height;
                    rect.x = 0;
                } else {
                    rect.x += rect.width;
                }
                explosion.texture.frame = rect;
                frames++;
            } else {
                PIXI.ticker.shared.remove(animation);
                LayerManager.stage.removeChild(container);
            }
        }, this)
    }

    renderLives(x, y) {
        let live = PIXI.Sprite.fromImage("../assets/live.png");
        live.width = live.height = 20;
        live.x = x;
        live.y = y;
        this.addChild(live);
        return live;
    }

    hitTest(bullet): boolean {
        let self = this;
        let hit = false;
        this._enemies.forEach((enemy: any, index) => {
            if ((bullet.y >= enemy.y && bullet.y <= enemy.y + enemy.height) &&
                (bullet.x >= enemy.getGlobalPosition().x && bullet.x <= enemy.getGlobalPosition().x + enemy.width)) {
                hit = true;
                bullet.remove();
                enemy.liveCount--;
                let live = enemy.lives.pop();
                this.removeChild(live);

                if (enemy.liveCount === 0) {
                    self.explode(enemy);
                    self.removeChild(enemy);
                    this._enemies.splice(index, 1);
                }
            }
        })

        return hit;
    }

    // gameOver() {
    //     this._boxVelocity = 0
    //     this._boxArea.visible = false
    // }

    protected onTick() {
        if (this._boxVelocity != 0) {
            this.x += this._boxVelocity

            if (this.x <= 0 || this.x + this.width >= this._renderer.width) {
                this._boxVelocity *= -1
            }
        }
    }
}