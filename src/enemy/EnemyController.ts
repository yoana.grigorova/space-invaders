import { EventHandler } from '@egt/egt-library/dist/events/EventHandler'
import { Events } from '../Events'
import { EnemyModel } from './EnemyModel'
import { EnemyView } from './EnemyView'

export class EnemyController extends EventHandler {
    protected _view: EnemyView
    protected _model: EnemyModel
    protected interval: any;

    constructor(view: EnemyView, model: EnemyModel) {
        super()

        this._view = view
        this._model = model
        
    }

    protected [Events.RESOURCES_LOADED]() {
        this._view.init();
        this.interval = setInterval(() => {
            if (this._view._enemies && this._view._enemies.length) {
                let enemy = this._view._enemies[Math.floor(Math.random() * this._view._enemies.length)];
                this.fire(Events.ENEMY_SHOOT, { x: enemy.getGlobalPosition().x + enemy.width/2, y: enemy.getGlobalPosition().y + enemy.height });
            } else if(this._view._enemies && this._view._enemies.length === 0) {
                clearInterval(this.interval);
            }
        }, 1000);
    } 

    protected [Events.GAME_OVER]() {
        clearInterval(this.interval);
    }

    // protected [Events.LOAD_GAME]() {
    // }

    protected [Events.HIT_CHECK](bullet) {
        let hit = this._view.hitTest(bullet);
        if(hit){
            this._model.addHitAttempt(hit);
        }

        if(!this._view._enemies.length){
            this.fire(Events.WON_GAME);
            clearInterval(this.interval);
        }
    } 

}