const chalk = require('chalk')
const https = require('https')
const fs = require('fs')

const destDir = './dev/'
const libsSrc = ['dist/', 'node_modules/pixi-layers/dist/', 'node_modules/@egt/egt-library/dist/', 'node_modules/pixi.js/dist/',
    'node_modules/bottlejs/dist/']
const libs = ['game', 'pixi-layers', 'egt-library', 'pixi', 'bottle']

let libsToCopy = []

function prepareCopyList() {
    for (let i = 0; i < libsSrc.length; i++) {
        let copyJSFile = {}
        let copyMapFile = {}

        copyJSFile.src = libsSrc[i] + libs[i] + '.js'
        copyJSFile.dest = destDir + libs[i] + '.js'
        libsToCopy.push(copyJSFile)

        copyMapFile.src = libsSrc[i] + libs[i] + '.js.map'
        copyMapFile.dest = destDir + libs[i] + '.js.map'
        libsToCopy.push(copyMapFile)
    }
}

prepareCopyList()

libsToCopy.forEach(copyObj => {
    try {
        if (fs.existsSync(copyObj.src)) {
            fs.copyFileSync(copyObj.src, copyObj.dest)
            console.log(chalk.green(`${copyObj.src} => ${copyObj.dest}`))
        }
    } catch (e) {
        console.log(chalk.red(`${copyObj.src} => ${copyObj.dest} failed: `), e)
    }
})
