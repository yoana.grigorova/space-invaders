const path = require('path');
const fs = require('fs');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require("webpack");

let productionMode = false

module.exports = (env, argv) => {
    // This is needed so that Webstorm can resolve this config file properly
    argv = argv || {}
    // This is intended to be global so the webpack-conditional-loader can work properly
    // noinspection JSUndeclaredVariable
    CUSTOM_RESULT_ENABLED = !!argv['custom-result'];

    console.log('Webpack mode set to', !!env.prod ? 'production.' : 'development.')
    console.log(`Building into ${path.resolve(__dirname, env.path ? env.path : 'dist')}`);

    productionMode = !!env.prod

    // noinspection JSUnusedLocalSymbols,SpellCheckingInspection
    return {
        mode: !!env.prod ? 'production' : 'development',
        entry: {
            "game" : './src/index',
            "game.min" : './src/index'
        },
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: [{
                        loader: 'ts-loader',
                        options: {
                            //allowTsInNodeModules: true,
                            logInfoToStdOut: true,
                            logLevel: "info",
                            /*compilerOptions: {
                                outDir: path.resolve(__dirname, 'dist')
                            }*/
                        }
                    }, 'webpack-conditional-loader'
                    ]
                }
            ]
        },
        resolve: {
            extensions: [ '.ts', '.tsx', '.js']
        },
        externals: {
            "pixi-layers": "",
            "pixi-filters": "",
            "@egt/egt-library": "egtLibrary",
            "@egt/egt-library/dist/DeviceOrientation": "egtLibrary",
            "@egt/egt-library/dist/Device": "egtLibrary",
            "@egt/egt-library/dist/MobileView": "egtLibrary",
            "@egt/egt-library/dist/LayerManager": "egtLibrary",
            "@egt/egt-library/dist/LogServer": "egtLibrary",
            "@egt/egt-library/dist/events/Event": "egtLibrary",
            "@egt/egt-library/dist/events/IEventDispatcher": "egtLibrary",
            "@egt/egt-library/dist/events/EventDispatcher": "egtLibrary",
            "@egt/egt-library/dist/events/EventHandler": "egtLibrary",
            "@egt/egt-library/dist/events/LoaderEvent": "egtLibrary",
            "@egt/egt-library/dist/events/EventEmitter": "egtLibrary",
            "@egt/egt-library/dist/loading/Loader": "egtLibrary",
            "@egt/egt-library/dist/loading/PIXILoader": "egtLibrary",
            "@egt/egt-library/dist/loading/JSONLoader": "egtLibrary",
            "@egt/egt-library/dist/loading/ScriptLoader": "egtLibrary",
            "@egt/egt-library/dist/loading/LayoutLoader": "egtLibrary",
            "@egt/egt-library/dist/loading/XMLLoader": "egtLibrary",
            "@egt/egt-library/dist/loading/LoadingQueue": "egtLibrary",
            "@egt/egt-library/dist/utils/log": {
                root: ["egtLibrary", "log"],
                commonjs2: ["./egtLibrary", "log"],
                commonjs: ["./egtLibrary", "log"],
                amd: ["egtLibrary", "log"]
            },
            "@egt/egt-library/dist/utils/Serializer": "egtLibrary",
            "@egt/egt-library/dist/utils/Timer": "egtLibrary",
            "@egt/egt-library/dist/utils/decimalToHexColorString": "egtLibrary",
            "@egt/egt-library/dist/utils/Utils": "egtLibrary",
            "@egt/egt-library/dist/utils/color": "egtLibrary",
            "@egt/egt-library/dist/widgets/Text": "egtLibrary",
            "@egt/egt-library/dist/widgets/MoneyText": "egtLibrary",
            "@egt/egt-library/dist/widgets/Button": "egtLibrary",
            "@egt/egt-library/dist/widgets/StateButton": "egtLibrary",
            "@egt/egt-library/dist/widgets/RollingNumberView": "egtLibrary",
            "@egt/egt-library/dist/Sound/Sound": "egtLibrary",
            "@egt/egt-library/dist/Sound/SoundManager": "egtLibrary",
            "@egt-types/gamy-ts": "gamy",
            "bottlejs": "Bottle"
        },
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, env.path ? env.path : 'dist'),
            library: "Game",
            libraryTarget: 'umd',
            umdNamedDefine: true
        },
        optimization: {
            minimize: !!env.prod,
            minimizer: [new UglifyJsPlugin({
                //include: /\.min\.js$/
                test: /\.min.js(\?.*)?$/i,
                sourceMap: true,
                parallel: true,
                extractComments: {
                    condition: true,
                    filename: file => null,
                    banner: false
                }
            })]
        },
        plugins: [
            new DtsBundlePlugin(),
            new webpack.DefinePlugin({
                DEBUG: JSON.stringify(!env.prod),
            })
        ],
        performance: {
            hints: false
        }
    };
}

function DtsBundlePlugin() {
    this.apply = compiler => {
        compiler.hooks.done.tap('DtsBundlePlugin', () => {
            let dts = require('dts-bundle');

            dts.bundle({
                name: 'template-project',
                main: 'dist/src/index.d.ts',
                out: '../game.d.ts',
                removeSource: productionMode,
                outputAsModuleFolder: false,
                headerPath: 'none',
                headerText: ''
            });
        });
    }
}
