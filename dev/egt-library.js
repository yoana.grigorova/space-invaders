(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("pixi-layers"));
	else if(typeof define === 'function' && define.amd)
		define("egtLibrary", ["pixi-layers"], factory);
	else if(typeof exports === 'object')
		exports["egtLibrary"] = factory(require("pixi-layers"));
	else
		root["egtLibrary"] = factory(root["pixi-layers"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__43__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 18);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Event_1 = __webpack_require__(3);
var LoaderEvent = /** @class */ (function (_super) {
    __extends(LoaderEvent, _super);
    function LoaderEvent(type, data) {
        if (data === void 0) { data = null; }
        return _super.call(this, type, data, false) || this;
    }
    LoaderEvent.COMPLETE = "complete";
    LoaderEvent.CHILD_COMPLETE = "childComplete";
    LoaderEvent.PROGRESS = "progress";
    LoaderEvent.CHILD_PROGRESS = "childProgress";
    LoaderEvent.ERROR = "error";
    LoaderEvent.CHILD_ERROR = "childError";
    LoaderEvent.DECODE_ERROR = "decodeError";
    return LoaderEvent;
}(Event_1.Event));
exports.LoaderEvent = LoaderEvent;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Loader_1 = __webpack_require__(4);
var LoaderEvent_1 = __webpack_require__(0);
var BaseLoader = /** @class */ (function (_super) {
    __extends(BaseLoader, _super);
    // TODO: implement getData in all loaders and return null if the content is not loaded
    // TODO: implement unloading of all loaders (if possible)
    function BaseLoader(src, cacheTimestamp, prependURL, id) {
        var _this = _super.call(this) || this;
        _this._state = BaseLoader.STATE_IDLE;
        _this._lastProgress = 0;
        _this._loadingRequested = false;
        _this._request = null;
        if (cacheTimestamp == undefined)
            cacheTimestamp = Loader_1.Loader.cacheTimestamp;
        _this._cacheTimestamp = cacheTimestamp;
        if (prependURL == undefined)
            prependURL = Loader_1.Loader.prependURL;
        _this._prependURL = prependURL;
        _this.constructGetParameters();
        if (id == undefined)
            id = src;
        _this._id = id;
        Loader_1.Loader.addLoader(_this);
        _this._src = src;
        _this.setUpRequest();
        return _this;
    }
    Object.defineProperty(BaseLoader.prototype, "src", {
        get: function () {
            return this._src;
        },
        enumerable: true,
        configurable: true
    });
    BaseLoader.prototype.isIdle = function () {
        return this._state == BaseLoader.STATE_IDLE;
    };
    BaseLoader.prototype.isLoading = function () {
        return this._state == BaseLoader.STATE_LOADING;
    };
    BaseLoader.prototype.isLoaded = function () {
        return this._state == BaseLoader.STATE_LOADED;
    };
    BaseLoader.prototype.hasError = function () {
        return this._state == BaseLoader.STATE_ERROR;
    };
    BaseLoader.prototype.load = function () {
        if (this._state == BaseLoader.STATE_LOADING)
            return;
        else if (this._state == BaseLoader.STATE_LOADED) {
            var self = this;
            // report the progress and call _onLoaded asynchronously
            setTimeout(function () {
                self.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, { progress: 1, progressChange: 0 }));
                self.onLoaded();
            }, 0);
            return;
        }
        else if (this._state == BaseLoader.STATE_ERROR ||
            this._request.readyState == XMLHttpRequest.UNSENT) {
            this.openRequest();
        }
        this._lastProgress = 0;
        this._state = BaseLoader.STATE_LOADING;
        if (this._request.readyState == XMLHttpRequest.OPENED)
            this._request.send();
        else
            this._loadingRequested = true;
    };
    BaseLoader.prototype.stop = function () {
        if (this._state != BaseLoader.STATE_LOADING)
            return;
        this._state = BaseLoader.STATE_IDLE;
        if (this._request)
            this._request.abort();
    };
    BaseLoader.prototype.dispose = function () {
        if (this._request) {
            this._request.abort();
            this._request.onreadystatechange = null;
            this._request.onprogress = null;
            this._request = null;
        }
        Loader_1.Loader.removeLoader(this);
        _super.prototype.dispose.call(this);
    };
    BaseLoader.prototype.setUpRequest = function () {
        this._request = new XMLHttpRequest();
        this.openRequest();
        this._request.responseType = this.getResponseType();
        this._request.onreadystatechange = this.onXHRReadyStateChange.bind(this);
        this._request.onprogress = this.onXHRProgress.bind(this);
    };
    BaseLoader.prototype.openRequest = function () {
        var prependURL = this._prependURL ? this._prependURL : "";
        this._request.open('GET', prependURL + this._src + this._getParameters);
    };
    BaseLoader.prototype.constructGetParameters = function () {
        this._getParameters = this._cacheTimestamp ? "?v=" + this._cacheTimestamp : "";
    };
    BaseLoader.prototype.onXHRReadyStateChange = function (event) {
        if (this._request.readyState == XMLHttpRequest.OPENED) {
            if (this._loadingRequested) {
                this._request.send();
                this._loadingRequested = false;
            }
        }
        else if (this._request.readyState == XMLHttpRequest.DONE) {
            if (this._request.status >= 200 && this._request.status < 400) {
                this._state = BaseLoader.STATE_LOADED;
                this.onLoaded(event);
            }
            else if (this._request.status > 0) {
                this._state = BaseLoader.STATE_ERROR;
                this.onError(event);
            }
        }
    };
    BaseLoader.prototype.onXHRProgress = function (event) {
        var newProgress;
        if (event.lengthComputable)
            newProgress = event.loaded / event.total;
        else
            newProgress = 1;
        var data = {
            progress: newProgress,
            progressChange: newProgress - this._lastProgress
        };
        this._lastProgress = newProgress;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, data));
    };
    BaseLoader.STATE_IDLE = 0;
    BaseLoader.STATE_LOADING = 1;
    BaseLoader.STATE_LOADED = 2;
    BaseLoader.STATE_ERROR = 4;
    return BaseLoader;
}(Loader_1.Loader));
exports.BaseLoader = BaseLoader;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var DeviceOrientation_1 = __webpack_require__(7);
var Device = /** @class */ (function () {
    function Device() {
    }
    Object.defineProperty(Device, "isMobile", {
        get: function () {
            return Device.isiOS || Device.isAndroid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "isiOS", {
        get: function () {
            return navigator.userAgent.match(/iPhone/i) != null ||
                navigator.userAgent.match(/iPad/i) != null ||
                navigator.userAgent.match(/iPod/i) != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "isAndroid", {
        get: function () {
            return navigator.userAgent.match(/Android/i) != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "isCriOS", {
        get: function () {
            return navigator.userAgent.match(/CriOS/i) != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "isChrome", {
        get: function () {
            return navigator.userAgent.match(/Chrome/i) != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "isSafari", {
        get: function () {
            return navigator.userAgent.match(/Safari/i) != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "isFirefox", {
        get: function () {
            return navigator.userAgent.match(/Firefox/i) != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "isIE11", {
        get: function () {
            return !Device.isFirefox && navigator.appName == "Netscape" && navigator.userAgent.match(/Trident/i) != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device, "currentOrientation", {
        get: function () {
            if (window.orientation != undefined) {
                if (window.orientation == -90 || window.orientation == 90) {
                    return DeviceOrientation_1.DeviceOrientation.Landscape;
                }
                else {
                    return DeviceOrientation_1.DeviceOrientation.Portrait;
                }
            }
            else {
                var orientation = window.screen['orientation'] || window.screen['MozOrientation'];
                if (orientation && orientation.type != undefined) {
                    if (orientation.type === "landscape-primary" || orientation.type === "landscape-secondary") {
                        return DeviceOrientation_1.DeviceOrientation.Landscape;
                    }
                    else {
                        return DeviceOrientation_1.DeviceOrientation.Portrait;
                    }
                }
                else {
                    return DeviceOrientation_1.DeviceOrientation.Landscape;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Device.resolveEvent = function (eventName) {
        var pointerSupport = typeof PointerEvent !== "undefined";
        var supportTouch = typeof Touch !== "undefined";
        if (eventName == "mousedown") {
            if (pointerSupport) {
                eventName = 'pointerdown';
            }
            else if (supportTouch) {
                eventName = "touchstart";
            }
        }
        else if (eventName == "mouseup") {
            if (pointerSupport) {
                eventName = 'pointerup';
            }
            else if (supportTouch) {
                eventName = "touchend";
            }
        }
        else if (eventName == "mouseupoutside") {
            if (pointerSupport) {
                eventName = 'pointerupoutside';
            }
            else if (supportTouch) {
                eventName = "touchendoutside";
            }
        }
        else if (eventName == "mousemove") {
            if (pointerSupport) {
                eventName = 'pointermove';
            }
            else if (supportTouch) {
                eventName = "touchmove";
            }
        }
        else if (eventName == "click") {
            if (pointerSupport) {
                eventName = 'pointertap';
            }
            else if (supportTouch) {
                eventName = "tap";
            }
        }
        return eventName;
    };
    // TODO: make these two getters
    Device.isSoundSupported = function () {
        return Device.getAudioContext() != undefined;
    };
    Device.getAudioContext = function () {
        if (window.AudioContext)
            return AudioContext;
        else
            return window.webkitAudioContext;
    };
    Device.scale = 1;
    return Device;
}());
exports.Device = Device;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Event = /** @class */ (function () {
    function Event(type, data, bubbles, cancelable) {
        if (data === void 0) { data = null; }
        if (bubbles === void 0) { bubbles = true; }
        if (cancelable === void 0) { cancelable = false; }
        this.type = type;
        this.data = data;
        this.bubbles = bubbles;
        this.cancelable = cancelable;
    }
    return Event;
}());
exports.Event = Event;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventDispatcher_1 = __webpack_require__(5);
var Loader = /** @class */ (function (_super) {
    __extends(Loader, _super);
    function Loader() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._id = "";
        return _this;
    }
    Loader.addLoader = function (loader) {
        Loader.loaders[loader._id] = loader;
    };
    Loader.removeLoader = function (loader) {
        delete Loader.loaders[loader._id];
    };
    Loader.getLoader = function (id) {
        return Loader.loaders[id];
    };
    Object.defineProperty(Loader.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Loader.loaders = {};
    Loader.prependURL = "";
    return Loader;
}(EventDispatcher_1.EventDispatcher));
exports.Loader = Loader;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var EventDispatcher = /** @class */ (function () {
    function EventDispatcher() {
        this._listeners = {};
    }
    EventDispatcher.prototype.addEventListener = function (type, callback, callbackContext) {
        var registeredObjects = this._listeners[type] || [];
        var callbacks;
        var len = registeredObjects.length;
        for (var i = 0; i < len; i++) {
            if (registeredObjects[i].context == callbackContext) {
                callbacks = registeredObjects[i].callbacks;
                break;
            }
        }
        if (!callbacks) {
            callbacks = [];
            var object = { context: callbackContext, callbacks: callbacks };
            registeredObjects.push(object);
        }
        len = callbacks.length;
        for (var i = 0; i < len; i++) {
            if (callbacks[i] == callback)
                return;
        }
        callbacks.push(callback);
        this._listeners[type] = registeredObjects;
    };
    EventDispatcher.prototype.dispatchEvent = function (event, target) {
        var registeredObjects = this._listeners[event.type];
        if (!registeredObjects)
            return;
        registeredObjects = registeredObjects.slice();
        if (target != undefined)
            event.target = target;
        else
            event.target = this;
        event.currentTarget = this;
        var len = registeredObjects.length;
        var object, callbacksLen;
        for (var i = 0; i < len; i++) {
            object = registeredObjects[i];
            callbacksLen = object.callbacks.length;
            for (var j = 0; j < callbacksLen; j++)
                object.callbacks[j].call(object.context, event);
        }
    };
    EventDispatcher.prototype.removeEventListener = function (type, callback, callbackContext) {
        var registeredObjects = this._listeners[type];
        if (!registeredObjects)
            return;
        var len = registeredObjects.length;
        var object, callbacksLen;
        for (var i = 0; i < len; i++) {
            object = registeredObjects[i];
            if (object.context != callbackContext)
                continue;
            callbacksLen = object.callbacks.length;
            for (var j = 0; j < callbacksLen; j++) {
                if (object.callbacks[j] == callback) {
                    object.callbacks.splice(j, 1);
                    j--;
                    callbacksLen--;
                }
            }
            if (callbacksLen == 0) {
                registeredObjects.splice(i, 1);
                i--;
                len--;
            }
        }
        if (len == 0) {
            delete this._listeners[type];
        }
    };
    EventDispatcher.prototype.hasEventListener = function (type) {
        return this._listeners[type] != undefined;
    };
    EventDispatcher.prototype.dispose = function () {
        for (var type in this._listeners)
            delete this._listeners[type];
    };
    return EventDispatcher;
}());
exports.EventDispatcher = EventDispatcher;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Serializer = /** @class */ (function () {
    function Serializer() {
    }
    Serializer.prototype.serialize = function (obj) {
        for (var key in obj) {
            if (this.hasOwnProperty(key) && obj[key] != null) {
                if (typeof this[key] === "object" && this[key] instanceof Serializer) {
                    this[key].serialize(obj[key]);
                }
                else {
                    if (this[key] instanceof Array && typeof this[key][0] === "object" &&
                        this[key][0] instanceof Serializer) {
                        if (obj[key].length == 0) {
                            this[key].length = 0;
                        }
                        else {
                            var objectClassConstructor = this[key][0].constructor;
                            for (var i = 0; i < obj[key].length; i++) {
                                this[key][i] = new objectClassConstructor();
                                this[key][i].serialize(obj[key][i]);
                            }
                        }
                    }
                    else {
                        this[key] = obj[key];
                    }
                }
            }
        }
    };
    return Serializer;
}());
exports.Serializer = Serializer;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var DeviceOrientation;
(function (DeviceOrientation) {
    DeviceOrientation["Portrait"] = "portrait";
    DeviceOrientation["Landscape"] = "landscape";
})(DeviceOrientation = exports.DeviceOrientation || (exports.DeviceOrientation = {}));


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * TypeScript log decorator:
 *  Add logging methods to class. Output in console contains name of class from which log is called;
 *  Add log to method calls. Output in console contains name of class, method, list of arguments and retrun value of the method;
 *  Add log to property access. Output contains name of class, property name and current value for getter, or new value for setter;
 *
 * Log decorators are enabled only if DEBUG variable set to `true` during compilation of this script;
 *   Run `grunt debug` to enalbe log decorators;
 *   Run `grunt build` to remove log docorators functionality;
 *
 * Log methods have exacly same functionality as console.log;
 * Log methods are extension of Logger specification https://console.spec.whatwg.org/#logger
 * How to format log messages https://console.spec.whatwg.org/#formatting-specifiers
 *
 * Log is disabled by default.
 * To enalbe it globaly do following from entry point of application:
 *
 * @example enalbe log
 *   import log = com.egt.library.utils.log;
 *   log.isEnabled = true;
 *
 * @example of a class docorator
 *   import log = com.egt.library.utils.log;
 *
 *   @log.decorator
 *   class SomeView {
 *       // Declare which methods from decorator to use in class.
 *       log;
 *
 *       someMethod(param) {
 *          this.log('Some method called with param[%s]', param);
 *          // Output to console: [EGT][SomeView] Some method called with param[trololo]
 *       }
 *   }
 *
 * @example of a method docorator
 *   import log = com.egt.library.utils.log;
 *
 *   class SomeView {
 *       @log.decorator
 *       someMethod(param1, param2) {
 *          return 'Wanna get some sleep';
 *       }
 *   }
 *
 *   const view = new SomeView();
 *   view.someMethod('friday', 'afternoon');
 *   // Output to console: [EGT][SomeView] SomeView.someMethod('friday', 'afternoon') => 'Wanna get some sleep'
 *
 * @example of a property docorator
 *   import log = com.egt.library.utils.log;
 *
 *   class SomeView {
 *       @log.decorator
 *       public someProperty: number = 10;
 *   }
 *
 *   const view = new SomeView();
 *   view.someProperty;
 *   // Output to console: [EGT][SomeView] Get SomeView.someProperty => 10;
 *
 *   view.someProperty = 21;
 *   // Output to console: [EGT][SomeView] Set SomeView.someProperty => 21;
 *
 * @example direct use of log, warn or error functions
 *   import log = com.egt.library.utils.log;
 *   import warn = com.egt.library.utils.warn;
 *   import error = com.egt.library.utils.warn;
 *
 *   log('Q: How can you tell if a poker player is bluffing? A: %s', answer);
 *   // Output in console: [EGT][log] How can you tell if a poker player is bluffing? A: His chips are moving.
 *
 *   warn('I sense a trap.');
 *   // Output marked as warning in console: [EGT][warn] I sense a trap.
 *
 *   error('Spring the trap.');
 *   // Output marked as error in console: [EGT][error] Spring the trap.
 */
Object.defineProperty(exports, "__esModule", { value: true });
// Log functions
exports.log = LogFunction('log');
exports.warn = LogFunction('warn');
exports.error = LogFunction('error');
// Global log settings
exports.isEnabled = false;
exports.isStylized = false;
var Config = /** @class */ (function () {
    function Config() {
        this.isEnabled = false;
        this.isStylized = false;
    }
    return Config;
}());
exports.Config = Config;
function decorator() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    switch (args.length) {
        case 1:
            return logClass.apply(this, args);
        case 2:
            return logProperty.apply(this, args);
        case 3:
            if (typeof args[2] === 'number') {
                return logParameter.apply(this, args);
            }
            else if (typeof args[2] === 'undefined') {
                return logProperty.apply(this, args);
            }
            return logMethod.apply(this, args);
        default:
            throw new Error('Decorators are not valid here!');
    }
}
exports.decorator = decorator;
// TODO create factory for config decorator.
function decoratorConfig(config) {
    return function (target) { return logClass(target, config); };
}
exports.decoratorConfig = decoratorConfig;
// Implemented log methods.
var logMethods = ['log', 'warn', 'error'];
var logClass;
var logMethod;
var logProperty;
var logParameter;
function noop() {
}
function LogFunction(level) {
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        output.apply(void 0, [level, level].concat(args));
    };
}
function output(level) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    if (typeof args[0] === 'string') {
        args[0] = "[EGT][" + this + "] " + args[0];
    }
    else {
        args.unshift("[EGT][" + this + "]");
    }
    console[level].apply(console, args);
}
function outputStylized(instance, level, style) {
    var args = [];
    for (var _i = 3; _i < arguments.length; _i++) {
        args[_i - 3] = arguments[_i];
    }
    if (typeof args[0] === 'string') {
        args[0] = "[EGT][%c" + instance + "%c] " + args[0];
        // Empty string is a style for second %c flag,
        // which is there only to terminate first style.
        args.splice(1, 0, style, '');
    }
    else {
        args.unshift("[EGT][%c" + instance, style, ']');
    }
    console[level].apply(console, args);
}
// When toString method called on class - output class name.
function addToStringMethod(prototype) {
    if (prototype && !prototype.hasOwnProperty('toString')) {
        Object.defineProperty(prototype, 'toString', {
            value: function () {
                return this.constructor['name'];
            }
        });
    }
}
function stringify(o) {
    return JSON.stringify(o, function (k, v) { return k ? '' + v : v; });
}
if (false) {}
else {
    // In release mode add empty methods instead of log calls.
    logClass = function logClass(target, config) {
        var objectProperties = {};
        logMethods.forEach(function (level) {
            objectProperties[level] = { value: noop };
        });
        Object.defineProperties(target.prototype, objectProperties);
        return target;
    };
    // In release mode do not modify properties.
    logProperty = noop;
    // In release mode do not modify methods.
    logMethod = function (target, key, descriptor) {
        return descriptor;
    };
    logParameter = noop;
}


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var BrowserDetect = /** @class */ (function () {
    function BrowserDetect() {
    }
    BrowserDetect.detect = function () {
        if (typeof navigator !== 'undefined') {
            return BrowserDetect.parseUserAgent(navigator.userAgent);
        }
        return null;
    };
    BrowserDetect.detectOS = function (userAgentString) {
        var rules = BrowserDetect.getOperatingSystemRules();
        var detected = rules.filter(function (os) {
            return os.rule && os.rule.test(userAgentString);
        })[0];
        return detected ? detected.name : null;
    };
    BrowserDetect.parseUserAgent = function (userAgentString) {
        var browsers = BrowserDetect.getBrowserRules();
        if (!userAgentString) {
            return null;
        }
        var detected = browsers.map(function (browser) {
            var match = browser.rule.exec(userAgentString);
            var version = match && match[1].split(/[._]/).slice(0, 3);
            if (version && version.length < 3) {
                version = version.concat(version.length == 1 ? [0, 0] : [0]);
            }
            return match && {
                name: browser.name,
                version: version.join('.')
            };
        }).filter(Boolean)[0] || null;
        if (detected) {
            detected.os = BrowserDetect.detectOS(userAgentString);
        }
        if (/alexa|bot|crawl(er|ing)|facebookexternalhit|feedburner|google web preview|nagios|postrank|pingdom|slurp|spider|yahoo!|yandex/i.test(userAgentString)) {
            detected = detected || {};
            detected.bot = true;
        }
        return detected;
    };
    BrowserDetect.getBrowserRules = function () {
        return BrowserDetect.buildRules([
            ['aol', /AOLShield\/([0-9\._]+)/],
            ['edge', /Edge\/([0-9\._]+)/],
            ['yandexbrowser', /YaBrowser\/([0-9\._]+)/],
            ['vivaldi', /Vivaldi\/([0-9\.]+)/],
            ['kakaotalk', /KAKAOTALK\s([0-9\.]+)/],
            ['samsung', /SamsungBrowser\/([0-9\.]+)/],
            ['chrome', /(?!Chrom.*OPR)Chrom(?:e|ium)\/([0-9\.]+)(:?\s|$)/],
            ['phantomjs', /PhantomJS\/([0-9\.]+)(:?\s|$)/],
            ['crios', /CriOS\/([0-9\.]+)(:?\s|$)/],
            ['firefox', /Firefox\/([0-9\.]+)(?:\s|$)/],
            ['fxios', /FxiOS\/([0-9\.]+)/],
            ['opera', /Opera\/([0-9\.]+)(?:\s|$)/],
            ['opera', /OPR\/([0-9\.]+)(:?\s|$)$/],
            ['ie', /Trident\/7\.0.*rv\:([0-9\.]+).*\).*Gecko$/],
            ['ie', /MSIE\s([0-9\.]+);.*Trident\/[4-7].0/],
            ['ie', /MSIE\s(7\.0)/],
            ['bb10', /BB10;\sTouch.*Version\/([0-9\.]+)/],
            ['android', /Android\s([0-9\.]+)/],
            ['ios', /Version\/([0-9\._]+).*Mobile.*Safari.*/],
            ['safari', /Version\/([0-9\._]+).*Safari/],
            ['facebook', /FBAV\/([0-9\.]+)/],
            ['instagram', /Instagram\ ([0-9\.]+)/],
            ['ios-webview', /AppleWebKit\/([0-9\.]+).*Mobile/]
        ]);
    };
    BrowserDetect.getOperatingSystemRules = function () {
        return BrowserDetect.buildRules([
            ['iOS', /iP(hone|od|ad)/],
            ['Android OS', /Android/],
            ['BlackBerry OS', /BlackBerry|BB10/],
            ['Windows Mobile', /IEMobile/],
            ['Amazon OS', /Kindle/],
            ['Windows 3.11', /Win16/],
            ['Windows 95', /(Windows 95)|(Win95)|(Windows_95)/],
            ['Windows 98', /(Windows 98)|(Win98)/],
            ['Windows 2000', /(Windows NT 5.0)|(Windows 2000)/],
            ['Windows XP', /(Windows NT 5.1)|(Windows XP)/],
            ['Windows Server 2003', /(Windows NT 5.2)/],
            ['Windows Vista', /(Windows NT 6.0)/],
            ['Windows 7', /(Windows NT 6.1)/],
            ['Windows 8', /(Windows NT 6.2)/],
            ['Windows 8.1', /(Windows NT 6.3)/],
            ['Windows 10', /(Windows NT 10.0)/],
            ['Windows ME', /Windows ME/],
            ['Open BSD', /OpenBSD/],
            ['Sun OS', /SunOS/],
            ['Linux', /(Linux)|(X11)/],
            ['Mac OS', /(Mac_PowerPC)|(Macintosh)/],
            ['QNX', /QNX/],
            ['BeOS', /BeOS/],
            ['OS/2', /OS\/2/],
            ['Search Bot', /(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves\/Teoma)|(ia_archiver)/]
        ]);
    };
    BrowserDetect.buildRules = function (ruleTuples) {
        return ruleTuples.map(function (tuple) {
            return {
                name: tuple[0],
                rule: tuple[1]
            };
        });
    };
    return BrowserDetect;
}());
exports.BrowserDetect = BrowserDetect;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Loader_1 = __webpack_require__(4);
var LoaderEvent_1 = __webpack_require__(0);
var BaseLoader_1 = __webpack_require__(1);
var ScriptLoader_1 = __webpack_require__(11);
var LoadingQueue = /** @class */ (function (_super) {
    __extends(LoadingQueue, _super);
    function LoadingQueue(asyncScriptExecution, id) {
        var _this = _super.call(this) || this;
        _this._loadersCount = 0;
        _this._completeLoadersCount = 0;
        _this._progress = 0;
        _this._id = id;
        if (_this._id != undefined)
            Loader_1.Loader.addLoader(_this);
        _this._state = BaseLoader_1.BaseLoader.STATE_IDLE;
        _this._asyncScriptExecution = asyncScriptExecution;
        _this._loaders = [];
        _this._scriptLoaders = [];
        return _this;
    }
    LoadingQueue.prototype.isIdle = function () {
        return this._state == BaseLoader_1.BaseLoader.STATE_IDLE;
    };
    LoadingQueue.prototype.isLoading = function () {
        return this._state == BaseLoader_1.BaseLoader.STATE_LOADING;
    };
    LoadingQueue.prototype.isLoaded = function () {
        return this._state == BaseLoader_1.BaseLoader.STATE_LOADED;
    };
    LoadingQueue.prototype.hasError = function () {
        return this._state == BaseLoader_1.BaseLoader.STATE_ERROR;
    };
    LoadingQueue.prototype.addLoader = function (loader) {
        var loadersArray = loader instanceof ScriptLoader_1.ScriptLoader ? this._scriptLoaders : this._loaders;
        var loaderIndex = loadersArray.indexOf(loader);
        if (loaderIndex != -1)
            return;
        loadersArray.push(loader);
        loader.addEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onLoaderLoaded, this);
        loader.addEventListener(LoaderEvent_1.LoaderEvent.PROGRESS, this.onLoaderProgress, this);
        loader.addEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onLoaderError, this);
        this._loadersCount++;
        // TODO: what happens in the other states of the queue and loader
        if (this.isLoading()) {
            loader.load();
        }
    };
    LoadingQueue.prototype.removeLoader = function (loader) {
        var loadersArray = loader instanceof ScriptLoader_1.ScriptLoader ? this._scriptLoaders : this._loaders;
        var loaderIndex = loadersArray.indexOf(loader);
        if (loaderIndex == -1)
            return;
        loadersArray.splice(loaderIndex, 1);
        this.removeLoaderListeners(loader);
        this._loadersCount--;
        // TODO: the state of the removed loader should be checked and the state of
        // the queue updated accordingly
    };
    LoadingQueue.prototype.load = function () {
        if (this._state == BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        else if (this._state == BaseLoader_1.BaseLoader.STATE_LOADED) {
            var self = this;
            // call loaderHasLoaded asynchronously
            setTimeout(function () {
                // the number of complete loaders should be decreased, because "loaderHasLoaded" increases this count and the comparison
                // (total == loaded) will fail
                self._completeLoadersCount--;
                self.loaderHasLoaded();
            }, 0);
            return;
        }
        this._completeLoadersCount = 0;
        this._progress = 0;
        this._state = BaseLoader_1.BaseLoader.STATE_LOADING;
        for (var loaderIndex in this._loaders)
            this._loaders[loaderIndex].load();
        if (this._scriptLoaders.length != 0) {
            if (this._asyncScriptExecution) {
                for (var loaderIndex in this._scriptLoaders)
                    this._scriptLoaders[loaderIndex].load();
            }
            else {
                this._scriptLoaders[0].load();
            }
        }
    };
    LoadingQueue.prototype.stop = function () {
        if (this._state != BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        this._state = BaseLoader_1.BaseLoader.STATE_IDLE;
        var length = this._loaders.length;
        for (var i = 0; i < length; i++)
            this._loaders[i].stop();
        length = this._scriptLoaders.length;
        for (var i = 0; i < length; i++)
            this._scriptLoaders[i].stop();
    };
    LoadingQueue.prototype.getData = function () {
        return null;
    };
    LoadingQueue.prototype.dispose = function () {
        if (this._id != undefined)
            Loader_1.Loader.removeLoader(this);
        var loaderIndex;
        var length = this._loaders.length;
        for (loaderIndex = 0; loaderIndex < length; loaderIndex++) {
            this.disposeLoader(this._loaders[loaderIndex]);
        }
        length = this._scriptLoaders.length;
        for (loaderIndex = 0; loaderIndex < length; loaderIndex++) {
            this.disposeLoader(this._scriptLoaders[loaderIndex]);
        }
        this._loaders.length = 0;
        this._scriptLoaders.length = 0;
        _super.prototype.dispose.call(this);
    };
    LoadingQueue.prototype.disposeLoader = function (loader) {
        this.removeLoaderListeners(loader);
        loader.dispose();
    };
    LoadingQueue.prototype.removeLoaderListeners = function (loader) {
        loader.removeEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onLoaderLoaded, this);
        loader.removeEventListener(LoaderEvent_1.LoaderEvent.PROGRESS, this.onLoaderProgress, this);
        loader.removeEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onLoaderError, this);
    };
    LoadingQueue.prototype.loaderHasLoaded = function (event) {
        if (event) {
            this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.CHILD_COMPLETE, event.data), event.target);
        }
        this._completeLoadersCount++;
        if (this._completeLoadersCount == this._loadersCount) {
            this._state = BaseLoader_1.BaseLoader.STATE_LOADED;
            // all loaders have completed
            this._progress = 1;
            this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, { progress: 1 }));
            this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE));
        }
    };
    LoadingQueue.prototype.onLoaderLoaded = function (event) {
        if (this._state == BaseLoader_1.BaseLoader.STATE_ERROR || this._state == BaseLoader_1.BaseLoader.STATE_IDLE)
            return;
        if (event.target instanceof ScriptLoader_1.ScriptLoader && !this._asyncScriptExecution) {
            var loaderIndex = this._scriptLoaders.indexOf(event.target);
            if (loaderIndex < this._scriptLoaders.length - 1) {
                this._scriptLoaders[loaderIndex + 1].load();
            }
        }
        this.loaderHasLoaded(event);
    };
    LoadingQueue.prototype.onLoaderProgress = function (event) {
        this._progress += event.data.progressChange / this._loadersCount;
        this._progress = Math.min(this._progress, 1);
        if (this._state == BaseLoader_1.BaseLoader.STATE_ERROR || this._state == BaseLoader_1.BaseLoader.STATE_IDLE)
            return;
        var data = {
            progress: this._progress
        };
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.CHILD_PROGRESS, event.data), event.target);
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, data));
    };
    LoadingQueue.prototype.onLoaderError = function (event) {
        this.stop();
        this._state = BaseLoader_1.BaseLoader.STATE_ERROR;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.CHILD_ERROR, event.data), event.target);
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return LoadingQueue;
}(Loader_1.Loader));
exports.LoadingQueue = LoadingQueue;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var LoaderEvent_1 = __webpack_require__(0);
var BaseLoader_1 = __webpack_require__(1);
var ScriptLoader = /** @class */ (function (_super) {
    __extends(ScriptLoader, _super);
    function ScriptLoader(src, cacheTimestamp, prependURL, id) {
        return _super.call(this, src, cacheTimestamp, prependURL, id) || this;
    }
    ScriptLoader.prototype.getData = function () {
        return this._script;
    };
    ScriptLoader.prototype.load = function () {
        if (this._state == BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        else if (this._state == BaseLoader_1.BaseLoader.STATE_LOADED) {
            var self = this;
            setTimeout(function () {
                self.onLoaded();
            }, 0);
            return;
        }
        this._lastProgress = 0;
        this._state = BaseLoader_1.BaseLoader.STATE_LOADING;
        $("head")[0].appendChild(this._script);
    };
    ScriptLoader.prototype.stop = function () {
        if (this._state != BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        this._state = BaseLoader_1.BaseLoader.STATE_IDLE;
    };
    ScriptLoader.prototype.dispose = function () {
        // TODO: check what will happen if we dispose the loader before it
        // has completed, check this for every type of loader
        this._script.onload = null;
        this._script.onerror = null;
        $(this._script).remove();
        _super.prototype.dispose.call(this);
    };
    ScriptLoader.prototype.getResponseType = function () {
        throw "This method is irrelevant for this type of loader.";
    };
    ScriptLoader.prototype.setUpRequest = function () {
        this._script = document.createElement("script");
        this._script.onload = this.onLoaded.bind(this);
        this._script.onerror = this.onError.bind(this);
        var prependURL = this._prependURL ? this._prependURL : "";
        this._script.src = prependURL + this._src + this._getParameters;
    };
    ScriptLoader.prototype.onLoaded = function (event) {
        this._state = BaseLoader_1.BaseLoader.STATE_LOADED;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, { progress: 1, progressChange: 1 }));
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE));
    };
    ScriptLoader.prototype.onError = function (event) {
        this._state = BaseLoader_1.BaseLoader.STATE_ERROR;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return ScriptLoader;
}(BaseLoader_1.BaseLoader));
exports.ScriptLoader = ScriptLoader;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var BaseLoader_1 = __webpack_require__(1);
var LoaderEvent_1 = __webpack_require__(0);
var Utils_1 = __webpack_require__(13);
var PIXILoader = /** @class */ (function (_super) {
    __extends(PIXILoader, _super);
    function PIXILoader(cacheTimestamp, prependURL, id) {
        var _this = _super.call(this, "", cacheTimestamp, prependURL, id) || this;
        _this._textures = {};
        _this._pixiLoader.baseUrl = _this._prependURL ? _this._prependURL : "";
        return _this;
    }
    PIXILoader.prototype.getData = function () {
        return null;
    };
    PIXILoader.prototype.getTexturesCount = function () {
        return Object.keys(this._textures).length;
    };
    PIXILoader.prototype.getTexture = function (name) {
        return this._textures[name];
    };
    PIXILoader.prototype.getTextureSequence = function (sequenceName, extension, texturesCount, sequenceDigitsCount) {
        if (sequenceDigitsCount === void 0) { sequenceDigitsCount = 3; }
        var textures = [];
        if (texturesCount == undefined)
            texturesCount = this.getTexturesCount();
        for (var i = 0; i < texturesCount; i++) {
            textures[i] = this.getTexture(sequenceName +
                Utils_1.Utils.convertToSequenceString(i, sequenceDigitsCount) + "." + extension);
        }
        return textures;
    };
    PIXILoader.prototype.load = function () {
        if (this._state == BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        else if (this._state == BaseLoader_1.BaseLoader.STATE_LOADED) {
            var self = this;
            setTimeout(function () {
                self.onLoaded();
            }, 0);
            return;
        }
        this._lastProgress = 0;
        this._state = BaseLoader_1.BaseLoader.STATE_LOADING;
        this._pixiLoader.load();
    };
    PIXILoader.prototype.stop = function () {
        if (this._state != BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        this._state = BaseLoader_1.BaseLoader.STATE_IDLE;
        this._pixiLoader.reset();
    };
    PIXILoader.prototype.addResource = function (name, src) {
        src = src + this._getParameters;
        this._pixiLoader.add(name, src);
    };
    PIXILoader.prototype.dispose = function () {
        this._pixiLoader.reset();
        this._pixiLoader.off('complete', this.onLoaded, this);
        this._pixiLoader.off('progress', this.onProgress, this);
        this._pixiLoader.off('error', this.onError, this);
        for (var key in this._textures) {
            this._textures[key].destroy(true);
        }
        _super.prototype.dispose.call(this);
    };
    PIXILoader.prototype.getResponseType = function () {
        throw "This method is irrelevant for this type of loader.";
    };
    PIXILoader.prototype.setUpRequest = function () {
        this._pixiLoader = new PIXI.loaders.Loader();
        this._pixiLoader.on('complete', this.onLoaded, this);
        this._pixiLoader.on('progress', this.onProgress, this);
        this._pixiLoader.on('error', this.onError, this);
    };
    PIXILoader.prototype.collectTextures = function () {
        // TODO: check if this works when the image is loaded as base64 string
        var resources = this._pixiLoader.resources;
        for (var resKey in resources) {
            if (resources[resKey].isImage) {
                // check for a json that defines the frames in this image
                var index = resKey.lastIndexOf("_image");
                var jsonName = undefined;
                if (index != -1)
                    jsonName = resKey.slice(0, index);
                if (resources[jsonName] && resources[jsonName].isJson) {
                    // we found a json for this image, extract the textures from the json
                    for (var textureName in resources[jsonName].textures)
                        this._textures[textureName] = resources[jsonName].textures[textureName];
                }
                else {
                    // this image does not have a corresponding json, get the entire
                    // texture for the image
                    this._textures[resKey] = resources[resKey].texture;
                }
            }
        }
    };
    PIXILoader.prototype.onLoaded = function (event) {
        if (event) {
            var data = {
                progress: event.progress / 100,
                progressChange: (event.progress - this._lastProgress) / 100
            };
            this._lastProgress = event.progress;
            this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, data));
            // TODO: remove this loop in pixi 4
            // This loop is needed because even if a resource fails the complete handler will be called.
            // Also if all resources in the loader fail the complete handler is called before the error handler.
            for (var key in this._pixiLoader.resources) {
                if (this._pixiLoader.resources[key].error != null) {
                    this.onError(null);
                    return;
                }
            }
            this.collectTextures();
        }
        else
            this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, { progress: 1, progressChange: 0 }));
        this._state = BaseLoader_1.BaseLoader.STATE_LOADED;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE));
    };
    PIXILoader.prototype.onProgress = function (event) {
        var data = {
            progress: event.progress / 100,
            progressChange: (event.progress - this._lastProgress) / 100
        };
        this._lastProgress = event.progress;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, data));
    };
    PIXILoader.prototype.onError = function (event) {
        // TODO: we can remove this check in pixi 4
        // If we have detected that there is a failed resource in the complete handler
        // the state will already be BaseLoader.STATE_ERROR
        if (this._state == BaseLoader_1.BaseLoader.STATE_ERROR)
            return;
        this._state = BaseLoader_1.BaseLoader.STATE_ERROR;
        // When a PIXI.loading.Loader has multiple resources added to it even if one of them fails
        // the loader will continue to load the others and eventually dispatch a complete event.
        // Thats why if we detected a failed resources here we stop the loader immediately.
        // This way if one resource fails the entire loader is marked as failed.
        this._pixiLoader.reset();
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return PIXILoader;
}(BaseLoader_1.BaseLoader));
exports.PIXILoader = PIXILoader;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.convertToSequenceString = function (num, sequenceDigitsCount) {
        if (sequenceDigitsCount === void 0) { sequenceDigitsCount = 2; }
        var sequenceString = num.toString();
        for (var i = sequenceString.length; i < sequenceDigitsCount; i++) {
            sequenceString = "0".concat(sequenceString);
        }
        return sequenceString;
    };
    Utils.formatMoney = function (num, denom, formatCurrency, noCoins, separator) {
        if (formatCurrency === void 0) { formatCurrency = false; }
        if (noCoins === void 0) { noCoins = false; }
        if (separator === void 0) { separator = ' '; }
        var str = formatCurrency ? (num / 100).toFixed(noCoins ? 0 : 2) : Math.floor(num / denom).toString();
        var arr = str.split(".");
        var n = arr[0];
        var prefix = n.split('');
        if (formatCurrency) {
            var counter = 3;
            var i = prefix.length;
            while (--i > 0) {
                counter--;
                if (counter == 0) {
                    counter = 3;
                    prefix.splice(i, 0, separator);
                }
            }
        }
        if (arr[1])
            return (prefix.join('') + "." + arr[1]);
        else
            return (prefix.join(''));
    };
    /**
     * Returns a random number in the range rangeStart(inclusive) - rangeEnd(exclusive)
     * @param {number} rangeStart - the start of the range (this number can be returned as a result
     * @param {number} rangeEnd - the end of the range (this number will never be returned as a result
     * @example
     * Utils.randomNumberInRange(4, 20) -> returns a number in the range [4, 20)
     *
     * If rangeStart is greater than rangeEnd the exclusivity of the range is swapped
     * Utils.randomNumberInRange(20, 4) -> returns a number in the range (4, 20]
     *
     * It also works with negative numbers
     * Utils.randomNumberInRange(-20, -4) -> returns a number in the range [-20, -4)
     * Utils.randomNumberInRange(-4, -20) -> returns a number in the range (-20, -4]
     */
    Utils.randomNumberInRange = function (rangeStart, rangeEnd) {
        return rangeStart + Math.random() * (rangeEnd - rangeStart);
    };
    return Utils;
}());
exports.Utils = Utils;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var BaseLoader_1 = __webpack_require__(1);
var LoaderEvent_1 = __webpack_require__(0);
var JSONLoader = /** @class */ (function (_super) {
    __extends(JSONLoader, _super);
    function JSONLoader(src, cacheTimestamp, prependURL, id) {
        var _this = _super.call(this, src, cacheTimestamp, prependURL, id) || this;
        _this._data = null;
        return _this;
    }
    JSONLoader.prototype.getData = function () {
        return this._data;
    };
    JSONLoader.prototype.getResponseType = function () {
        return "text";
    };
    JSONLoader.prototype.onLoaded = function (event) {
        this._data = {
            jsonContent: JSON.parse(this._request.response),
            rawContent: this._request.response
        };
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE, this._data));
    };
    JSONLoader.prototype.onError = function (event) {
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return JSONLoader;
}(BaseLoader_1.BaseLoader));
exports.JSONLoader = JSONLoader;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var EventEmitter = /** @class */ (function () {
    function EventEmitter() {
    }
    EventEmitter.prototype.fire = function (eventName, data, namespace, fireImmediately) {
        if (!namespace) {
            namespace = "global";
        }
        // if there are no events in the queue we should treat this as a normal event
        // ignoring the value of fireImmediately, otherwise if other events are fired while
        // processing this event they will not wait in a queue until this event is completely processed
        if (fireImmediately && EventEmitter.eventsQueue.length > 0) {
            this.processEvent(eventName, data, namespace);
        }
        else {
            EventEmitter.eventsQueue.push({
                name: eventName,
                data: data,
                namespace: namespace
            });
            // when the queue contains more than 1 event, this means that there are
            // currently other events that are being processed so we should wait for them
            if (EventEmitter.eventsQueue.length > 1) {
                return;
            }
            // at this point we know that there are no events currently being processed,
            // so we can process this event immediately, and then continue to process events
            // that might have been added to the queue while this event was being processed
            do {
                var event_1 = EventEmitter.eventsQueue[0];
                this.processEvent(event_1.name, event_1.data, event_1.namespace);
                EventEmitter.eventsQueue.shift();
            } while (EventEmitter.eventsQueue.length > 0);
        }
    };
    EventEmitter.prototype.processEvent = function (eventName, data, namespace) {
        var idx, eventObjects;
        try {
            eventObjects = EventEmitter.handlers[namespace];
        }
        catch (e) {
            return; // No handlers yet.
        }
        try {
            idx = eventObjects.findIndex(function (x) { return x.name === eventName.substr(EventEmitter.handlerPrefix.length).split("_")[0]; });
        }
        catch (e) {
            idx = -1;
        }
        if (idx > -1) {
            eventObjects[idx].handlers.forEach(function (handlerObject) {
                handlerObject.handler.call(handlerObject.instance, data);
            });
        }
    };
    EventEmitter.handlers = null;
    EventEmitter.eventsQueue = [];
    EventEmitter.handlerPrefix = 'on';
    return EventEmitter;
}());
exports.EventEmitter = EventEmitter;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Device_1 = __webpack_require__(2);
var MobileView_1 = __webpack_require__(17);
var DeviceOrientation_1 = __webpack_require__(7);
var Button = /** @class */ (function (_super) {
    __extends(Button, _super);
    function Button(textures) {
        var _this = _super.call(this) || this;
        _this._isDown = false;
        _this._isOver = false;
        _this.shouldScale = false;
        _this.textures = textures;
        _this.on(Device_1.Device.resolveEvent("mousedown"), _this.onMouseDown, _this);
        _this.on(Device_1.Device.resolveEvent("mouseup"), _this.onMouseUp, _this);
        _this.on(Device_1.Device.resolveEvent("mouseupoutside"), _this.onMouseUp, _this);
        _this.on(Device_1.Device.resolveEvent("mouseover"), _this.onMouseOver, _this);
        _this.on(Device_1.Device.resolveEvent("mouseout"), _this.onMouseOut, _this);
        _this._background = new PIXI.Sprite(new PIXI.Texture(new PIXI.BaseTexture(null)));
        _this.addChild(_this._background);
        _this.interactive = true;
        _this.buttonMode = true;
        _this.setImageForState(Button.STATE_UP);
        return _this;
    }
    Button.prototype.refreshState = function () {
        this.setImageForState(this._state);
    };
    Button.prototype.dispose = function () {
        this.off(Device_1.Device.resolveEvent("mousedown"), this.onMouseDown, this);
        this.off(Device_1.Device.resolveEvent("mouseup"), this.onMouseUp, this);
        this.off(Device_1.Device.resolveEvent("mouseupoutside"), this.onMouseUp, this);
        this.off(Device_1.Device.resolveEvent("mouseover"), this.onMouseOver, this);
        this.off(Device_1.Device.resolveEvent("mouseout"), this.onMouseOut, this);
        if (this._scaleTween) {
            this._scaleTween.kill();
        }
        //this.destroy();
    };
    Object.defineProperty(Button.prototype, "textures", {
        set: function (value) {
            this._hasTexturesBasedOnOrientation = value[Button.LANDSCAPE_TEXTURES] != null || value[Button.PORTRAIT_TEXTURES] != null;
            if (value[Button.LANDSCAPE_TEXTURES] != null && value[Button.LANDSCAPE_TEXTURES][Button.STATE_UP] == null) {
                throw "At least a 'state up' should be supplied for landscape orientation";
            }
            if (value[Button.PORTRAIT_TEXTURES] != null && value[Button.PORTRAIT_TEXTURES][Button.STATE_UP] == null) {
                throw "At least a 'state up' should be supplied for portrait orientation";
            }
            if (!this._hasTexturesBasedOnOrientation && value[Button.STATE_UP] == null) {
                throw "At least a 'state up' should be supplied";
            }
            this._textures = value;
        },
        enumerable: true,
        configurable: true
    });
    Button.prototype.setImageForState = function (state) {
        var textures = null;
        if (this._hasTexturesBasedOnOrientation) {
            if (MobileView_1.MobileView.deviceOrientation == DeviceOrientation_1.DeviceOrientation.Landscape) {
                textures = this._textures[Button.LANDSCAPE_TEXTURES] || this._textures[Button.PORTRAIT_TEXTURES];
            }
            else {
                textures = this._textures[Button.PORTRAIT_TEXTURES] || this._textures[Button.LANDSCAPE_TEXTURES];
            }
        }
        else {
            textures = this._textures;
        }
        if (textures[state] === undefined) {
            state = Button.STATE_UP;
        }
        // TODO: check this behavior
        if (!this.interactive && textures[Button.STATE_DISABLED] !== undefined)
            state = Button.STATE_DISABLED;
        else if (this.interactive && this._state == Button.STATE_DISABLED)
            state = Button.STATE_UP;
        // else if(!this.interactive && this.textures[Button.STATE_DISABLED] === undefined)
        //     state = Button.STATE_UP;
        this._state = state;
        this._background.texture = textures[state];
    };
    Button.prototype.onMouseDown = function () {
        this._isDown = true;
        this.setImageForState(Button.STATE_DOWN);
    };
    Button.prototype.onMouseUp = function () {
        this._isDown = false;
        if (this._isOver) {
            this.setImageForState(Button.STATE_OVER);
        }
        else {
            this.setImageForState(Button.STATE_UP);
        }
    };
    Button.prototype.onMouseOver = function () {
        this._isOver = true;
        if (!this._isDown) {
            this.setImageForState(Button.STATE_OVER);
        }
        if (this.shouldScale) {
            this._scaleTween = gsap.TweenMax.to(this.scale, 0.3, { x: 1.1, y: 1.1 });
        }
    };
    Button.prototype.onMouseOut = function () {
        this._isOver = false;
        if (!this._isDown) {
            this.setImageForState(Button.STATE_UP);
        }
        if (this.shouldScale && this.scale.x != 1) {
            this._scaleTween = gsap.TweenMax.to(this.scale, 0.3, { x: 1, y: 1 });
        }
    };
    Button.prototype.onRotationChange = function () {
        this.setImageForState(this._state);
    };
    Button.LANDSCAPE_TEXTURES = "landscapeTextures";
    Button.PORTRAIT_TEXTURES = "portraitTextures";
    Button.STATE_UP = "up";
    Button.STATE_DOWN = "down";
    Button.STATE_OVER = "over";
    Button.STATE_DISABLED = "disabled";
    return Button;
}(MobileView_1.MobileView));
exports.Button = Button;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Device_1 = __webpack_require__(2);
/**
 * Wrapper class over PIXI.Container that supports orientation changes.
 */
var MobileView = /** @class */ (function (_super) {
    __extends(MobileView, _super);
    /**
     * Class constructor. Attaches orientation change listener and adds instance to _instances array.
     */
    function MobileView() {
        var _this = _super.call(this) || this;
        if (!MobileView._isListening) {
            MobileView.listenForOrientationChange();
            MobileView._isListening = true;
        }
        MobileView._instances.push(_this);
        return _this;
    }
    MobileView.dispose = function () {
        window.removeEventListener("orientationchange", MobileView.onOrientationChange);
        MobileView._instances.length = 0;
        MobileView._isListening = false;
    };
    Object.defineProperty(MobileView, "deviceOrientation", {
        /**
         * Returns Device Orientation. Portrait or Landscape
         * @returns {com.egt.library.DeviceOrientation}
         */
        get: function () {
            return Device_1.Device.currentOrientation;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Attaches orientationchange listener. Invoked in class constructor.
     */
    MobileView.listenForOrientationChange = function () {
        MobileView._currentOrientation = Device_1.Device.currentOrientation;
        window.addEventListener("orientationchange", MobileView.onOrientationChange);
    };
    /**
     * Changes _currentOrientation
     * Invoked on orientationChange event (listenForOrientationChange)
     */
    MobileView.onOrientationChange = function () {
        var deviceOrientation = MobileView.deviceOrientation;
        if (deviceOrientation !== MobileView._currentOrientation) {
            MobileView._currentOrientation = deviceOrientation;
            MobileView.callOrientationChange();
            return;
        }
    };
    /**
     * calls onRotationChange method in all instances of MobileView
     * Callback function invoked onOrientationChange
     */
    MobileView.callOrientationChange = function () {
        for (var _i = 0, _a = MobileView._instances; _i < _a.length; _i++) {
            var instance = _a[_i];
            instance.onRotationChange();
        }
    };
    /**
     * Removes an instance from the static array of MobileView instances
     * Call this function prior deletion of a MobileView object
     * @returns the removed instance
     */
    MobileView.prototype.removeInstance = function () {
        var _this = this;
        var index = MobileView._instances.findIndex(function (instance) { return instance === _this; });
        if (index > -1) {
            return MobileView._instances.splice(index, 1)[0];
        }
    };
    /**
     * Contains all MobileView instances. Used in callOrientationChange function
     * @type MobileView[]
     */
    MobileView._instances = new Array();
    MobileView._currentOrientation = null;
    MobileView._isListening = false;
    return MobileView;
}(PIXI.Container));
exports.MobileView = MobileView;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var logserver = __webpack_require__(19);
exports.logserver = logserver;
var CanvasConsole = __webpack_require__(20);
exports.CanvasConsole = CanvasConsole;
var log = __webpack_require__(8);
exports.log = log;
var sort = __webpack_require__(21);
exports.sort = sort;
var SoundManager_1 = __webpack_require__(22);
exports.SoundManager = SoundManager_1.SoundManager;
var LoadingQueue_1 = __webpack_require__(10);
exports.LoadingQueue = LoadingQueue_1.LoadingQueue;
var PIXILoader_1 = __webpack_require__(12);
exports.PIXILoader = PIXILoader_1.PIXILoader;
var JSONLoader_1 = __webpack_require__(14);
exports.JSONLoader = JSONLoader_1.JSONLoader;
var LayoutLoader_1 = __webpack_require__(24);
exports.LayoutLoader = LayoutLoader_1.LayoutLoader;
var Loader_1 = __webpack_require__(4);
exports.Loader = Loader_1.Loader;
var ScriptLoader_1 = __webpack_require__(11);
exports.ScriptLoader = ScriptLoader_1.ScriptLoader;
var XMLLoader_1 = __webpack_require__(28);
exports.XMLLoader = XMLLoader_1.XMLLoader;
var Event_1 = __webpack_require__(3);
exports.Event = Event_1.Event;
var EventDispatcher_1 = __webpack_require__(5);
exports.EventDispatcher = EventDispatcher_1.EventDispatcher;
var LoaderEvent_1 = __webpack_require__(0);
exports.LoaderEvent = LoaderEvent_1.LoaderEvent;
var EventEmitter_1 = __webpack_require__(15);
exports.EventEmitter = EventEmitter_1.EventEmitter;
var EventHandler_1 = __webpack_require__(29);
exports.EventHandler = EventHandler_1.EventHandler;
var BrowserDetect_1 = __webpack_require__(9);
exports.BrowserDetect = BrowserDetect_1.BrowserDetect;
var color_1 = __webpack_require__(30);
exports.decimalToHexColorString = color_1.decimalToHexColorString;
var ErrorSerializer_1 = __webpack_require__(31);
exports.ErrorSerializer = ErrorSerializer_1.ErrorSerializer;
var Timer_1 = __webpack_require__(32);
exports.Timer = Timer_1.Timer;
var UncaughtErrorsHandler_1 = __webpack_require__(33);
exports.UncaughtErrorsHandler = UncaughtErrorsHandler_1.UncaughtErrorsHandler;
var Utils_1 = __webpack_require__(13);
exports.Utils = Utils_1.Utils;
var Serializer_1 = __webpack_require__(6);
exports.Serializer = Serializer_1.Serializer;
var Rectangle_1 = __webpack_require__(34);
exports.Rectangle = Rectangle_1.Rectangle;
var BitmapMoneyText_1 = __webpack_require__(35);
exports.BitmapMoneyText = BitmapMoneyText_1.BitmapMoneyText;
var MoneyText_1 = __webpack_require__(36);
exports.MoneyText = MoneyText_1.MoneyText;
var Text_1 = __webpack_require__(37);
exports.Text = Text_1.Text;
var Button_1 = __webpack_require__(16);
exports.Button = Button_1.Button;
var StateButton_1 = __webpack_require__(38);
exports.StateButton = StateButton_1.StateButton;
var RollingNumberView_1 = __webpack_require__(39);
exports.RollingNumberView = RollingNumberView_1.RollingNumberView;
var RollingBitmapNumberView_1 = __webpack_require__(40);
exports.RollingBitmapNumberView = RollingBitmapNumberView_1.RollingBitmapNumberView;
var ScrollView_1 = __webpack_require__(41);
exports.ScrollView = ScrollView_1.ScrollView;
var Device_1 = __webpack_require__(2);
exports.Device = Device_1.Device;
var LayerManager_1 = __webpack_require__(42);
exports.LayerManager = LayerManager_1.LayerManager;
var DeviceOrientation_1 = __webpack_require__(7);
exports.DeviceOrientation = DeviceOrientation_1.DeviceOrientation;
var MobileView_1 = __webpack_require__(17);
exports.MobileView = MobileView_1.MobileView;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrowserDetect_1 = __webpack_require__(9);
var LogServer = /** @class */ (function () {
    function LogServer() {
    }
    LogServer.sendStatisticUsage = function (data) {
        var browserDetect = BrowserDetect_1.BrowserDetect.detect();
        var statisticData;
        if (browserDetect && browserDetect.name) {
            statisticData = {
                browserName: browserDetect.name,
                OS: browserDetect.os
            };
        }
        LogServer.postData(LogServer.serverPostStatistic, __assign({}, data, statisticData));
    };
    LogServer.sendError = function (e) {
        e.projectName = e.projectName || exports.projectMetadata.projectName;
        e.projectBuild = e.projectBuild || exports.projectMetadata.projectBuild;
        LogServer.postData(LogServer.serverPostError, e); //.then(() => console.log("Error logged", e))
    };
    LogServer.sendLog = function (objectToLog) {
        LogServer.postData(LogServer.LogObjectServerAdress, objectToLog);
    };
    LogServer.postData = function (url, data, callback) {
        if (callback === void 0) { callback = null; }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: callback,
            dataType: "json"
        });
        // return fetch(url, {
        //     body: JSON.stringify(data),
        //     cache: 'no-cache',
        //     credentials: 'same-origin',
        //     headers: {
        //         'user-agent': window.navigator.userAgent,
        //         'content-type': 'application/json'
        //     },
        //     method: 'POST',
        //     mode: 'cors',
        //     redirect: 'follow',
        //     referrer: 'no-referrer',
        // })
        //     .then(response => response.json())
        //     .catch(err => console.log(err))
    };
    LogServer.ErrorLogServerAdress = "https://error-handling-server.herokuapp.com";
    LogServer.LogObjectServerAdress = "https://server-object-logger.herokuapp.com/api/log";
    LogServer.serverPostError = LogServer.ErrorLogServerAdress + "/api/errors";
    LogServer.serverPostStatistic = LogServer.ErrorLogServerAdress + "/api/statistic";
    return LogServer;
}());
exports.LogServer = LogServer;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Device_1 = __webpack_require__(2);
var CanvasConsoleConfig = /** @class */ (function () {
    function CanvasConsoleConfig() {
        this.consoleWidth = 1280;
        this.consoleHeight = 720;
        this.consoleAlpha = 0.2;
        this.addHideButton = true;
        this.addScrollButtons = false;
        this.attatchConsoleLog = false;
        this.attatchConsoleError = true;
        this.backgroundColor = 0xfffff;
        this.scrollButtonsColor = 0xffff00;
        this.showOnError = true;
    }
    return CanvasConsoleConfig;
}());
exports.CanvasConsoleConfig = CanvasConsoleConfig;
var CanvasConsole = /** @class */ (function (_super) {
    __extends(CanvasConsole, _super);
    function CanvasConsole(config) {
        var _this = _super.call(this) || this;
        _this.errorCounter = 0;
        CanvasConsole.instance = _this;
        if (!config) {
            config = new CanvasConsoleConfig();
        }
        _this.config = config;
        _this.init();
        _this.attachToConsole();
        _this.hide();
        return _this;
    }
    CanvasConsole.getInstance = function () {
        if (!this.instance) {
            this.instance = new CanvasConsole();
        }
        return CanvasConsole.instance;
    };
    CanvasConsole.prototype.show = function () {
        this.visible = true;
        return this;
    };
    CanvasConsole.prototype.hide = function () {
        this.visible = false;
        return this;
    };
    CanvasConsole.prototype.print = function (message, color, fontSize) {
        if (color === void 0) { color = 0xffffff; }
        if (fontSize === void 0) { fontSize = 30; }
        var text = new PIXI.Text(message, {
            fill: color, fontSize: fontSize,
            wordWrap: true, wordWrapWidth: this.config.consoleWidth - CanvasConsole.TEXT_STARTING_X
        });
        var currentTextHeight = this.consoleContainer.children
            .map(function (textContainer) { return textContainer.height + CanvasConsole.TEXT_Y_SPACING; })
            .reduce(function (totalHeight, currentHeight) { return totalHeight + currentHeight; }, 0);
        var textContainer = new PIXI.Container();
        textContainer.addChild(text);
        textContainer.x = CanvasConsole.TEXT_STARTING_X;
        textContainer.y = CanvasConsole.TEXT_STARTING_Y + currentTextHeight;
        this.consoleContainer.addChild(textContainer);
        if (CanvasConsole.TEXT_STARTING_Y + currentTextHeight > this.config.consoleHeight) {
            this.consoleContainer.y = -currentTextHeight;
        }
        return this;
    };
    CanvasConsole.prototype.clearConsole = function () {
        while (this.consoleContainer.children.length > 0) {
            this.consoleContainer.removeChildAt(0);
        }
        this.errorCounter = 0;
        this.consoleContainer.y = 0;
        return this;
    };
    CanvasConsole.prototype.scrollDown = function (timesScroll) {
        if (timesScroll === void 0) { timesScroll = 1; }
        this.consoleContainer.y -= CanvasConsole.SCROLLING_Y_STEP * timesScroll;
        return this;
    };
    CanvasConsole.prototype.scrollUp = function (timesScroll) {
        if (timesScroll === void 0) { timesScroll = 1; }
        if (this.consoleContainer.y < CanvasConsole.SCROLLING_Y_STEP) {
            this.consoleContainer.y += CanvasConsole.SCROLLING_Y_STEP * timesScroll;
        }
        return this;
    };
    CanvasConsole.prototype.dispose = function () {
        //TODO :
    };
    CanvasConsole.prototype.init = function () {
        var background = new PIXI.Graphics();
        background.beginFill(this.config.backgroundColor, this.config.consoleAlpha);
        background.drawRect(0, 0, this.config.consoleWidth, this.config.consoleHeight);
        background.endFill();
        this.addChild(background);
        this.consoleContainer = new PIXI.Container();
        this.addChild(this.consoleContainer);
        if (this.config.addHideButton) {
            this.hideButton = this.getRect(0x68EFAD, 70, 70);
            this.hideButton.y = this.config.consoleHeight - this.hideButton.height - 80;
            this.hideButton.x = this.config.consoleWidth - this.hideButton.width - 150;
            this.addChild(this.hideButton);
            this.setupHideButtonEvents();
        }
        // scroll buttons
        if (this.config.addScrollButtons) {
            this.scrollDownButton = this.getTriangle(this.config.scrollButtonsColor);
            this.scrollUpButton = this.getTriangle(this.config.scrollButtonsColor);
            this.scrollUpButton.anchor.x = this.scrollUpButton.anchor.y = this.scrollDownButton.anchor.x = this.scrollDownButton.anchor.y = 0.5;
            this.scrollDownButton.x = this.config.consoleWidth - this.scrollDownButton.width - 20;
            this.scrollUpButton.x = this.config.consoleWidth - this.scrollUpButton.width - 20;
            this.scrollDownButton.y = this.config.consoleHeight - this.scrollDownButton.height;
            this.scrollUpButton.y = this.config.consoleHeight - this.scrollUpButton.height - 80;
            this.scrollDownButton.rotation += 3.15;
            this.addChild(this.scrollDownButton);
            this.addChild(this.scrollUpButton);
            this.setupScrollButtonsEvents();
        }
    };
    CanvasConsole.prototype.attachToConsole = function () {
        var _this = this;
        var origLog = console.log;
        var origError = console.error;
        var self = this;
        if (this.config.attatchConsoleLog) {
            console.log = function () {
                this.log(arguments[0]);
                return origLog.apply(this, arguments);
            };
        }
        if (this.config.attatchConsoleError) {
            window.addEventListener("error", function (e) {
                if (self.config.showOnError) {
                    self.show();
                }
                if (e.error.stack) {
                    _this.printError(_this.errorCounter + ". " + e.message + "\n\t" +
                        e.error.stack.split("@").join("\n\t"));
                }
                else {
                    _this.printError(_this.errorCounter + ". Error at line - " + e.lineno + "\n\t" + e.message + "\n\t" + e.filename + "\n\t");
                }
            });
            console.error = function () {
                if (self.config.showOnError) {
                    self.show();
                }
                this.error(arguments[0]);
                return origError.apply(this, arguments);
            };
        }
    };
    CanvasConsole.prototype.printLog = function (message) {
        this.print(message);
    };
    CanvasConsole.prototype.printError = function (message) {
        this.errorCounter++;
        this.print(message, 0xff0000);
    };
    CanvasConsole.prototype.setupScrollButtonsEvents = function (on) {
        if (on === void 0) { on = true; }
        if (on) {
            this.scrollUpButton.on(Device_1.Device.resolveEvent("click"), this.onScrollUpButtonClicked, this);
            this.scrollDownButton.on(Device_1.Device.resolveEvent("click"), this.onScrollDownButtonClicked, this);
        }
        else {
            this.scrollUpButton.off(Device_1.Device.resolveEvent("click"), this.onScrollUpButtonClicked, this);
            this.scrollDownButton.off(Device_1.Device.resolveEvent("click"), this.onScrollDownButtonClicked, this);
        }
    };
    CanvasConsole.prototype.setupHideButtonEvents = function (on) {
        if (on === void 0) { on = true; }
        if (on) {
            this.hideButton.on(Device_1.Device.resolveEvent("click"), this.onHideButtonClicked, this);
        }
        else {
            this.hideButton.off(Device_1.Device.resolveEvent("click"), this.onHideButtonClicked, this);
        }
    };
    CanvasConsole.prototype.onHideButtonClicked = function () {
        this.hide();
    };
    CanvasConsole.prototype.onScrollDownButtonClicked = function () {
        this.scrollDown(2);
    };
    CanvasConsole.prototype.onScrollUpButtonClicked = function () {
        this.scrollUp(2);
    };
    CanvasConsole.prototype.getTriangle = function (color) {
        var triangle = new PIXI.Graphics();
        triangle.beginFill(color);
        triangle.moveTo(25, 0);
        triangle.lineTo(0, 75);
        triangle.lineTo(50, 75);
        triangle.endFill();
        var result = new PIXI.Sprite(triangle.generateCanvasTexture());
        result.interactive = true;
        return result;
    };
    CanvasConsole.prototype.getRect = function (color, width, height) {
        var rect = new PIXI.Graphics();
        rect.beginFill(color, 1);
        rect.drawRect(0, 0, width, height);
        rect.endFill();
        var result = new PIXI.Sprite(rect.generateCanvasTexture());
        result.interactive = true;
        return result;
    };
    CanvasConsole.SCROLLING_Y_STEP = 40;
    CanvasConsole.TEXT_STARTING_X = 10;
    CanvasConsole.TEXT_STARTING_Y = 10;
    CanvasConsole.TEXT_Y_SPACING = 40;
    return CanvasConsole;
}(PIXI.Container));
exports.CanvasConsole = CanvasConsole;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Sort array containing cells by rows and colums.
 * @param {number[]} cells - array that should be sorted.
 * @param {boolean} [isColFirst=true] - indicate which come first in paris, column or row.
 * @example
 *  let specialExpand = [2, 1, 1, 1, 2, 2, 1, 0, 3, 2, 1, 2, 0, 0];
 *  sortListOfCells(specialExpand);
 *  console.log(specialExpand);
 *  // now array `specialExpand` is sorted and looks like [0, 0, 1, 0, 1, 1, 2, 1, 1, 2, 2, 2, 3, 2]
 **/
function sortCells(cells, isColFirst) {
    if (isColFirst === void 0) { isColFirst = true; }
    var lastCellIndex = cells.length - 2;
    var cellIndex = 0, leftCol, leftRow, rightCol, rightRow, isLeftColBigger, isLeftRowBigger, isRowSame, shouldSwap, colIndexOffset, rowIndexOffset;
    if (isColFirst) {
        colIndexOffset = 0;
        rowIndexOffset = 1;
    }
    else {
        colIndexOffset = 1;
        rowIndexOffset = 0;
    }
    for (; cellIndex < lastCellIndex;) {
        leftCol = cells[cellIndex + colIndexOffset];
        leftRow = cells[cellIndex + rowIndexOffset];
        rightCol = cells[cellIndex + 2 + colIndexOffset];
        rightRow = cells[cellIndex + 2 + rowIndexOffset];
        isLeftRowBigger = leftRow > rightRow;
        isLeftColBigger = leftCol > rightCol;
        isRowSame = leftRow === rightRow;
        shouldSwap = isLeftRowBigger || (isRowSame && isLeftColBigger);
        if (shouldSwap) {
            cells.splice(cellIndex, 4, rightCol, rightRow, leftCol, leftRow);
            cellIndex = Math.max(0, cellIndex - 2);
        }
        else {
            cellIndex += 2;
        }
    }
    return cells;
}
exports.sortCells = sortCells;
/**
 * @param {number[]} cells - array of cell pairs that should be split into rows.
 * @param {boolean} [isColFirst=true] - indicate which come first in paris, column or row.
 * @param {boolean} [fillEmptyRows=true] - add empty row(null) if there is no cells in it.
 * @example
 *  const specialExpand = [1,0,1,1,2,1,1,2,2,2,3,2,2,5,4,6];
 *  splitIntoRows(specialExpand);              // [[1,0],[1,1,2,1],[1,2,2,2,3,2],null,null,[2,5],[4,6]]
 *  splitIntoRows(specialExpand, true, true);  // [[1,0],[1,1,2,1],[1,2,2,2,3,2],[2,5],[4,6]]
 **/
function splitIntoRows(cells, isColFirst, fillEmptyRows) {
    if (isColFirst === void 0) { isColFirst = true; }
    if (fillEmptyRows === void 0) { fillEmptyRows = true; }
    var result = [];
    var lastCellIndex = cells.length;
    var cellIndex = 0, col, row, colIndexOffset, rowIndexOffset;
    if (isColFirst) {
        colIndexOffset = 0;
        rowIndexOffset = 1;
    }
    else {
        colIndexOffset = 1;
        rowIndexOffset = 0;
    }
    for (; cellIndex < lastCellIndex; cellIndex += 2) {
        col = cells[cellIndex + colIndexOffset];
        row = cells[cellIndex + rowIndexOffset];
        if (!result[row]) {
            result[row] = [];
        }
        if (isColFirst) {
            result[row].push(col, row);
        }
        else {
            result[row].push(row, col);
        }
    }
    if (!fillEmptyRows) {
        result = result.filter(function (row) { return row; });
    }
    return result;
}
exports.splitIntoRows = splitIntoRows;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SoundLoader_1 = __webpack_require__(23);
var LoaderEvent_1 = __webpack_require__(0);
var EventDispatcher_1 = __webpack_require__(5);
var Device_1 = __webpack_require__(2);
var Event_1 = __webpack_require__(3);
var log = __webpack_require__(8);
var SoundManager = /** @class */ (function (_super) {
    __extends(SoundManager, _super);
    function SoundManager(cacheTimestamp) {
        var _this = _super.call(this) || this;
        _this._soundLoaders = {};
        _this._soundSources = {};
        _this._sounds = {};
        _this._soundBuffers = {};
        _this._mute = true;
        _this._cacheTimestamp = cacheTimestamp;
        if (Device_1.Device.isSoundSupported()) {
            // there is a limit of AudioContext instance that can exist
            // so make sure to use one instance
            // TODO: this class should be moved in the library in package sound
            // and audioContextInstance should not be global
            if (window.audioContextInstance == null)
                window.audioContextInstance = new (Device_1.Device.getAudioContext())();
            _this._audioContext = window.audioContextInstance;
            // TODO: test volume                 on all devices
            //                if(!this._audioContext.                createGainNode)
            //                    this._audioContext.createGainNode = this._audioContext.createGain;
            _this._masterGain = _this._audioContext.createGain();
            _this._masterGain.connect(_this._audioContext.destination);
            _this._masterGain.gain.value = 1;
        }
        return _this;
    }
    SoundManager_1 = SoundManager;
    SoundManager.prototype.playDummySound = function (dummyBuffer) {
        if (this._audioContext) {
            var source = this._audioContext.createBufferSource();
            source.buffer = dummyBuffer;
            source.connect(this._audioContext.destination);
            source.start(0, 0, 1);
        }
    };
    SoundManager.prototype.registerSounds = function (sounds, prependURL, cacheTimestamp) {
        if (sounds.length == 0)
            return;
        var len = sounds.length;
        for (var i = 0; i < len; i++) {
            var id = sounds[i].id || sounds[i].src;
            // don`t override already existing sounds
            if (this._soundSources[id]) {
                this.warn('Sound[%s] already registered', sounds[i].src);
                continue;
            }
            this._soundSources[id] = sounds[i].src;
            var soundGroup = sounds[i].sounds;
            var len2 = soundGroup.length;
            for (var j = 0; j < len2; j++) {
                var obj = soundGroup[j];
                this._sounds[obj.name] = { start: obj.start, duration: obj.duration, soundId: id, name: obj.name };
            }
            this.log('Loading sound[%s]...', sounds[i].src);
            var soundLoader = new SoundLoader_1.SoundLoader(this._soundSources[id], cacheTimestamp || this._cacheTimestamp, prependURL, id);
            soundLoader.addEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onSoundLoaded, this);
            soundLoader.addEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onSoundError, this);
            this._soundLoaders[id] = soundLoader;
        }
    };
    SoundManager.prototype.hasSound = function (soundName) {
        if (!this._sounds[soundName])
            return false;
        return true;
    };
    SoundManager.prototype.isSoundLoaded = function (soundName) {
        // TODO: check the second operand
        if (!this._sounds[soundName] || !this._soundBuffers[this._sounds[soundName].soundId])
            return false;
        return true;
    };
    SoundManager.prototype.load = function (id) {
        if (!this._audioContext || this._soundBuffers[id] !== undefined)
            return;
        this._soundBuffers[id] = null;
        this._soundLoaders[id].load();
    };
    SoundManager.prototype.loadAll = function () {
        for (var key in this._soundSources) {
            this.load(key);
        }
    };
    SoundManager.prototype.getSoundDuration = function (soundName) {
        var soundInfo = this._sounds[soundName];
        if (!soundName || !this._audioContext || !soundInfo ||
            !this._soundBuffers[soundInfo.soundId])
            return -1;
        return soundInfo.duration;
    };
    SoundManager.prototype.play = function (soundName, loop, volume) {
        if (loop === void 0) { loop = false; }
        if (volume === void 0) { volume = 1; }
        if (!soundName || !this._audioContext || !this._sounds[soundName] ||
            !this._soundBuffers[this._sounds[soundName].soundId]) {
            this.warn('Unable to play sound[%s]', soundName);
            return null;
        }
        if (this._audioContext.state == "suspended" && this._audioContext.resume !== undefined) {
            this._audioContext.resume();
        }
        var soundInfo = this._sounds[soundName];
        var source = this._audioContext.createBufferSource();
        source.buffer = this._soundBuffers[soundInfo.soundId];
        var instance = new SoundInstance(source, this._masterGain, soundInfo, loop);
        instance.volume = volume;
        return instance;
    };
    SoundManager.prototype.playStream = function (soundURI) {
        var _this = this;
        if (!soundURI || !this._audioContext) {
            this.warn('Unable to stream sound[%s]', soundURI);
            return null;
        }
        var audioElement = document.createElement("audio");
        audioElement.crossOrigin = "anonymous";
        var source = this._audioContext.createMediaElementSource(audioElement);
        audioElement.muted = this.isMuted;
        source.connect(this._audioContext.destination);
        audioElement.src = soundURI;
        audioElement.autoplay = true;
        audioElement.addEventListener("ended", function () {
            source.disconnect(_this._audioContext.destination);
        });
        return new StreamSoundInstance(source, this._masterGain, audioElement);
    };
    Object.defineProperty(SoundManager.prototype, "isContextSuspended", {
        get: function () {
            return this._audioContext && this._audioContext.state == "suspended" ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SoundManager.prototype, "isMuted", {
        get: function () {
            return this._mute;
        },
        set: function (value) {
            if (!this._audioContext)
                return;
            this._mute = value;
            if (this._mute)
                this._masterGain.gain.value = 0;
            else
                this._masterGain.gain.value = 1;
        },
        enumerable: true,
        configurable: true
    });
    SoundManager.prototype.dispose = function () {
        for (var key in this._soundLoaders) {
            var soundLoader = this._soundLoaders[key];
            soundLoader.removeEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onSoundLoaded, this);
            soundLoader.removeEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onSoundError, this);
            soundLoader.dispose();
        }
        _super.prototype.dispose.call(this);
    };
    SoundManager.prototype.onSoundLoaded = function (event) {
        event.target.removeEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onSoundLoaded, this);
        event.target.removeEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onSoundError, this);
        var id = event.target.id;
        this.log('Sound loaded[%s]', this._soundSources[id]);
        this._soundBuffers[id] = event.data;
        this.dispatchEvent(new Event_1.Event(SoundManager_1.SOUND_LOADED, { soundId: id }));
    };
    SoundManager.prototype.onSoundError = function (event) {
        this.error('Unable to load sound[%s]', event.target.src);
        event.target.removeEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onSoundLoaded, this);
        event.target.removeEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onSoundError, this);
    };
    var SoundManager_1;
    SoundManager.SOUND_LOADED = "soundLoaded";
    SoundManager = SoundManager_1 = __decorate([
        log.decoratorConfig({ style: 'color: #A3D9FF' })
    ], SoundManager);
    return SoundManager;
}(EventDispatcher_1.EventDispatcher));
exports.SoundManager = SoundManager;
var StreamSoundInstance = /** @class */ (function (_super) {
    __extends(StreamSoundInstance, _super);
    function StreamSoundInstance(source, masterGain, audioElement) {
        var _this = _super.call(this) || this;
        _this._hasSoundEned = false;
        _this._gainNode = source.context.createGain();
        _this._gainNode.connect(masterGain);
        source.connect(_this._gainNode);
        _this._source = source;
        _this._audioElement = audioElement;
        return _this;
    }
    Object.defineProperty(StreamSoundInstance.prototype, "volume", {
        get: function () {
            return this._gainNode.gain.value;
        },
        set: function (value) {
            value = Math.min(Math.max(value, 0), 1);
            this._gainNode.gain.value = value;
            this._audioElement.volume = value;
        },
        enumerable: true,
        configurable: true
    });
    StreamSoundInstance.prototype.stop = function () {
        this._audioElement.pause();
    };
    StreamSoundInstance.prototype.onSoundEnded = function () {
        this.dispatchEvent(new Event_1.Event("ended"));
    };
    return StreamSoundInstance;
}(EventDispatcher_1.EventDispatcher));
var SoundInstance = /** @class */ (function (_super) {
    __extends(SoundInstance, _super);
    function SoundInstance(source, masterGain, soundInfo, loop) {
        var _this = _super.call(this) || this;
        _this._hasSoundEnded = false;
        _this._gainNode = source.context.createGain();
        _this._gainNode.connect(masterGain);
        source.connect(_this._gainNode);
        source.loop = loop;
        source.loopStart = soundInfo.start;
        source.loopEnd = soundInfo.start + soundInfo.duration;
        source.onended = _this.onSoundEnded.bind(_this);
        _this._source = source;
        _this._soundInfo = soundInfo;
        _this.log('Started');
        if (loop) {
            _this._source.start(0, _this._soundInfo.start);
        }
        else {
            _this._onEndDelay = gsap.TweenMax.delayedCall(soundInfo.duration, _this.onSoundEnded, null, _this);
            _this._source.start(0, _this._soundInfo.start, _this._soundInfo.duration);
        }
        return _this;
    }
    Object.defineProperty(SoundInstance.prototype, "volume", {
        get: function () {
            return this._gainNode.gain.value;
        },
        set: function (value) {
            value = Math.min(Math.max(value, 0), 1);
            this._gainNode.gain.value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SoundInstance.prototype, "playbackRate", {
        get: function () {
            return this._source.playbackRate.value;
        },
        // TODO: test if this value is readonly
        set: function (value) {
            value = Math.abs(value);
            this._source.playbackRate.value = value;
        },
        enumerable: true,
        configurable: true
    });
    SoundInstance.prototype.stop = function (suppressEndedEvent) {
        if (suppressEndedEvent === void 0) { suppressEndedEvent = false; }
        if (!this._isStopped) {
            this.log('Stopped');
            this._isStopped = true;
            this._hasSoundEnded = suppressEndedEvent;
            this._source.stop();
            if (this._onEndDelay != null) {
                this._onEndDelay.totalTime(this._onEndDelay.totalDuration());
            }
        }
    };
    SoundInstance.prototype.onSoundEnded = function () {
        if (this._onEndDelay) {
            this._onEndDelay.kill();
            this._onEndDelay = null;
        }
        if (!this._hasSoundEnded) {
            this.log('Ended');
            this._hasSoundEnded = true;
            this.dispatchEvent(new Event_1.Event("ended"));
        }
    };
    SoundInstance.prototype.toString = function () {
        return this.constructor['name'] + " " + this._soundInfo.soundId + "/" + this._soundInfo.name;
    };
    SoundInstance = __decorate([
        log.decoratorConfig({ style: 'color: #006E90' })
    ], SoundInstance);
    return SoundInstance;
}(EventDispatcher_1.EventDispatcher));


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Device_1 = __webpack_require__(2);
var LoaderEvent_1 = __webpack_require__(0);
var BaseLoader_1 = __webpack_require__(1);
var SoundLoader = /** @class */ (function (_super) {
    __extends(SoundLoader, _super);
    function SoundLoader(src, cacheTimestamp, prependURL, id) {
        var _this = _super.call(this, src, cacheTimestamp, prependURL, id) || this;
        _this._data = null;
        return _this;
    }
    SoundLoader.prototype.getData = function () {
        return this._data;
    };
    SoundLoader.prototype.getResponseType = function () {
        return "arraybuffer";
    };
    SoundLoader.prototype.onLoaded = function (event) {
        // TODO: make sure all loaders handle this case correctly
        if (event == undefined) {
            // the "load" method was called but the content has already been loaded
            this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE, this._data));
            return;
        }
        // TODO: if there is no sound support, don`t even make http request
        if (!Device_1.Device.isSoundSupported()) {
            this._state = BaseLoader_1.BaseLoader.STATE_ERROR;
            this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
        }
        else {
            var self = this;
            // TODO: remove audioContextInstance, create package sound in the library
            // and create classes that share this instance, it should not be global
            if (window.audioContextInstance == null) {
                window.audioContextInstance = new (Device_1.Device.getAudioContext())();
            }
            var audioContext = window.audioContextInstance;
            audioContext.decodeAudioData(this._request.response, function (buffer) {
                self.onBufferDecoded(buffer);
            }, function () {
                self.onBufferDecodeError();
            });
        }
    };
    // TODO: specify the type of error that has occured(loading erro, decoding error....)
    SoundLoader.prototype.onError = function (event) {
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    SoundLoader.prototype.onBufferDecoded = function (buffer) {
        this._data = buffer;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE, this._data));
    };
    SoundLoader.prototype.onBufferDecodeError = function () {
        this._state = BaseLoader_1.BaseLoader.STATE_ERROR;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return SoundLoader;
}(BaseLoader_1.BaseLoader));
exports.SoundLoader = SoundLoader;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var BaseLoader_1 = __webpack_require__(1);
var LoaderEvent_1 = __webpack_require__(0);
var JSONLoader_1 = __webpack_require__(14);
var LoadingQueue_1 = __webpack_require__(10);
var PIXILoader_1 = __webpack_require__(12);
var VideoLoader_1 = __webpack_require__(25);
var LayoutImporter_1 = __webpack_require__(26);
var Loader_1 = __webpack_require__(4);
var FontLoader_1 = __webpack_require__(27);
var LayoutLoader = /** @class */ (function (_super) {
    __extends(LayoutLoader, _super);
    function LayoutLoader(src, cacheTimestamp, prependURL, id) {
        return _super.call(this, src, cacheTimestamp, prependURL, id) || this;
    }
    LayoutLoader.prototype.getData = function () {
        return this._data;
    };
    LayoutLoader.prototype.getTexture = function (name) {
        var texture = undefined;
        if (this._pixiLoader) {
            texture = this._pixiLoader.getTexture(name);
        }
        if (this._videoAlternativeLoader && !texture) {
            texture = this._videoAlternativeLoader.getTexture(name);
        }
        return texture;
    };
    LayoutLoader.prototype.getTextureSequence = function (sequenceName, extension, texturesCount, sequenceDigitsCount) {
        if (sequenceDigitsCount === void 0) { sequenceDigitsCount = 3; }
        if (this._pixiLoader) {
            return this._pixiLoader.getTextureSequence(sequenceName, extension, texturesCount, sequenceDigitsCount);
        }
        return undefined;
    };
    LayoutLoader.prototype.load = function () {
        if (this._state == BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        else if (this._state == BaseLoader_1.BaseLoader.STATE_LOADED) {
            // TODO: remove everithing with self use bind instead
            var self = this;
            setTimeout(function () {
                self.onLoaded();
            }, 0);
            return;
        }
        this._lastProgress = 0;
        this._state = BaseLoader_1.BaseLoader.STATE_LOADING;
        this._jsonLoader.load();
    };
    LayoutLoader.prototype.stop = function () {
        if (this._state != BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        this._state = BaseLoader_1.BaseLoader.STATE_IDLE;
        this._jsonLoader.stop();
        if (this._resourcesLoader)
            this._resourcesLoader.stop();
    };
    LayoutLoader.prototype.dispose = function () {
        this._jsonLoader.removeEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onJSONLoaded, this);
        this._jsonLoader.removeEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onError, this);
        this._jsonLoader.dispose();
        if (this._resourcesLoader) {
            this._resourcesLoader.removeEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onResourcesLoaded, this);
            this._resourcesLoader.removeEventListener(LoaderEvent_1.LoaderEvent.PROGRESS, this.onProgress, this);
            this._resourcesLoader.removeEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onError, this);
            this._resourcesLoader.dispose();
        }
        if (this._videoLoaders) {
            for (var i = 0; i < this._videoLoaders.length; i++) {
                this._videoLoaders[i].removeEventListener(LoaderEvent_1.LoaderEvent.DECODE_ERROR, this.onVideoDecodeError, this);
            }
        }
        if (this._data) {
            this._data.mainContainer.destroy(true);
        }
        _super.prototype.dispose.call(this);
    };
    LayoutLoader.prototype.getResponseType = function () {
        throw "This method is irrelevant for this type of loader.";
    };
    LayoutLoader.prototype.setUpRequest = function () {
        this._jsonLoader = new JSONLoader_1.JSONLoader(this._src, this._cacheTimestamp, this._prependURL, "layoutJSONLoader_" + this._id);
        this._jsonLoader.addEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onJSONLoaded, this);
        this._jsonLoader.addEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onError, this);
    };
    LayoutLoader.prototype.parseLayout = function () {
        var importer = new LayoutImporter_1.LayoutImporter();
        this._data = importer.import(this._jsonLoader.getData().jsonContent, Loader_1.Loader.getLoader("layoutPIXILoader" + this._id));
    };
    LayoutLoader.prototype.onJSONLoaded = function (event) {
        var resources = event.data.jsonContent.resources;
        var fontStyles = event.data.jsonContent.fontStyles;
        var fontNames = event.data.jsonContent.fontNames;
        var bitmapFonts = event.data.jsonContent.bitmapFonts;
        var videos = event.data.jsonContent.videos ? event.data.jsonContent.videos : [];
        if (resources.length == 0 && fontStyles.length == 0 && bitmapFonts.length == 0 && videos.length == 0) {
            this.parseLayout();
            this.onLoaded();
            return;
        }
        this._resourcesLoader = new LoadingQueue_1.LoadingQueue();
        var length = fontStyles.length;
        for (var i = 0; i < length; i++) {
            this._resourcesLoader.addLoader(new FontLoader_1.FontLoader(fontNames[i], fontStyles[i]));
        }
        if (resources.length != 0 || bitmapFonts.length != 0) {
            var lastSlashIndex = this._src.lastIndexOf("/");
            var prependURL = this._src.substring(0, lastSlashIndex);
            this._pixiLoader = new PIXILoader_1.PIXILoader(this._cacheTimestamp, prependURL, "layoutPIXILoader" + this._id);
            this._resourcesLoader.addLoader(this._pixiLoader);
        }
        if (videos.length != 0) {
            this._videoLoaders = [];
            length = videos.length;
            for (var i = 0; i < length; i++) {
                var lastSlashIndex = this._src.lastIndexOf("/");
                var prependURL = this._src.substring(0, lastSlashIndex) + "/images/";
                var videoLoader = new VideoLoader_1.VideoLoader(videos[i], this._cacheTimestamp, prependURL);
                videoLoader.addEventListener(LoaderEvent_1.LoaderEvent.DECODE_ERROR, this.onVideoDecodeError, this);
                this._videoLoaders.push(videoLoader);
                this._resourcesLoader.addLoader(videoLoader);
            }
        }
        length = resources.length;
        for (var i = 0; i < length; i++) {
            this._pixiLoader.addResource(resources[i], "images/" + resources[i]);
        }
        length = bitmapFonts.length;
        for (var i = 0; i < length; i++) {
            this._pixiLoader.addResource(bitmapFonts[i], "images/" + bitmapFonts[i] + ".fnt");
        }
        this._resourcesLoader.addEventListener(LoaderEvent_1.LoaderEvent.COMPLETE, this.onResourcesLoaded, this);
        this._resourcesLoader.addEventListener(LoaderEvent_1.LoaderEvent.PROGRESS, this.onProgress, this);
        this._resourcesLoader.addEventListener(LoaderEvent_1.LoaderEvent.ERROR, this.onError, this);
        this._resourcesLoader.load();
    };
    LayoutLoader.prototype.onResourcesLoaded = function (event) {
        this.parseLayout();
        this.onLoaded();
    };
    LayoutLoader.prototype.onLoaded = function () {
        this._state = BaseLoader_1.BaseLoader.STATE_LOADED;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, { progress: 1, progressChange: 1 - this._lastProgress }));
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE));
    };
    LayoutLoader.prototype.onProgress = function (event) {
        var data = {
            progress: event.data.progress,
            progressChange: event.data.progress - this._lastProgress
        };
        this._lastProgress = event.data.progress;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, data));
    };
    LayoutLoader.prototype.onError = function (event) {
        this._state = BaseLoader_1.BaseLoader.STATE_ERROR;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    LayoutLoader.prototype.onVideoDecodeError = function () {
        var videoAlternative = this._jsonLoader.getData().jsonContent.videoAlternative;
        for (var i = 0; i < this._videoLoaders.length; i++) {
            var loader = this._videoLoaders[i];
            this._resourcesLoader.removeLoader(loader);
            loader.removeEventListener(LoaderEvent_1.LoaderEvent.DECODE_ERROR, this.onVideoDecodeError, this);
            loader.dispose();
        }
        this._videoLoaders.length = 0;
        if (videoAlternative && videoAlternative.length != 0) {
            var lastSlashIndex = this._src.lastIndexOf("/");
            var prependURL = this._src.substring(0, lastSlashIndex);
            this._videoAlternativeLoader = new PIXILoader_1.PIXILoader(this._cacheTimestamp, prependURL, "videoAlternativeLoader" + this._id);
            for (var i = 0; i < videoAlternative.length; i++) {
                this._videoAlternativeLoader.addResource(videoAlternative[i], "images/" + videoAlternative[i]);
            }
            this._resourcesLoader.addLoader(this._videoAlternativeLoader);
        }
        else {
            throw new Error("Video alternative should always be provided");
        }
    };
    return LayoutLoader;
}(BaseLoader_1.BaseLoader));
exports.LayoutLoader = LayoutLoader;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Created by plamen on 11-May-17.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var LoaderEvent_1 = __webpack_require__(0);
var BaseLoader_1 = __webpack_require__(1);
var VideoLoader = /** @class */ (function (_super) {
    __extends(VideoLoader, _super);
    function VideoLoader(src, cacheTimestamp, prependURL, id) {
        var _this = _super.call(this, src, cacheTimestamp, prependURL, id) || this;
        _this._video = null;
        return _this;
    }
    VideoLoader.prototype.getData = function () {
        return this._video;
    };
    VideoLoader.prototype.getResponseType = function () {
        return "blob";
    };
    VideoLoader.prototype.onLoaded = function (event) {
        this._video = document.createElement("video");
        // Video is now downloaded
        // and we can set it as source on the video element
        this._video.src = URL.createObjectURL(this._request.response); // IE10+
        this._video.oncanplay = this.onCanPlay.bind(this);
        this._video.oncanplaythrough = this.onCanPlay.bind(this);
        this._video.onerror = this.onDecodeError.bind(this);
    };
    VideoLoader.prototype.onCanPlay = function () {
        this._video.oncanplay = null;
        this._video.oncanplaythrough = null;
        this._video.width = this._video.videoWidth;
        this._video.height = this._video.videoHeight;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE));
    };
    VideoLoader.prototype.onDecodeError = function () {
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.DECODE_ERROR));
    };
    VideoLoader.prototype.onError = function (event) {
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return VideoLoader;
}(BaseLoader_1.BaseLoader));
exports.VideoLoader = VideoLoader;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var LayoutImporter = /** @class */ (function () {
    function LayoutImporter() {
    }
    LayoutImporter.prototype.import = function (json, loader) {
        this._elementsWithID = {};
        this._pixiLoader = loader;
        this._mainContainer = this.importElements(json);
        return {
            "elementsWithID": this._elementsWithID,
            "mainContainer": this._mainContainer
        };
    };
    LayoutImporter.prototype.importElements = function (json) {
        var parent = this.switchType(json);
        if (json.children) {
            for (var i = 0; i < json.children.length; i++) {
                var child = this.importElements(json.children[i]);
                this.checkForIDandAdd(child, json.children[i], parent);
            }
        }
        return parent;
    };
    LayoutImporter.prototype.switchType = function (json) {
        var element;
        switch (json.type) {
            case "rectangle":
                element = this.createRectangle(json);
                break;
            case "roundedRect":
                element = this.createRoundedRectangle(json);
                break;
            case "image":
                element = this.createImage(json);
                break;
            case "text":
                element = this.createText(json);
                break;
            case "BText":
                element = this.createBitmapText(json);
                break;
            default:
                element = new PIXI.Container();
        }
        return element;
    };
    LayoutImporter.prototype.createRectangle = function (props) {
        var rect = new PIXI.Graphics();
        rect.lineStyle(props.strokeWidth, props.strokeColor);
        rect.beginFill(props.fillColor, props.graphicsAlpha);
        rect.drawRect(0, 0, props.width, props.height);
        rect.endFill();
        rect.position = { x: props.x, y: props.y };
        return rect;
    };
    LayoutImporter.prototype.createRoundedRectangle = function (props) {
        var rect = new PIXI.Graphics();
        rect.lineStyle(props.strokeWidth, props.strokeColor);
        rect.beginFill(props.fillColor);
        rect.drawRoundedRect(0, 0, props.width, props.height, props.radius);
        rect.endFill();
        rect.position = { x: props.x, y: props.y };
        return rect;
    };
    LayoutImporter.prototype.createImage = function (props) {
        var img = new PIXI.Sprite();
        img.texture = this._pixiLoader.getTexture(props.picID);
        img.position.x = props.x;
        img.position.y = props.y;
        img.scale = new PIXI.Point(props.scale, props.scale);
        //img.texture.frame = new PIXI.Rectangle(props.frameX, props.frameY, props.frameWidth, props.frameHeight);
        return img;
    };
    LayoutImporter.prototype.createText = function (props) {
        var style = {
            fontSize: props.fontSize,
            stroke: props.strokeColor,
            fill: props.fillColor,
            strokeThickness: props.strokeWidth,
            align: props.align,
            lineHeight: props.lineHeight,
            fontFamily: props.fontFamily,
            dropShadow: props.dropShadow,
            dropShadowBlur: props.shadow,
            dropShadowDistance: props.dropShadowDistance,
            dropShadowColor: props.shadowColor,
            fontWeight: props.fontWeight,
            wordWrap: props.wordWrap,
            wordWrapWidth: props.wordWrapWidth
        };
        var text = new PIXI.Text(props.text, style);
        text.position.x = props.x;
        text.position.y = props.y;
        return text;
    };
    LayoutImporter.prototype.createBitmapText = function (props) {
        var style = {
            font: {
                name: props.fontFamily,
                size: props.fontSize
            },
            align: props.align,
        };
        var text = new PIXI.extras.BitmapText(props.text, style);
        text.position.x = props.x;
        text.position.y = props.y;
        return text;
    };
    LayoutImporter.prototype.checkForIDandAdd = function (element, props, parent) {
        if (props.id != "" && props.id != null) {
            this._elementsWithID[props.id] = element;
            parent[props.id] = element;
        }
        parent.addChild(element);
    };
    return LayoutImporter;
}());
exports.LayoutImporter = LayoutImporter;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var BaseLoader_1 = __webpack_require__(1);
var LoaderEvent_1 = __webpack_require__(0);
// TODO: test this class for correct disposing
var FontLoader = /** @class */ (function (_super) {
    __extends(FontLoader, _super);
    function FontLoader(fontFamily, fontContent) {
        var _this = _super.call(this, "") || this;
        _this._fontFamily = fontFamily;
        _this._fontContent = fontContent;
        return _this;
    }
    FontLoader.prototype.getData = function () {
        return null;
    };
    FontLoader.prototype.load = function () {
        if (this._state == BaseLoader_1.BaseLoader.STATE_LOADING)
            return;
        else if (this._state == BaseLoader_1.BaseLoader.STATE_LOADED) {
            var self_1 = this;
            setTimeout(function () {
                self_1.onLoaded();
            }, 0);
            return;
        }
        this._styleTag.remove();
        this._styleTag.html(this._fontContent);
        $("head").append(this._styleTag);
        var self = this;
        window["WebFont"].load({
            custom: {
                families: [this._fontFamily]
            },
            loading: function () { },
            active: function () {
                self.onLoaded();
            }
        });
    };
    FontLoader.prototype.dispose = function () {
        this._styleTag.remove();
        this._styleTag = null;
        _super.prototype.dispose.call(this);
    };
    FontLoader.prototype.getResponseType = function () {
        throw "This method is irrelevant for this type of loader.";
    };
    FontLoader.prototype.setUpRequest = function () {
        this._styleTag = $("<style>");
    };
    FontLoader.prototype.onLoaded = function (event) {
        this._state = BaseLoader_1.BaseLoader.STATE_LOADED;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.PROGRESS, { progress: 1, progressChange: 1 }));
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE));
    };
    FontLoader.prototype.onError = function (event) {
        this._state = BaseLoader_1.BaseLoader.STATE_ERROR;
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return FontLoader;
}(BaseLoader_1.BaseLoader));
exports.FontLoader = FontLoader;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var LoaderEvent_1 = __webpack_require__(0);
var BaseLoader_1 = __webpack_require__(1);
var XMLLoader = /** @class */ (function (_super) {
    __extends(XMLLoader, _super);
    function XMLLoader(src, cacheTimestamp, prependURL, id) {
        return _super.call(this, src, cacheTimestamp, prependURL, id) || this;
    }
    XMLLoader.prototype.getData = function () {
        return this._request.response;
    };
    XMLLoader.prototype.getResponseType = function () {
        return "text";
    };
    XMLLoader.prototype.onLoaded = function (event) {
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.COMPLETE, this._request.response));
    };
    XMLLoader.prototype.onError = function (event) {
        this.dispatchEvent(new LoaderEvent_1.LoaderEvent(LoaderEvent_1.LoaderEvent.ERROR));
    };
    return XMLLoader;
}(BaseLoader_1.BaseLoader));
exports.XMLLoader = XMLLoader;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmitter_1 = __webpack_require__(15);
var EventHandler = /** @class */ (function (_super) {
    __extends(EventHandler, _super);
    function EventHandler() {
        var _this = _super.call(this) || this;
        // If modules are compiled for es6 (or higher) as a target, all handlers go into the prototype of the constructor.
        if (!_this.registerEventHandlers()) {
            _this.registerEventHandlersES6(_this);
        }
        return _this;
    }
    Object.defineProperty(EventHandler.prototype, "internalNamespace", {
        get: function () {
            return this.constructor.prototype.constructor.name;
        },
        enumerable: true,
        configurable: true
    });
    EventHandler.prototype.registerEventHandlers = function () {
        var handlersSet = false;
        for (var prop in this) {
            if (typeof this[prop] === "function") {
                if (EventHandler.handlerTest.test(prop)) {
                    var handler = prop.split("_");
                    this.registerHandler(handler[0].slice(EventEmitter_1.EventEmitter.handlerPrefix.length), this[prop], handler[1]);
                    handlersSet = true;
                }
                else if (EventHandler.internalHandlerTest.test(prop)) {
                    this.registerHandler(prop.slice(EventEmitter_1.EventEmitter.handlerPrefix.length + 2), this[prop], this.internalNamespace);
                    handlersSet = true;
                }
            }
        }
        return handlersSet;
    };
    EventHandler.prototype.registerEventHandlersES6 = function (instance) {
        var vInstance = instance.constructor.prototype;
        var instanceProperties = [];
        try {
            instanceProperties = Object.getOwnPropertyDescriptors(vInstance);
        }
        catch (e) {
            console.error("Function is not supported on this browser.");
            return;
        }
        for (var prop in instanceProperties) {
            if (typeof instance[prop] === "function") {
                if (EventHandler.handlerTest.test(prop)) {
                    var handler = prop.split("_");
                    this.registerHandler(handler[0].slice(EventEmitter_1.EventEmitter.handlerPrefix.length), instance[prop], handler[1]);
                }
                else if (EventHandler.handlerTest.test(prop)) {
                    this.registerHandler(prop.slice(EventEmitter_1.EventEmitter.handlerPrefix.length + 2), instance[prop], this.internalNamespace);
                }
            }
        }
    };
    EventHandler.prototype.registerHandler = function (event, handler, namespace) {
        if (!namespace) {
            namespace = 'global';
        }
        var handlerObject = {};
        handlerObject["handler"] = handler;
        handlerObject["instance"] = this;
        if (!EventEmitter_1.EventEmitter.handlers) {
            EventEmitter_1.EventEmitter.handlers = {};
            var nameSpaceObject = {};
            var eventObject = {};
            eventObject["name"] = event;
            eventObject["handlers"] = [];
            eventObject["handlers"].push(handlerObject);
            nameSpaceObject[namespace] = [];
            nameSpaceObject[namespace].push(eventObject);
            EventEmitter_1.EventEmitter.handlers = nameSpaceObject;
        }
        else {
            var eventObjects = EventEmitter_1.EventEmitter.handlers[namespace];
            if (!eventObjects) {
                eventObjects = EventEmitter_1.EventEmitter.handlers[namespace] = [];
            }
            var idx = eventObjects.findIndex(function (x) { return x.name === event; });
            if (idx > -1) {
                eventObjects[idx].handlers.push(handlerObject);
            }
            else {
                var eventObject = {};
                eventObject["name"] = event;
                eventObject["handlers"] = [];
                eventObject["handlers"].push(handlerObject);
                eventObjects.push(eventObject);
            }
        }
    };
    EventHandler.handlerTest = new RegExp("^" + EventEmitter_1.EventEmitter.handlerPrefix + ".*");
    EventHandler.internalHandlerTest = new RegExp("^__" + EventEmitter_1.EventEmitter.handlerPrefix + ".*");
    return EventHandler;
}(EventEmitter_1.EventEmitter));
exports.EventHandler = EventHandler;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function decimalToHexColorString(value) {
    return '#' + ('00000' + Math.abs(value).toString(16)).substr(-6);
}
exports.decimalToHexColorString = decimalToHexColorString;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// Make a value ready for JSON.stringify() / process.send()
var ErrorSerializer = /** @class */ (function () {
    function ErrorSerializer() {
    }
    ErrorSerializer.Serialize = function (value) {
        if (typeof value === 'object') {
            return ErrorSerializer.destroyCircular(value, []);
        }
        // People sometimes throw things besides Error objects, so...
        if (typeof value === 'function') {
            // JSON.stringify discards functions. We do to, unless a function is thrown directly.
            return '[Function: ' + (value.name || 'anonymous') + ']';
        }
        return value;
    };
    ;
    // https://www.npmjs.com/package/destroy-circular
    ErrorSerializer.destroyCircular = function (from, seen) {
        var to;
        if (Array.isArray(from)) {
            to = [];
        }
        else {
            to = {};
        }
        seen.push(from);
        Object.keys(from).forEach(function (key) {
            var value = from[key];
            if (typeof value === 'function') {
                return;
            }
            if (!value || typeof value !== 'object') {
                to[key] = value;
                return;
            }
            if (seen.indexOf(from[key]) === -1) {
                to[key] = ErrorSerializer.destroyCircular(from[key], seen.slice(0));
                return;
            }
            to[key] = '[Circular]';
        });
        if (typeof from.name === 'string') {
            to.name = from.name;
        }
        if (typeof from.message === 'string') {
            to.message = from.message;
        }
        if (typeof from.stack === 'string') {
            to.stack = from.stack;
        }
        var errorEventProps = ["message", "filename", "lineno", "colno", "error"];
        errorEventProps.forEach(function (prop) {
            if (typeof from[prop] === 'string' ||
                typeof from[prop] === 'number') {
                to[prop] = from[prop];
                return;
            }
            if (typeof from[prop] === 'object') {
                to[prop] = ErrorSerializer.Serialize(from[prop]);
            }
        });
        return to;
    };
    return ErrorSerializer;
}());
exports.ErrorSerializer = ErrorSerializer;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Created by Georgi Dimov on 17.8.2017 г..
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventDispatcher_1 = __webpack_require__(5);
var Event_1 = __webpack_require__(3);
var Timer = /** @class */ (function (_super) {
    __extends(Timer, _super);
    function Timer(delay, repeatCount) {
        if (repeatCount === void 0) { repeatCount = 0; }
        var _this = _super.call(this) || this;
        _this._currentCount = 0;
        _this._delay = delay;
        _this._repeatCount = repeatCount;
        return _this;
    }
    Object.defineProperty(Timer.prototype, "currentCount", {
        get: function () {
            return this._currentCount;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Timer.prototype, "running", {
        get: function () {
            return this._delayedCall != null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Timer.prototype, "repeatCount", {
        get: function () {
            return this._repeatCount;
        },
        set: function (value) {
            this._repeatCount = Math.max(0, value);
            if (this.isFinished)
                this.stop();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Timer.prototype, "delay", {
        get: function () {
            return this._delay;
        },
        set: function (value) {
            this._delay = Math.max(0, value);
            if (this.running) {
                this.stop();
                this.start();
            }
        },
        enumerable: true,
        configurable: true
    });
    Timer.prototype.start = function () {
        if (this.running)
            return;
        this._delayedCall = gsap.TweenMax.delayedCall(this._delay, this.onDelay, null, this);
    };
    Timer.prototype.stop = function () {
        if (this.running) {
            this._delayedCall.kill();
            this._delayedCall = null;
        }
    };
    Timer.prototype.reset = function () {
        this.stop();
        this._currentCount = 0;
    };
    Object.defineProperty(Timer.prototype, "isFinished", {
        get: function () {
            return this._currentCount >= this._repeatCount && this._repeatCount != 0;
        },
        enumerable: true,
        configurable: true
    });
    Timer.prototype.onDelay = function () {
        this._currentCount++;
        this.dispatchEvent(new Event_1.Event(Timer.TIMER));
        if (this.isFinished) {
            this.stop();
            this.dispatchEvent(new Event_1.Event(Timer.TIMER_COMPLETE));
        }
        else if (this.running) {
            this.stop();
            this.start();
        }
    };
    Timer.TIMER_COMPLETE = "timerComplete";
    Timer.TIMER = "timer";
    return Timer;
}(EventDispatcher_1.EventDispatcher));
exports.Timer = Timer;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var UncaughtErrorsHandler = /** @class */ (function () {
    /**
     * Create a uncaughtErrorsHandler object.
     * @param {Function} callback - The callback which is called after an occured error event.
     * @param {Boolean} startListening - Chose if the object should start listening immediately.
     */
    function UncaughtErrorsHandler(callback, startListening) {
        if (startListening === void 0) { startListening = true; }
        if (!callback) {
            throw new Error("Missing callback function");
        }
        this._callback = callback;
        this._listening = false;
        if (startListening) {
            this._setupEvents();
        }
    }
    /**
     * Start listening for error events.
     * @return {void}
     */
    UncaughtErrorsHandler.prototype.startListening = function () {
        if (!this._listening) {
            this._setupEvents();
        }
    };
    /**
     * Stop listening for error events.
     * @return {void}
     */
    UncaughtErrorsHandler.prototype.stopListening = function () {
        if (this._listening) {
            this._setupEvents(false);
        }
    };
    /**
     * Disposing the class so there would be no memory leak.
     * @return {void}
     */
    UncaughtErrorsHandler.prototype.dispose = function () {
        if (this._listening) {
            this._setupEvents(false);
        }
        this._callback = null;
    };
    /**
     * It will attatch or detatch the listening event based on the given parametter;
     * @param {Boolean} attatch - if true the object will start listening.
     * @return {void}
     */
    UncaughtErrorsHandler.prototype._setupEvents = function (attatch) {
        if (attatch === void 0) { attatch = true; }
        this._listening = attatch ? true : false;
        if (attatch) {
            if (typeof window !== "undefined") {
                window.addEventListener("error", this._callback);
            }
        }
        else {
            if (typeof window !== "undefined") {
                window.removeEventListener("error", this._callback);
            }
        }
    };
    return UncaughtErrorsHandler;
}());
exports.UncaughtErrorsHandler = UncaughtErrorsHandler;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var RectangleClass = /** @class */ (function () {
    function RectangleClass() {
    }
    Object.defineProperty(RectangleClass, "Instance", {
        get: function () {
            return this.instance || (this.instance = new this());
        },
        enumerable: true,
        configurable: true
    });
    RectangleClass.drawToCanvas = function (options) {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var shouldStroke = true;
        canvas.width = options.width;
        canvas.height = options.height;
        ctx.clearRect(0, 0, options.width, options.height);
        if (typeof options.strokeStyle !== 'undefined') {
            if (typeof options.strokeStyle === 'string') {
                ctx.strokeStyle = options.strokeStyle;
            }
            else {
                var gradient = ctx.createLinearGradient(0, 0, 0, options.height);
                for (var i = 0; i < options.strokeStyle.length; i++) {
                    gradient.addColorStop(options.strokeStyle[i].offset, options.strokeStyle[i].color);
                }
                ctx.strokeStyle = gradient;
            }
        }
        else {
            shouldStroke = false;
        }
        if (typeof options.fillStyle !== 'undefined') {
            if (typeof options.fillStyle === 'string') {
                ctx.fillStyle = options.fillStyle;
            }
            else {
                var gradient = ctx.createLinearGradient(0, 0, 0, options.height);
                for (var i = 0; i < options.fillStyle.length; i++) {
                    gradient.addColorStop(options.fillStyle[i].offset, options.fillStyle[i].color);
                }
                ctx.fillStyle = gradient;
            }
        }
        RectangleClass.drawRoundRect(ctx, options.width, options.height, options.lineWidth || 0, options.radius || 0, shouldStroke);
        return canvas;
    };
    RectangleClass.drawRoundRect = function (ctx, width, height, lineWidth, radius, stroke) {
        var x = 0;
        var y = 0;
        ctx.lineWidth = lineWidth * 2;
        ctx.beginPath();
        ctx.moveTo(x + radius + lineWidth, y + lineWidth);
        ctx.lineTo(x + width - radius - lineWidth, y + lineWidth); // top horizontal
        ctx.quadraticCurveTo(x + width - lineWidth, y + lineWidth, x + width - lineWidth, y + radius + lineWidth);
        ctx.lineTo(x + width - lineWidth, y + height - radius - lineWidth); // right vertical
        ctx.quadraticCurveTo(x + width - lineWidth, y + height - lineWidth, x + width - radius - lineWidth, y + height - lineWidth);
        ctx.lineTo(x + radius + lineWidth, y + height - lineWidth); // bottom horizontal
        ctx.quadraticCurveTo(x + lineWidth, y + height - lineWidth, x + lineWidth, y + height - radius - lineWidth);
        ctx.lineTo(x + lineWidth, y + radius + lineWidth); // left vertical
        ctx.quadraticCurveTo(x + lineWidth, y + lineWidth, x + radius + lineWidth, y + lineWidth);
        ctx.closePath();
        if (stroke) {
            ctx.stroke();
        }
        ctx.fill();
    };
    return RectangleClass;
}());
var Rectangle = RectangleClass.Instance;
exports.Rectangle = Rectangle;
Rectangle.drawToCanvas = RectangleClass.drawToCanvas;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Serializer_1 = __webpack_require__(6);
var BitmapMoneyText = /** @class */ (function (_super) {
    __extends(BitmapMoneyText, _super);
    function BitmapMoneyText(config) {
        var _this = _super.call(this) || this;
        _this._config = new MoneyTextConfig();
        _this._config.serialize(config);
        if (_this._config.centsFontSize == undefined)
            _this._config.centsFontSize = _this._config.moneyFontSize;
        if (_this._config.currencyFontSize == undefined)
            _this._config.currencyFontSize = _this._config.moneyFontSize;
        if (_this._config.suffixFontSize == undefined)
            _this._config.suffixFontSize = _this._config.moneyFontSize;
        if (_this._config.centsfontFamily == undefined)
            _this._config.centsfontFamily = _this._config.fontFamily;
        if (_this._config.currencyfontFamily == undefined)
            _this._config.currencyfontFamily = _this._config.fontFamily;
        if (_this._config.suffixfontFamily == undefined)
            _this._config.suffixfontFamily = _this._config.fontFamily;
        _this._rect = new PIXI.Graphics();
        _this._rect.beginFill(0xff0000, 1);
        _this._rect.drawRect(0, 0, _this._config.bounds.width, _this._config.bounds.height);
        _this._rect.endFill();
        _this.addChild(_this._rect);
        _this._rect.visible = true;
        _this._rect.alpha = 0;
        if (_this._config.debug) {
            _this._rect.alpha = 1;
        }
        _this.createTextFields();
        if (_this._config.diamond) {
            _this._diamondImage = new PIXI.Sprite(_this._config.diamond);
            _this.addChild(_this._diamondImage);
            _this._diamondImage.anchor.set(0.5);
            _this._diamondImage.visible = false;
        }
        return _this;
    }
    Object.defineProperty(BitmapMoneyText.prototype, "money", {
        get: function () {
            return this._config.money;
        },
        set: function (value) {
            this._config.money = value;
            this._moneyText.x = 0;
            this._moneyText.y = 0;
            this._centsText.x = 0;
            this._currencyText.x = 0;
            this._moneyText.text = this._config.moneyString;
            this._centsText.text = this._config.centsString;
            this.resetCentsTextWidthHeight();
            this.resetMoneyTextWidthHeight();
            this.recalcVerticalOffset();
            if (this._config.shouldHideWhenMoneyValueIsZero && value == 0) {
                this._moneyText.visible = false;
                this._centsText.visible = false;
                this._currencyText.visible = false;
                if (this._diamondImage)
                    this._diamondImage.visible = false;
            }
            else {
                if (this._diamondImage) {
                    this._diamondImage.visible = true;
                }
                this._moneyText.visible = true;
                this._centsText.visible = this._config.shouldCentsHaveValue;
                this._currencyText.visible = !this._config.isShowingCredits &&
                    this._config.shouldShowCurrency && !this._config.diamond;
            }
            if (isNaN(this._config.currencyPositionOffsetPercentX)) {
                this._currencyText.text = " " + this._config.currency;
                this.resetCurrencyTextWidthHeight();
            }
            else {
                this._currencyText.text = "" + this._config.currency;
                this.resetCurrencyTextWidthHeight();
            }
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "align", {
        set: function (value) {
            this._config.align = value;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "bounds", {
        get: function () {
            return { width: this._config.bounds.width, height: this._config.bounds.height };
        },
        set: function (value) {
            this._config.bounds = { width: value.width, height: value.height };
            this._rect.width = value.width;
            this._rect.height = value.height;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "isShowingCredits", {
        set: function (val) {
            if (this._config.diamondPlay) {
                if (this._config.isShowingCredits) {
                    this._config.isShowingCredits = false;
                    this.money = this._config.money;
                }
                return;
            }
            this._config.isShowingCredits = val;
            this.money = this._config.money;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "denomination", {
        set: function (val) {
            this._config.denomination = val;
            this.money = this._config.money;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "prefix", {
        set: function (value) {
            this._config.prefix = value;
            this._moneyText.text = this._config.moneyString;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "suffix", {
        set: function (value) {
            this._config.suffix = value;
            this._suffix.text = this._config.suffix;
            this._suffix.visible = true;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    BitmapMoneyText.prototype.recalcVerticalOffset = function () {
        if (isNaN(this._config.centsPositionOffsetPercentY))
            this._config.centsOffsetCalc(this._centsText.y, this._moneyText.textHeight);
        if (isNaN(this._config.currencyPositionOffsetPercentY))
            this._config.currencyOffsetCalc(this._currencyText.y, this._moneyText.textHeight);
    };
    Object.defineProperty(BitmapMoneyText.prototype, "moneyTextPositionY", {
        set: function (y) {
            this._moneyText.y = y;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "centsTextPositionY", {
        set: function (y) {
            this._centsText.y = y;
            if (this._moneyText.textHeight > 0) {
                this._config.centsOffsetCalc(this._centsText.y, this._moneyText.textHeight);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "currencyTextPositionY", {
        set: function (y) {
            this._currencyText.y = y;
            if (this._moneyText.textHeight > 0) {
                this._config.currencyOffsetCalc(this._currencyText.y, this._moneyText.textHeight);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "font", {
        get: function () {
            return { name: this._config.fontFamily, size: this._config.moneyFontSize };
        },
        set: function (fontObj) {
            this._moneyText.x = 0;
            this._moneyText.y = 0;
            this._centsText.x = 0;
            this._centsText.y = 0;
            this._currencyText.x = 0;
            this._currencyText.y = 0;
            this._suffix.x = 0;
            this._suffix.y = 0;
            this._moneyText.font = fontObj;
            this._centsText.font = fontObj;
            this._currencyText.font = fontObj;
            this._suffix.font = fontObj;
            this._config.fontFamily = fontObj.name;
            this._config.moneyFontSize = fontObj.size;
            this._config.centsfontFamily = fontObj.name;
            this._config.centsFontSize = fontObj.size;
            this._config.currencyfontFamily = fontObj.name;
            this._config.currencyFontSize = fontObj.size;
            this._config.suffixfontFamily = fontObj.name;
            this._config.suffixFontSize = fontObj.size;
            this._config.centsPositionOffsetPercentY = NaN;
            this._config.currencyPositionOffsetPercentY = NaN;
            this._config.centsPositionOffsetPercentX = NaN;
            this._config.currencyPositionOffsetPercentX = NaN;
            this._config.letterSpacing = fontObj.letterSpacing || 0;
            this.recalcVerticalOffset();
            this.resetAllTextWidthHeight();
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "moneyFont", {
        get: function () {
            return { name: this._config.fontFamily, size: this._config.moneyFontSize };
        },
        set: function (moneyFontObj) {
            this._moneyText.x = 0;
            this._moneyText.font = moneyFontObj;
            this._config.fontFamily = moneyFontObj.name;
            this._config.moneyFontSize = moneyFontObj.size;
            this.recalcVerticalOffset();
            this.resetMoneyTextWidthHeight();
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "centsFont", {
        get: function () {
            return { name: this._config.centsfontFamily, size: this._config.centsFontSize };
        },
        set: function (font) {
            this._centsText.x = 0;
            this._centsText.y = 0;
            this._centsText.font = font;
            this._config.centsfontFamily = font.name;
            this._config.centsFontSize = font.size;
            this._config.centsPositionOffsetPercentX = NaN;
            this._config.centsPositionOffsetPercentX = NaN;
            this.recalcVerticalOffset();
            this.resetCentsTextWidthHeight();
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "suffixFont", {
        get: function () {
            return { name: this._config.suffixfontFamily, size: this._config.suffixFontSize };
        },
        set: function (font) {
            this._suffix.x = 0;
            this._suffix.y = 0;
            this._suffix.font = font;
            this._config.suffixfontFamily = font.name;
            this._config.suffixFontSize = font.size;
            this.resetSuffixWidthHeight();
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "currencyFont", {
        get: function () {
            return { name: this._config.currencyfontFamily, size: this._config.currencyFontSize };
        },
        set: function (font) {
            this._currencyText.x = 0;
            this._currencyText.y = 0;
            this._currencyText.font = font;
            this._config.currencyfontFamily = font.name;
            this._config.currencyFontSize = font.size;
            this._config.currencyPositionOffsetPercentY = NaN;
            this._config.currencyPositionOffsetPercentX = NaN;
            this.recalcVerticalOffset();
            this.resetCurrencyTextWidthHeight();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitmapMoneyText.prototype, "scaleDiamond", {
        set: function (value) {
            this._diamondImage.scale.set(value, value);
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    BitmapMoneyText.prototype.resizeDiamond = function () {
        this._diamondImage.scale.set(1);
        var diamandScaleToSet = (this._moneyText.textHeight / this._diamondImage.height) * this._config.diamondScaleCoef;
        this._diamondImage.scale.x = diamandScaleToSet;
        this._diamondImage.scale.y = diamandScaleToSet;
    };
    Object.defineProperty(BitmapMoneyText.prototype, "setScaleDiamondCoef", {
        set: function (value) {
            this._config.diamondScaleCoef = value;
        },
        enumerable: true,
        configurable: true
    });
    BitmapMoneyText.prototype.createTextFields = function () {
        var textStyle = {
            font: {
                name: this._config.fontFamily,
                size: this._config.moneyFontSize
            },
            align: 'left',
            tint: this._config.moneyTint
        };
        this._moneyText = new PIXI.extras.BitmapText("", textStyle);
        this._moneyText.letterSpacing = this._config.letterSpacing;
        textStyle = {
            font: {
                name: this._config.centsfontFamily,
                size: this._config.centsFontSize
            },
            align: 'left',
            tint: this._config.centsTint
        };
        this._centsText = new PIXI.extras.BitmapText("", textStyle);
        this._centsText.letterSpacing = this._config.letterSpacing;
        textStyle = {
            font: {
                name: this._config.currencyfontFamily,
                size: this._config.currencyFontSize
            },
            align: 'left',
            tint: this._config.currencyTint
        };
        this._currencyText = new PIXI.extras.BitmapText(this._config.currency, textStyle);
        this._currencyText.letterSpacing = this._config.letterSpacing;
        textStyle = {
            font: {
                name: this._config.suffixfontFamily,
                size: this._config.suffixFontSize
            },
            align: 'left',
            tint: this._config.suffixTint
        };
        this._suffix = new PIXI.extras.BitmapText(this._config.suffix, textStyle);
        this._suffix.letterSpacing = this._config.letterSpacing;
        this.resetAllTextWidthHeight();
        this.addChild(this._moneyText);
        this.addChild(this._centsText);
        this.addChild(this._currencyText);
        this.addChild(this._suffix);
        this._moneyText.visible = false;
        this._centsText.visible = false;
        this._currencyText.visible = false;
        this._suffix.visible = false;
    };
    BitmapMoneyText.prototype.positionText = function () {
        this._moneyText.x = 0;
        this._moneyText.y = 0;
        this._centsText.x = 0;
        this._centsText.y = 0;
        this._currencyText.x = 0;
        this._currencyText.y = 0;
        this._suffix.x = 0;
        this._suffix.y = 0;
        this._moneyText.y = (this._config.bounds.height - this._moneyText.textHeight) / 2;
        this._suffix.y = this._moneyText.y + this._moneyText.textHeight - this._suffix.textHeight;
        if (this._config.diamond) {
            this._diamondImage.y = this._moneyText.y + this._moneyText.textHeight / 2;
        }
        if (this._config.align == "left") {
            if (this._config.diamond) {
                this._diamondImage.x = this._diamondImage.width / 2;
                this._moneyText.x = this._diamondImage.width + this._config.spaceBetweenMoneyAndDiamond + this.moneyTextLetterSpacing;
            }
        }
        else if (this._config.align == "right") {
            if (this._config.diamond) {
                this._moneyText.x = this._config.bounds.width - this._moneyText.textWidth;
                this._diamondImage.x = this._moneyText.x - this._config.spaceBetweenMoneyAndDiamond - this._diamondImage.width / 2 +
                    this.moneyTextLetterSpacing;
            }
            else {
                this._moneyText.x = this._config.bounds.width - this.textWidth;
            }
        }
        else {
            if (this._config.diamond) {
                this._moneyText.x = (this._config.bounds.width - this.textWidth) / 2;
                if (this._config.centerAlignOnlyMoney) {
                    this._moneyText.x += (this._diamondImage.width + this._config.spaceBetweenMoneyAndDiamond) / 2 -
                        this.moneyTextLetterSpacing;
                    this._diamondImage.x = this._moneyText.x - this._diamondImage.width / 2 - this._config.spaceBetweenMoneyAndDiamond;
                }
                else {
                    this._diamondImage.x = this._moneyText.x + this._diamondImage.width / 2;
                    this._moneyText.x += this._diamondImage.width + this._config.spaceBetweenMoneyAndDiamond +
                        this.moneyTextLetterSpacing;
                }
            }
            else {
                this._moneyText.x = (this._config.bounds.width - this.textWidth) / 2;
                if (this._config.centerAlignOnlyMoney && !this._config.isShowingCredits)
                    this._moneyText.x += this._currencyText.width / 2;
            }
        }
        this._suffix.x = this._moneyText.x + this._moneyText.textWidth;
        if (this._centsText.visible) {
            this.calcHorizontalOffset();
            this._centsText.x = this._moneyText.x + this._moneyText.textWidth + this._config.spaceBetweenMoneyAndCents;
            this._centsText.y = this._moneyText.y + this._moneyText.textHeight * this._config.centsPositionOffsetPercentY;
            this._suffix.x = this._centsText.x + this._centsText.textWidth;
        }
        // TODO: replace this check with this._currencyText.visible
        if (!this._config.isShowingCredits && this._config.shouldShowCurrency) {
            var leftText = this._centsText.visible ? this._centsText : this._moneyText;
            this._currencyText.x = leftText.x + leftText.width +
                (!isNaN(this._config.currencyPositionOffsetPercentX) ? this._config.currencyPositionOffsetPercentX *
                    this._moneyText.textHeight : 0);
            this._currencyText.y = this._moneyText.y + this._moneyText.textHeight * this._config.currencyPositionOffsetPercentY;
            this._suffix.x = this._currencyText.x + this._currencyText.textWidth;
        }
        this._moneyText.x = Math.round(this._moneyText.x);
        this._moneyText.y = Math.round(this._moneyText.y);
        this._centsText.x = Math.round(this._centsText.x);
        this._centsText.y = Math.round(this._centsText.y);
        this._currencyText.x = Math.round(this._currencyText.x);
        this._currencyText.y = Math.round(this._currencyText.y);
        this._suffix.x = Math.round(this._suffix.x);
        this._suffix.y = Math.round(this._suffix.y);
    };
    Object.defineProperty(BitmapMoneyText.prototype, "textWidth", {
        get: function () {
            this._moneyText.width;
            this._centsText.width;
            this._currencyText.width;
            this.resetSuffixWidthHeight();
            var moneyWidth = this._moneyText.textWidth;
            if (!this._config.isShowingCredits && !this._config.diamondPlay)
                this.calcHorizontalOffset();
            var centsWidth = this._centsText.visible ? this._centsText.textWidth +
                (isNaN(this._config.spaceBetweenMoneyAndCents) ? 0 : this._config.spaceBetweenMoneyAndCents) : 0;
            var currencyWidth = this._config.isShowingCredits ||
                !this._config.shouldShowCurrency ? 0 : this._config.diamond ? this._diamondImage.width +
                this._config.spaceBetweenMoneyAndDiamond + this.moneyTextLetterSpacing :
                this._currencyText.textWidth +
                    (isNaN(this._config.spaceBetweenCentsAndCurrency) ? 0 : this._config.spaceBetweenCentsAndCurrency);
            var suffixWidth = this._config.suffix ? this._suffix.textWidth : 0;
            return moneyWidth + centsWidth + currencyWidth + suffixWidth;
        },
        enumerable: true,
        configurable: true
    });
    BitmapMoneyText.prototype.setCentsPositionOffsetPercentX = function (muneyEnd, centsBegin, moneyTextHeight) {
        this._config.centsPositionOffsetPercentX = centsBegin == muneyEnd ? 0 : (centsBegin - muneyEnd) / moneyTextHeight;
    };
    BitmapMoneyText.prototype.setCurrencyPositionOffsetPercentX = function (centsEnd, currencyBegin, moneyTextHeight) {
        this._config.currencyPositionOffsetPercentX = currencyBegin == centsEnd ? 0 : (currencyBegin - centsEnd) / moneyTextHeight;
    };
    Object.defineProperty(BitmapMoneyText.prototype, "moneyTextLetterSpacing", {
        get: function () {
            this._moneyText.text = "111111";
            this.resetMoneyTextWidthHeight();
            var letterSpacing = this._moneyText.children[1].x -
                this._moneyText.children[0].x -
                this._moneyText.children[0].width;
            this._moneyText.text = this._config.moneyString;
            this.resetMoneyTextWidthHeight();
            if (letterSpacing < 0) {
                letterSpacing /= 2;
            }
            else {
                letterSpacing = 0;
            }
            return letterSpacing;
        },
        enumerable: true,
        configurable: true
    });
    BitmapMoneyText.prototype.calcHorizontalOffset = function () {
        this._moneyText.text = "111111";
        this.resetMoneyTextWidthHeight();
        var letterSpacing = this._moneyText.children[1].x -
            this._moneyText.children[0].x -
            this._moneyText.children[0].width;
        this._moneyText.text = this._config.moneyString;
        this.resetMoneyTextWidthHeight();
        if (!isNaN(this._config.centsPositionOffsetPercentX))
            letterSpacing = 0;
        this._config.spaceBetweenMoneyAndCents = letterSpacing + (!isNaN(this._config.centsPositionOffsetPercentX) ?
            this._config.centsPositionOffsetPercentX * this._moneyText.textHeight : 0);
        this._centsText.text = "111111";
        this.resetCentsTextWidthHeight();
        letterSpacing = this._centsText.children[1].x -
            this._centsText.children[0].x -
            this._centsText.children[0].width;
        this._centsText.text = this._config.centsString;
        this.resetCentsTextWidthHeight();
        if (!isNaN(this._config.currencyPositionOffsetPercentX))
            letterSpacing = 0;
        this._config.spaceBetweenCentsAndCurrency = letterSpacing + (!isNaN(this._config.currencyPositionOffsetPercentX) ?
            this._config.currencyPositionOffsetPercentX * this._moneyText.textHeight : 0);
    };
    BitmapMoneyText.prototype.resetAllTextWidthHeight = function () {
        this.resetMoneyTextWidthHeight();
        this.resetCentsTextWidthHeight();
        this.resetCurrencyTextWidthHeight();
        this.resetSuffixWidthHeight();
    };
    BitmapMoneyText.prototype.resetCurrencyTextWidthHeight = function () {
        this._currencyText.height;
        this._currencyText.width;
    };
    BitmapMoneyText.prototype.resetCentsTextWidthHeight = function () {
        this._centsText.height;
        this._centsText.width;
    };
    BitmapMoneyText.prototype.resetMoneyTextWidthHeight = function () {
        this._moneyText.height;
        this._moneyText.width;
    };
    BitmapMoneyText.prototype.resetSuffixWidthHeight = function () {
        this._suffix.height;
        this._suffix.width;
    };
    BitmapMoneyText.prototype.dispose = function () {
        this._moneyText.destroy();
        this._centsText.destroy();
        this._currencyText.destroy();
    };
    BitmapMoneyText.prototype.resetFonts = function () {
        this._moneyText.font = {
            name: this._config.fontFamily,
            size: this._config.moneyFontSize
        };
        this._moneyText.align = "left";
        this._centsText.font = {
            name: this._config.centsfontFamily,
            size: this._config.centsFontSize
        };
        this._centsText.align = "left";
        this._currencyText.font = {
            name: this._config.currencyfontFamily,
            size: this._config.currencyFontSize
        };
        this._currencyText.align = "left";
        this._suffix.font = {
            name: this._config.suffixfontFamily,
            size: this._config.suffixFontSize
        };
        this._suffix.align = "left";
        this.resetAllTextWidthHeight();
    };
    BitmapMoneyText.prototype.fitToBounds = function () {
        this.resetFonts();
        if (this._config.diamond) {
            this.resizeDiamond();
        }
        if (!this._config.shouldFitToBounds)
            return;
        // TODO: implement scaling of all 3 fields
        var currencyCompensation = 0;
        if (this._config.centerAlignOnlyMoney && this._config.align == "center") {
            currencyCompensation = this._config.diamond ? this._diamondImage.width + this._config.spaceBetweenMoneyAndDiamond :
                this._currencyText.width;
        }
        var moneyFontSize = this._moneyText.font.size;
        var centsFontSize = this._centsText.font.size;
        var currencyFontSize = this._currencyText.font.size;
        var suffixFontSize = this._suffix.font.size;
        while (this.textWidth > this._config.bounds.width - currencyCompensation && moneyFontSize > this._config.minFontSize) {
            var coefCurrency = this._config.diamond ? this._config.diamondScaleCoef : currencyFontSize / moneyFontSize;
            var coefCents = centsFontSize / moneyFontSize;
            var coefSuffix = suffixFontSize / moneyFontSize;
            moneyFontSize--;
            this._moneyText.font = {
                // tint: (this._moneyText.font as any).tint,
                // align: "left",
                name: this._config.fontFamily,
                size: moneyFontSize
            };
            centsFontSize = moneyFontSize * coefCents;
            this._centsText.font = {
                // tint: (this._centsText.font as any).tint,
                // align: "left",
                name: this._config.centsfontFamily,
                size: centsFontSize
            };
            currencyFontSize = moneyFontSize * coefCurrency;
            if (this._config.diamond != null && !this._config.keepDiamondSize) {
                this.resizeDiamond();
            }
            else if (!this._config.keepCurrencySize) {
                this._currencyText.font = {
                    // tint: (this._currencyText.font as any).tint,
                    // align: "left",
                    name: this._config.currencyfontFamily,
                    size: currencyFontSize
                };
            }
            suffixFontSize = moneyFontSize * coefSuffix;
            this._suffix.font = {
                // tint: (this._suffix.font as any).tint,
                // align: "left",
                name: this._config.suffixfontFamily,
                size: suffixFontSize
            };
        }
    };
    return BitmapMoneyText;
}(PIXI.Container));
exports.BitmapMoneyText = BitmapMoneyText;
var MoneyTextConfig = /** @class */ (function (_super) {
    __extends(MoneyTextConfig, _super);
    function MoneyTextConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.money = 0;
        _this.prefix = "";
        _this.suffix = "";
        _this.denomination = 1;
        _this.currency = "";
        _this.centsVisibilityMoneyLimit = -1;
        _this.isShowingCredits = false;
        _this.fontFamily = "MyCustomFont";
        _this.centsfontFamily = undefined;
        _this.currencyfontFamily = undefined;
        _this.suffixfontFamily = undefined;
        _this.align = "center";
        _this.moneyTint = 16777215;
        _this.centsTint = 16777215;
        _this.currencyTint = 16777215;
        _this.suffixTint = 16777215;
        _this.moneyFontSize = 1;
        _this.centsFontSize = undefined;
        _this.currencyFontSize = undefined;
        _this.suffixFontSize = undefined;
        _this.spaceBetweenMoneyAndCents = NaN;
        _this.spaceBetweenCentsAndCurrency = NaN;
        _this.spaceBetweenMoneyAndDiamond = 5;
        _this.shouldShowCurrency = true;
        _this.centerAlignOnlyMoney = false;
        _this.shouldHideWhenMoneyValueIsZero = false;
        _this.minFontSize = 6;
        _this.bounds = { width: 100, height: 50 };
        _this.shouldFitToBounds = true;
        _this.debug = false;
        _this.diamond = null;
        _this.diamondScaleCoef = 1;
        _this.posDiamondY = 0;
        _this.diamondPlay = false;
        _this.keepCurrencySize = false;
        _this.keepDiamondSize = false;
        _this.centsPositionOffsetPercentY = NaN;
        _this.currencyPositionOffsetPercentY = NaN;
        _this.centsPositionOffsetPercentX = NaN;
        _this.currencyPositionOffsetPercentX = NaN;
        _this.hideCents = false;
        _this.letterSpacing = 0;
        return _this;
    }
    MoneyTextConfig.prototype.centsOffsetCalc = function (centsPosition, moneyTextHeight) {
        if (centsPosition == 0)
            this.centsPositionOffsetPercentY = 0;
        else
            this.centsPositionOffsetPercentY = centsPosition / moneyTextHeight;
    };
    MoneyTextConfig.prototype.currencyOffsetCalc = function (currencyPosition, moneyTextHeight) {
        if (currencyPosition == 0)
            this.currencyPositionOffsetPercentY = 0;
        else
            this.currencyPositionOffsetPercentY = currencyPosition / moneyTextHeight;
    };
    Object.defineProperty(MoneyTextConfig.prototype, "moneyString", {
        get: function () {
            var str = "";
            if (this.isShowingCredits) {
                str = Math.floor(this.money / this.denomination).toString();
            }
            else if (this.diamondPlay) {
                str = (this.money / 100).toFixed();
            }
            else {
                str = (this.money / 100).toFixed(2);
                var moneyParts = str.split(".");
                var moneyDigits = moneyParts[0].split('');
                var counter = 3;
                var digitIndex = moneyDigits.length;
                while (--digitIndex > 0) {
                    counter--;
                    if (counter == 0) {
                        counter = 3;
                        moneyDigits.splice(digitIndex, 0, " ");
                    }
                }
                str = moneyDigits.join('') + (this.shouldCentsHaveValue ? "." : "");
            }
            if (this.prefix)
                str = this.prefix + str;
            return str;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyTextConfig.prototype, "centsString", {
        get: function () {
            var str = "";
            if (this.shouldCentsHaveValue) {
                str = (this.money / 100).toFixed(2).split(".")[1];
            }
            return str;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyTextConfig.prototype, "shouldCentsHaveValue", {
        get: function () {
            return !this.isShowingCredits && !this.diamondPlay && !this.hideCents &&
                (this.money <= this.centsVisibilityMoneyLimit || this.centsVisibilityMoneyLimit < 0);
        },
        enumerable: true,
        configurable: true
    });
    return MoneyTextConfig;
}(Serializer_1.Serializer));


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var log = __webpack_require__(8);
var Serializer_1 = __webpack_require__(6);
var MoneyText = /** @class */ (function (_super) {
    __extends(MoneyText, _super);
    function MoneyText(config) {
        var _this = _super.call(this) || this;
        _this._lastCalculatedWidths = null;
        _this._config = new MoneyTextConfig();
        _this._config.serialize(config);
        if (_this._config.debug) {
            _this.createDebugFields();
        }
        _this.createTextFields();
        if (_this._config.diamond) {
            _this._diamondImage = new PIXI.Sprite(_this._config.diamond);
            _this.addChild(_this._diamondImage);
            _this._diamondImage.pivot.set(_this._diamondImage.texture.width / 2, _this._diamondImage.texture.height / 2);
            _this._diamondImage.visible = false;
        }
        return _this;
    }
    Object.defineProperty(MoneyText.prototype, "moneyString", {
        get: function () {
            return this._config.moneyString;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "centsString", {
        get: function () {
            return this._config.centsString;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "money", {
        get: function () {
            return this._config.money;
        },
        set: function (value) {
            var moneyLength = this._moneyText.text.length;
            var centsLength = this._centsText.text.length;
            var moneyVisible = this._moneyText.visible;
            var centsVisible = this._centsText.visible;
            this._config.money = value;
            this._moneyText.text = this._config.moneyString;
            this._centsText.text = this._config.centsString;
            if (this._config.shouldHideWhenMoneyValueIsZero && value == 0) {
                this._moneyText.visible = false;
                this._decimalSymbolText.visible = false;
                this._centsText.visible = false;
                this._currencyText.visible = false;
                if (this._diamondImage)
                    this._diamondImage.visible = false;
            }
            else {
                if (this._diamondImage)
                    this._diamondImage.visible = true;
                this._moneyText.visible = true;
                this._centsText.visible = this._config.shouldCentsHaveValue;
                this._decimalSymbolText.visible = this._centsText.visible;
                this._currencyText.visible = !this._config.isShowingCredits &&
                    this._config.shouldShowCurrency && !this._config.diamond;
            }
            if (this._config.suffix.length != 0)
                this._suffix.visible = true;
            // Recalculate font size and position only if money or cents length or visibility changed.
            if (!this._lastCalculatedWidths
                || moneyLength !== this._moneyText.text.length
                || centsLength !== this._centsText.text.length
                || moneyVisible !== this._moneyText.visible
                || centsVisible !== this._centsText.visible) {
                this.fitToBounds();
                this.positionText();
            }
            if (this._config.debug) {
                this._boundsDebug.width = this._config.bounds.width;
                this._boundsDebug.height = this._config.bounds.height;
                this._integerDebug.x = this._moneyText.x;
                this._integerDebug.y = this._moneyText.y;
                this._integerDebug.width = this._lastCalculatedWidths.integer; //this._moneyText.width;
                this._integerDebug.height = this._moneyText.height;
                this._decimalSymbolDebug.x = this._decimalSymbolText.x;
                this._decimalSymbolDebug.y = this._decimalSymbolText.y;
                this._decimalSymbolDebug.width = this._lastCalculatedWidths.decimalSymbol; //this._decimalSymbolText.width;
                this._decimalSymbolDebug.height = this._decimalSymbolText.height;
                this._decimalDebug.x = this._centsText.x;
                this._decimalDebug.y = this._centsText.y;
                this._decimalDebug.width = this._lastCalculatedWidths.decimal; //this._centsText.width;
                this._decimalDebug.height = this._centsText.height;
                this._currencyDebug.x = this._currencyText.x;
                this._currencyDebug.y = this._currencyText.y;
                this._currencyDebug.width = this._lastCalculatedWidths.currency; //this._currencyText.width;
                this._currencyDebug.height = this._currencyText.height;
                this._suffixDebug.x = this._suffix.x;
                this._suffixDebug.y = this._suffix.y;
                this._suffixDebug.width = this._lastCalculatedWidths.suffix; //this._suffix.width;
                this._suffixDebug.height = this._suffix.height;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "bounds", {
        get: function () {
            return { width: this._config.bounds.width, height: this._config.bounds.height };
        },
        set: function (value) {
            this._config.bounds.height = value.height;
            this._config.bounds.width = value.width;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "isShowingCredits", {
        set: function (val) {
            this._config.isShowingCredits = val;
            this.money = this.money;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "denomination", {
        set: function (val) {
            this._config.denomination = val;
            this.money = this.money;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "prefix", {
        set: function (value) {
            this._config.prefix = value;
            this._moneyText.text = this._config.moneyString;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "suffix", {
        set: function (value) {
            this._config.suffix = value;
            this._suffix.text = this._config.suffix;
            this._suffix.visible = true;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "style", {
        set: function (style) {
            var moneyStyle = this._moneyText.style;
            var centsStyle = this._centsText.style;
            var currencyStyle = this._currencyText.style;
            var suffixStyle = this._suffix.style;
            for (var key in style) {
                if (key != "fontSize" && key != "font") {
                    moneyStyle[key] = style[key];
                    centsStyle[key] = style[key];
                    currencyStyle[key] = style[key];
                    suffixStyle[key] = style[key];
                }
            }
            this._moneyText.style = moneyStyle;
            this._centsText.style = centsStyle;
            this._currencyText.style = currencyStyle;
            this._suffix.style = suffixStyle;
            this._decimalSymbolText.style = moneyStyle;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "moneyStyle", {
        set: function (style) {
            var moneyStyle = this._moneyText.style;
            for (var key in style) {
                if (key != "fontSize" && key != "font") {
                    moneyStyle[key] = style[key];
                }
            }
            this._moneyText.style = moneyStyle;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "centsStyle", {
        set: function (style) {
            var centsStyle = this._centsText.style;
            for (var key in style) {
                if (key != "fontSize" && key != "font") {
                    centsStyle[key] = style[key];
                }
            }
            this._centsText.style = centsStyle;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "currencyStyle", {
        set: function (style) {
            var currencyStyle = this._currencyText.style;
            for (var key in style) {
                if (key != "fontSize" && key != "font") {
                    currencyStyle[key] = style[key];
                }
            }
            this._currencyText.style = currencyStyle;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "suffixStyle", {
        set: function (style) {
            var suffixStyle = this._suffix.style;
            for (var key in style) {
                if (key != "fontSize" && key != "font") {
                    suffixStyle[key] = style[key];
                }
            }
            this._suffix.style = suffixStyle;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "moneyFontSize", {
        get: function () {
            return this._moneyText.style.fontSize;
        },
        set: function (value) {
            this._config.moneyFontSize = value;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "centsFontSize", {
        get: function () {
            return this._centsText.style.fontSize;
        },
        set: function (value) {
            this._config.centsFontSize = value;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "currencyFontSize", {
        get: function () {
            return this._currencyText.style.fontSize;
        },
        set: function (value) {
            this._config.currencyFontSize = value;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "scaleDiamond", {
        set: function (value) {
            this._config.diamondScaleCoef = value;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    MoneyText.prototype.setAlign = function (value) {
        this._config.align = value;
        this.fitToBounds();
        this.positionText();
    };
    MoneyText.prototype.resizeDiamond = function () {
        this._diamondImage.scale.set(1);
        var diamondScaleToSet = (this._moneyText.height / this._diamondImage.height) * this._config.diamondScaleCoef;
        this._diamondImage.width = Math.round(this._diamondImage.width * diamondScaleToSet);
        this._diamondImage.height = Math.round(this._diamondImage.height * diamondScaleToSet);
    };
    MoneyText.prototype.calculateFontProperty = function () {
        // this causes the "font" property of style to be calculated
        this._moneyText.width;
        this._centsText.width;
        this._currencyText.width;
        this._decimalSymbolText.width;
        this._suffix.width;
    };
    MoneyText.prototype.createTextFields = function () {
        var textStyle = {
            fontFamily: this._config.fontFamily,
            fontSize: this._config.moneyFontSize,
            letterSpacing: this._config.letterSpacing
        };
        this._moneyText = new PIXI.Text("", textStyle);
        this._decimalSymbolText = new PIXI.Text(this._config.decimalSymbol, textStyle);
        textStyle = {
            fontFamily: this._config.fontFamily,
            fontSize: this._config.centsFontSize,
            letterSpacing: this._config.letterSpacing
        };
        this._centsText = new PIXI.Text("", textStyle);
        textStyle = {
            fontFamily: this._config.fontFamily,
            fontSize: this._config.currencyFontSize,
            letterSpacing: this._config.letterSpacing
        };
        this._currencyText = new PIXI.Text(" " + this._config.currency, textStyle);
        textStyle = {
            fontFamily: this._config.fontFamily,
            fontSize: this._config.suffixFontSize,
            letterSpacing: this._config.letterSpacing
        };
        this._suffix = new PIXI.Text(this._config.suffix, textStyle);
        this.calculateFontProperty();
        this.addChild(this._moneyText);
        this.addChild(this._decimalSymbolText);
        this.addChild(this._centsText);
        this.addChild(this._currencyText);
        this.addChild(this._suffix);
        this._moneyText.visible = false;
        this._decimalSymbolText.visible = false;
        this._centsText.visible = false;
        this._currencyText.visible = false;
        this._suffix.visible = false;
    };
    MoneyText.prototype.createDebugFields = function () {
        this._integerDebug = new PIXI.Graphics();
        this._decimalDebug = new PIXI.Graphics();
        this._decimalSymbolDebug = new PIXI.Graphics();
        this._boundsDebug = new PIXI.Graphics();
        this._currencyDebug = new PIXI.Graphics();
        this._suffixDebug = new PIXI.Graphics();
        this._boundsDebug.beginFill(0x57F900, 0.5);
        // this._boundsDebug.drawRect(0, 0, this._config.bounds.width, this._config.bounds.height);
        this._boundsDebug.drawRect(0, 0, 1, 1);
        this._integerDebug.beginFill(0xC8002B, 0.5);
        // this._integerDebug.drawRect(this._moneyText.x, this._moneyText.y, this._moneyText.width, this._moneyText.height);
        this._integerDebug.drawRect(0, 0, 1, 1);
        this._decimalSymbolDebug.beginFill(0x0386F2, 0.5);
        // this._decimalSymbolDebug.drawRect(this._decimalSymbolText.x, this._decimalSymbolText.y, this._decimalSymbolText.width, this._decimalSymbolText.height);
        this._decimalSymbolDebug.drawRect(0, 0, 1, 1);
        this._decimalDebug.beginFill(0xD87D00, 0.5);
        // this._decimalDebug.drawRect(this._centsText.x, this._centsText.y, this._centsText.width, this._centsText.height);
        this._decimalDebug.drawRect(0, 0, 1, 1);
        this._currencyDebug.beginFill(0x9F0CF0, 0.5);
        // this._decimalDebug.drawRect(this._centsText.x, this._centsText.y, this._centsText.width, this._centsText.height);
        this._currencyDebug.drawRect(0, 0, 1, 1);
        this._suffixDebug.beginFill(0xFFFF00, 0.5);
        // this._decimalDebug.drawRect(this._centsText.x, this._centsText.y, this._centsText.width, this._centsText.height);
        this._suffixDebug.drawRect(0, 0, 1, 1);
        this.addChild(this._boundsDebug, this._integerDebug, this._decimalSymbolDebug, this._decimalDebug, this._currencyDebug, this._suffixDebug);
    };
    MoneyText.prototype.positionText = function () {
        var moneyFontProps = PIXI.TextMetrics.measureText(this._moneyText.text, this._moneyText.style);
        var decimalSymbolFontProps = PIXI.TextMetrics.measureText(this._decimalSymbolText.text, this._decimalSymbolText.style);
        var centsFontProps = PIXI.TextMetrics.measureText(this._centsText.text, this._centsText.style);
        var currencyFontProps = PIXI.TextMetrics.measureText(this._currencyText.text, this._currencyText.style);
        var suffixFontProps = PIXI.TextMetrics.measureText(this._suffix.text, this._suffix.style);
        var w = this._lastCalculatedWidths;
        if (this._config.bounds.height == 0)
            this._moneyText.y = 0;
        else
            this._moneyText.y = (this._config.bounds.height - this._moneyText.height) / 2;
        this._suffix.y = this._moneyText.y + moneyFontProps.fontProperties.ascent - suffixFontProps.fontProperties.ascent;
        if (this._config.diamond) {
            this._diamondImage.y = Math.round(this._moneyText.y + this._moneyText.height / 2);
        }
        if (this._config.align == "left") {
            this._moneyText.x = 0;
            if (this._config.diamond) {
                this._diamondImage.x = Math.round(this._diamondImage.width / 2);
                this._moneyText.x += this._diamondImage.width + this._config.spaceBetweenMoneyAndDiamond;
            }
        }
        else if (this._config.align == "right") {
            if (this._config.diamond) {
                this._moneyText.x = this._config.bounds.width - w.integer;
                this._diamondImage.x = Math.round(this._moneyText.x - this._config.spaceBetweenMoneyAndDiamond -
                    this._diamondImage.width / 2);
            }
            else {
                this._moneyText.x = this._config.bounds.width - (w.integer + w.decimal + w.decimalSymbol + w.currency + w.suffix);
            }
        }
        else {
            if (this._config.diamond) {
                this._moneyText.x = (this._config.bounds.width - w.integer) / 2;
                if (!this._config.centerAlignOnlyMoney)
                    this._moneyText.x += (this._diamondImage.width + this._config.spaceBetweenMoneyAndDiamond) / 2;
                this._diamondImage.x = Math.round(this._moneyText.x - this._diamondImage.width / 2 -
                    this._config.spaceBetweenMoneyAndDiamond);
            }
            else {
                this._moneyText.x = (this._config.bounds.width - this.textWidth) / 2;
                if (this._config.centerAlignOnlyMoney && !this._config.isShowingCredits)
                    this._moneyText.x += w.currency / 2;
            }
        }
        this._suffix.x = this._moneyText.x + w.integer;
        if (this._centsText.visible) {
            this._decimalSymbolText.x = this._moneyText.x + w.integer;
            this._decimalSymbolText.y = this._moneyText.y;
            this._centsText.x = this._decimalSymbolText.x + w.decimalSymbol + this._config.spaceBetweenMoneyAndCents;
            this._centsText.y = this._moneyText.y + moneyFontProps.fontProperties.ascent - centsFontProps.fontProperties.ascent;
            this._suffix.x = this._centsText.x + w.decimalText;
            this._suffix.y = this._centsText.y + centsFontProps.fontProperties.ascent - suffixFontProps.fontProperties.ascent;
        }
        if (this.isCurrencyVisible) {
            if (this._centsText.visible) {
                this._currencyText.x = this._centsText.x + w.decimal;
            }
            else {
                this._currencyText.x = this._moneyText.x + w.integer;
            }
            this._currencyText.y = this._moneyText.style.strokeThickness / 2 - this._currencyText.style.strokeThickness / 2 +
                this._moneyText.y + moneyFontProps.fontProperties.ascent - currencyFontProps.fontProperties.ascent;
            this._suffix.x = this._currencyText.x + w.currency;
            this._suffix.y = this._currencyText.y + currencyFontProps.fontProperties.ascent - suffixFontProps.fontProperties.ascent;
        }
    };
    Object.defineProperty(MoneyText.prototype, "Y", {
        get: function () {
            return this._moneyText.y;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "textWidth", {
        get: function () {
            var widths = this.calculateWidths();
            return widths.integer + widths.decimal + widths.decimalSymbol + widths.currency + widths.suffix;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "isCurrencyVisible", {
        get: function () {
            return !!(this._config.shouldShowCurrency && !this._config.isShowingCredits && this._currencyText.text.length);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "isSuffixVisible", {
        get: function () {
            return !!this._config.suffix.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyText.prototype, "isDiamondVisible", {
        get: function () {
            return !!this._config.diamond;
        },
        enumerable: true,
        configurable: true
    });
    MoneyText.prototype.calculateWidths = function () {
        this.calculateFontProperty();
        var config = this._config;
        var integer = this._moneyText;
        var decimal = this._centsText;
        var decimalSymbol = this._decimalSymbolText;
        var suffix = this._suffix;
        var isDecimalVisible = decimal.visible;
        var isSuffixVisible = this.isSuffixVisible;
        var isCurrencyVisible = this.isCurrencyVisible;
        var isDiamondVisible = this.isDiamondVisible;
        var integerTextWidth = integer.context.measureText(integer.text).width;
        var decimalTextWidth = 0;
        var decimalSymbolTextWidth = 0;
        var suffixTextWidth = 0;
        var integerWidth = integerTextWidth + integer.style.strokeThickness;
        var decimalWidth = 0;
        var decimalSymbolWidth = 0;
        var suffixWidth = 0;
        var currencyWidth = 0;
        if (isDecimalVisible) {
            decimalTextWidth = decimal.context.measureText(decimal.text).width;
            decimalSymbolTextWidth = decimalSymbol.context.measureText(decimalSymbol.text).width;
            decimalWidth = decimalTextWidth + decimal.style.strokeThickness;
            decimalSymbolWidth = decimalSymbolTextWidth + decimalSymbol.style.strokeThickness;
        }
        if (isSuffixVisible) {
            suffixTextWidth = suffix.context.measureText(suffix.text).width;
            suffixWidth = suffixTextWidth + suffix.style.strokeThickness;
        }
        if (isDiamondVisible) {
            currencyWidth = this._diamondImage.width;
        }
        else if (isCurrencyVisible) {
            currencyWidth = this._currencyText.context.measureText(this._currencyText.text).width;
        }
        this._lastCalculatedWidths = {
            integer: integerWidth,
            integerText: integerTextWidth,
            decimal: decimalWidth,
            decimalText: decimalTextWidth,
            decimalSymbol: decimalSymbolWidth,
            decimalSymbolText: decimalSymbolTextWidth,
            currency: currencyWidth,
            suffix: suffixWidth,
            suffixText: suffixTextWidth
        };
        return this._lastCalculatedWidths;
    };
    MoneyText.prototype.dispose = function () {
        this._moneyText.destroy();
        this._decimalSymbolText.destroy();
        this._centsText.destroy();
        this._currencyText.destroy();
        // this.destroy();
    };
    MoneyText.prototype.resetFontSizes = function () {
        this._moneyText.style.fontSize = this._config.moneyFontSize;
        this._moneyText.style = this._moneyText.style;
        this._decimalSymbolText.style.fontSize = this._config.moneyFontSize;
        this._decimalSymbolText.style = this._decimalSymbolText.style;
        this._centsText.style.fontSize = this._config.centsFontSize;
        this._centsText.style = this._centsText.style;
        this._currencyText.style.fontSize = this._config.currencyFontSize;
        this._currencyText.style = this._currencyText.style;
        this._suffix.style.fontSize = this._config.suffixFontSize;
        this._suffix.style = this._suffix.style;
        this.calculateFontProperty();
    };
    MoneyText.prototype.fitToBounds = function () {
        this.resetFontSizes();
        if (this._config.diamond)
            this.resizeDiamond();
        if (!this._config.shouldFitToBounds) {
            this.calculateWidths();
            return;
        }
        // TODO: implement scaling of all 3 fields
        var currencyCompensation = 0;
        if (this._config.centerAlignOnlyMoney && this._config.align == "center") {
            currencyCompensation = this._config.diamond ? this._diamondImage.width + this._config.spaceBetweenMoneyAndDiamond :
                this._currencyText.context.measureText(this._currencyText.text).width;
        }
        var moneyFontSize = this._moneyText.style.fontSize;
        var centsFontSize = this._centsText.style.fontSize;
        var currencyFontSize = this._currencyText.style.fontSize;
        var suffixFontSize = this._suffix.style.fontSize;
        var integerStrokeSize = this._moneyText.style.strokeThickness;
        var decimalStrokeSize = this._centsText.style.strokeThickness;
        var integerStrokeRation = integerStrokeSize ? moneyFontSize / integerStrokeSize : 0;
        var decimalStrokeRation = decimalStrokeSize ? centsFontSize / decimalStrokeSize : 0;
        while (this.textWidth > this._config.bounds.width - currencyCompensation && moneyFontSize > this._config.minFontSize) {
            var coefCurrency = this._config.diamond ? this._config.diamondScaleCoef : currencyFontSize / moneyFontSize;
            var coefCents = centsFontSize / moneyFontSize;
            var coefSuffix = suffixFontSize / moneyFontSize;
            moneyFontSize--;
            centsFontSize = moneyFontSize * coefCents;
            currencyFontSize = moneyFontSize * coefCurrency;
            suffixFontSize = moneyFontSize * coefSuffix;
            this._moneyText.style.fontSize = moneyFontSize;
            this._decimalSymbolText.style.fontSize = moneyFontSize;
            this._centsText.style.fontSize = centsFontSize;
            if (integerStrokeRation) {
                this._moneyText.style.strokeThickness = moneyFontSize / integerStrokeRation;
                this._decimalSymbolText.style.strokeThickness = moneyFontSize / integerStrokeRation;
            }
            if (decimalStrokeRation) {
                this._centsText.style.strokeThickness = centsFontSize / decimalStrokeRation;
            }
            this._moneyText.style = this._moneyText.style;
            this._decimalSymbolText.style = this._decimalSymbolText.style;
            this._centsText.style = this._centsText.style;
            this.calculateFontProperty();
            if (this._config.diamond && !this._config.keepDiamondSize) {
                this.resizeDiamond();
            }
            else if (!this._config.keepCurrencySize) {
                this._currencyText.style.fontSize = currencyFontSize;
                this._currencyText.style = this._currencyText.style;
            }
            if (!this._config.keepSuffixSize) {
                this._suffix.style.fontSize = suffixFontSize;
                this._suffix.style = this._suffix.style;
            }
        }
    };
    MoneyText = __decorate([
        log.decorator
    ], MoneyText);
    return MoneyText;
}(PIXI.Container));
exports.MoneyText = MoneyText;
var MoneyTextConfig = /** @class */ (function (_super) {
    __extends(MoneyTextConfig, _super);
    function MoneyTextConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.money = 0;
        _this.prefix = "";
        _this.suffix = "";
        _this.decimalSymbol = ".";
        _this.denomination = 1;
        _this.currency = "";
        _this.centsVisibilityMoneyLimit = -1;
        _this.isShowingCredits = false;
        _this.fontFamily = "Arial"; // TODO: font family should be provided in the TextStyle not in the config
        _this.align = "left";
        _this.moneyFontSize = 24;
        _this.centsFontSize = undefined;
        _this.currencyFontSize = undefined;
        _this.suffixFontSize = undefined;
        _this.spaceBetweenMoneyAndCents = 0;
        _this.spaceBetweenMoneyAndDiamond = 5;
        _this.shouldShowCurrency = true;
        _this.centerAlignOnlyMoney = false;
        _this.shouldHideWhenMoneyValueIsZero = false;
        _this.minFontSize = 6;
        _this.bounds = { width: 0, height: 0 };
        _this.shouldFitToBounds = true;
        _this.debug = false;
        _this.diamond = null;
        _this.diamondScaleCoef = 1;
        _this.posDiamondY = 0;
        _this.diamondPlay = false;
        _this.keepCurrencySize = false;
        _this.keepDiamondSize = false;
        _this.shouldHaveSpaceBetweenThousands = true;
        _this.hideCents = false;
        _this.keepSuffixSize = false;
        _this.letterSpacing = 0;
        return _this;
    }
    MoneyTextConfig.prototype.serialize = function (obj) {
        _super.prototype.serialize.call(this, obj);
        if (this.centsFontSize == undefined)
            this.centsFontSize = this.moneyFontSize;
        if (this.currencyFontSize == undefined)
            this.currencyFontSize = this.moneyFontSize;
        if (this.suffixFontSize == undefined)
            this.suffixFontSize = this.moneyFontSize;
    };
    Object.defineProperty(MoneyTextConfig.prototype, "moneyString", {
        get: function () {
            var str = "";
            if (this.isShowingCredits) {
                str = Math.floor(this.money / this.denomination).toString();
            }
            else if (this.diamondPlay) {
                str = (this.money / 100).toFixed();
            }
            else {
                str = (this.money / 100).toFixed(2);
                var moneyParts = str.split(".");
                var moneyDigits = moneyParts[0].split('');
                if (this.shouldHaveSpaceBetweenThousands) {
                    var counter = 3;
                    var digitIndex = moneyDigits.length;
                    while (--digitIndex > 0) {
                        counter--;
                        if (counter == 0) {
                            counter = 3;
                            moneyDigits.splice(digitIndex, 0, " ");
                        }
                    }
                }
                str = moneyDigits.join('');
            }
            if (this.prefix)
                str = this.prefix + str;
            return str;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyTextConfig.prototype, "centsString", {
        get: function () {
            var str = "";
            if (this.shouldCentsHaveValue) {
                str = (this.money / 100).toFixed(2).split(".")[1];
            }
            return str;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoneyTextConfig.prototype, "shouldCentsHaveValue", {
        get: function () {
            return !this.isShowingCredits && !this.diamondPlay && !this.hideCents &&
                (this.money <= this.centsVisibilityMoneyLimit || this.centsVisibilityMoneyLimit < 0);
        },
        enumerable: true,
        configurable: true
    });
    return MoneyTextConfig;
}(Serializer_1.Serializer));


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Represents a Textfield wrapper that can be fitted to given bounds.
 */
var Serializer_1 = __webpack_require__(6);
var Text = /** @class */ (function (_super) {
    __extends(Text, _super);
    function Text(config, text, style) {
        var _this = _super.call(this) || this;
        _this._config = new TextConfig();
        _this._config.serialize(config);
        _this._text = new PIXI.Text(text, style);
        _this._originalFontSize = _this._text.style.fontSize;
        _this.drawDebugRect();
        _this.addChild(_this._text);
        _this.fitToBounds();
        _this.positionText();
        return _this;
    }
    Object.defineProperty(Text.prototype, "text", {
        get: function () {
            return this._text.text;
        },
        /**
         * Sets the text of the textfield. Repositions and fits to bounds (if given)
         * @param {string} value
         */
        set: function (value) {
            if (value != this._text.text) {
                this._text.text = value;
                this.fitToBounds();
                this.positionText();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Text.prototype, "config", {
        /**
         * Serializes config and recalculates text.
         * @param conf Object with config params
         */
        set: function (conf) {
            this._config.serialize(conf);
            this.drawDebugRect();
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Text.prototype, "style", {
        get: function () {
            return this._text.style;
        },
        set: function (value) {
            if (value.fontSize == undefined)
                value.fontSize = this._originalFontSize;
            this._text.style = value;
            this._originalFontSize = this._text.style.fontSize;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Text.prototype, "align", {
        //sample comment check
        set: function (value) {
            this._config.align = value;
            this.fitToBounds();
            this.positionText();
        },
        enumerable: true,
        configurable: true
    });
    Text.prototype.dispose = function () {
        this._text.destroy();
        if (this._rect) {
            this._rect.destroy();
        }
        this.destroy();
    };
    /**
     * Fits text depending on given bounds and font
     *
     */
    Text.prototype.fitToBounds = function () {
        if (!this._config.shouldScale)
            return;
        this._text.style.fontSize = this._originalFontSize;
        this._text.style = this._text.style;
        var fontSize = this._text.style.fontSize;
        while (this._text.width > this._config.bounds.width &&
            fontSize > this._config.minFontSize) {
            fontSize--;
            this._text.style.fontSize = fontSize;
            this._text.style = this._text.style;
        }
    };
    /**
     * Positions text depending on style
     */
    Text.prototype.positionText = function () {
        //sample comment check
        if (this._config.align == "left")
            this._text.x = 0;
        else if (this._config.align == "center")
            this._text.x = Math.round((this._config.bounds.width - this._text.width) / 2);
        else if (this._config.align == "right")
            this._text.x = Math.round(this._config.bounds.width - this._text.width);
        if (this._config.verticalAlign === "middle") {
            this._text.y = Math.round((this._config.bounds.height - this._text.height) / 2);
        }
        else if (this._config.verticalAlign === "top") {
            this._text.y = 0;
        }
        else if (this._config.verticalAlign === "bottom") {
            this._text.y = Math.round(this._config.bounds.height - this._text.height);
        }
    };
    /**
     * Draws semi-transparent rectangle with the width of the container of the text
     */
    Text.prototype.drawDebugRect = function () {
        if (this._rect)
            this._rect.destroy();
        if (this._config.debug) {
            this._rect = new PIXI.Graphics();
            this._rect.beginFill(0xff0000, 0.3);
            this._rect.drawRect(0, 0, this._config.bounds.width, this._config.bounds.height);
            this._rect.endFill();
            this.addChild(this._rect);
        }
    };
    return Text;
}(PIXI.Container));
exports.Text = Text;
var TextConfig = /** @class */ (function (_super) {
    __extends(TextConfig, _super);
    function TextConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * Used when calculating text. Final text will not expand beyond the bounds
         * @type {{width: number; height: number}}
         */
        _this.bounds = { width: 100, height: 50 };
        _this.align = "left";
        _this.verticalAlign = "middle";
        /**
         * Same as shouldFitToBounds. True if bounds should be respected
         * @type {boolean}
         */
        _this.shouldScale = false; // TODO: rename this to shouldFitToBounds
        _this.minFontSize = 4;
        /**
         * Draws debug rectangle that shows text's container bounds.
         * @type {boolean}
         */
        _this.debug = false;
        return _this;
    }
    return TextConfig;
}(Serializer_1.Serializer));
exports.TextConfig = TextConfig;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Button_1 = __webpack_require__(16);
var StateButton = /** @class */ (function (_super) {
    __extends(StateButton, _super);
    function StateButton(states, initialState) {
        var _this = _super.call(this, states[initialState]) || this;
        _this._texturesState = initialState;
        _this._states = states;
        return _this;
    }
    StateButton.prototype.state = function (state) {
        if (state == undefined)
            return this._texturesState;
        this._texturesState = state;
        this.textures = this._states[state];
        this.setImageForState(Button_1.Button.STATE_UP);
    };
    return StateButton;
}(Button_1.Button));
exports.StateButton = StateButton;


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var RollingNumberView = /** @class */ (function (_super) {
    __extends(RollingNumberView, _super);
    function RollingNumberView(config) {
        var _this = _super.call(this) || this;
        _this._finalCentValue = _this._finalValue = 0;
        _this._currValue = 0;
        _this._finalValue = 0;
        _this._delta = 0;
        _this._direction = 0;
        _this._oldDirection = 0;
        _this._timePerCent = 0;
        _this._dummyObject = { value: 0 };
        _this._digits = [];
        _this._digitTweens = [];
        _this.config = config;
        return _this;
    }
    RollingNumberView.prototype.isAnimating = function () {
        var len = this._digitTweens.length;
        for (var i = 0; i < len; i++) {
            if (this._digitTweens[i]) {
                return true;
            }
        }
        if (this._failSafeTween) {
            return true;
        }
        return false;
    };
    RollingNumberView.prototype.denomination = function (value) {
        this._config.denomination = value;
        this.currency(this._config.currency);
    };
    RollingNumberView.prototype.killTweens = function () {
        var len = this._digitTweens.length;
        for (var i = 0; i < len; i++) {
            if (this._digitTweens[i]) {
                this._digitTweens[i].kill();
            }
            this._digitTweens[i] = null;
        }
        if (this._failSafeTween) {
            this._failSafeTween.kill();
        }
        this._failSafeTween = null;
    };
    RollingNumberView.prototype.currency = function (val) {
        this._config.currency = val;
        this.killTweens();
        this.createNumbers();
        this.setValue(this._finalCentValue);
    };
    RollingNumberView.prototype.setBounds = function (value) {
        this._config.bounds = value;
        this.killTweens();
        this.createNumbers();
        this.createMask();
        this.setFinalValue();
    };
    Object.defineProperty(RollingNumberView.prototype, "config", {
        set: function (value) {
            this._config = value;
            if (!this._config.separatorSize) {
                this._config.separatorSize = 8;
            }
            if (this._config.topPadding === undefined) {
                this._config.topPadding = 0;
            }
            if (this._config.denomination === undefined) {
                this._config.denomination = 1;
            }
            if (this._config.horizontalSpacing === undefined) {
                this._config.horizontalSpacing = 0;
            }
            this.killTweens();
            this.createNumbers();
            this.createMask();
            this.setFinalValue();
        },
        enumerable: true,
        configurable: true
    });
    RollingNumberView.prototype.setValue = function (value, animating, time) {
        if (time === void 0) { time = 0; }
        this._finalCentValue = value;
        if (this._config.isDiamondPlay) {
            value = Math.floor(value / 100);
        }
        else if (!this._config.currency) {
            value = Math.floor(value / this._config.denomination);
        }
        else if (this._config.numberOfCents === 0) {
            value = Math.floor(value / 100);
        }
        this._finalValue = value;
        this._totalTime = time;
        if (animating) {
            this._delta = this._currValue - value;
            if (this._delta === 0) {
                return;
            }
            var prevDir = this._direction;
            this._direction = this._delta < 0 ? -1 : 1;
            this._oldDirection = this._direction;
            this._timePerCent = Math.abs(time / this._delta);
            var anim = this.isAnimating();
            if (anim) {
                if (prevDir !== this._direction) {
                    this._oldDirection = prevDir;
                }
                if (this._failSafeTween) {
                    this._failSafeTween.kill();
                    this._failSafeTween = null;
                    this._finalValue = this._dummyObject.value;
                    this.setFinalValue();
                    this._finalValue = value;
                    this.animateDigit(0);
                }
            }
            else {
                this.animateDigit(0);
            }
        }
        else {
            var anim = this.isAnimating();
            if (anim) {
                gsap.TweenMax.to(this, 0.3, {
                    alpha: 0,
                    ease: gsap.Linear.easeNone,
                    onComplete: this.onFadeOutComplete.bind(this),
                });
            }
            else {
                this.setFinalValue();
            }
        }
    };
    RollingNumberView.prototype.complete = function () {
        if (this.isAnimating) {
            gsap.TweenMax.to(this, 0.3, { alpha: 0, ease: gsap.Linear.easeNone, onComplete: this.onFadeOutComplete.bind(this) });
        }
    };
    RollingNumberView.prototype.createMask = function () {
        if (this._storedMask != null) {
            this.removeChild(this._storedMask);
        }
        this._storedMask = new PIXI.Graphics();
        this._storedMask.beginFill(0xff0000, 0.5);
        this._storedMask.drawRect(0, 0, this._config.bounds.width, this._config.bounds.height);
        this._storedMask.endFill();
        this.addChild(this._storedMask);
        if (!this._config.debug) {
            this.mask = this._storedMask;
        }
    };
    RollingNumberView.prototype.animateDigit = function (index) {
        if (index === this._digits.length) {
            return;
        }
        if (this._digitTweens[index]) {
            this._digitTweens[index].totalTime(this._digitTweens[index].totalDuration());
        }
        // TODO: should be < 0.020 test it
        if (this._timePerCent < 0.04 || this._direction === 1) {
            this._dummyObject.value = this._currValue;
            this._dummyObject.finalValue = this._finalValue;
            this._failSafeTween = gsap.TweenMax.to(this._dummyObject, this._totalTime, {
                value: this._finalValue, ease: gsap.Linear.easeNone, roundProps: ["value"],
                onUpdate: this.onFailSafeTweenUpdate, onUpdateScope: this, onCompleteScope: this, onComplete: function () {
                    this._failSafeTween = null;
                },
            });
            return;
        }
        var digitSprite = this._digits[index];
        var currDigit = Number(digitSprite.children[0].text);
        var nextDigit = currDigit - this._direction;
        if (this._direction === -1) {
            if (nextDigit > 9) {
                nextDigit = 0;
            }
            digitSprite.children[1].text = nextDigit.toString();
            digitSprite.visible = true;
        }
        else {
            // TODO: reverse animation should be checked
            if (nextDigit < 0) {
                nextDigit = 9;
            }
            digitSprite.y = -this._config.bounds.height;
            digitSprite.children[1].text = currDigit.toString();
            digitSprite.children[0].text = nextDigit.toString();
        }
        if ((this._direction === -1 && currDigit === 9) || (this._direction === 1 && currDigit === 0)) {
            this.animateDigit(index + 1);
        }
        this._digitTweens[index] = gsap.TweenMax.to(digitSprite, this._timePerCent, {
            y: digitSprite.y + this._config.bounds.height * this._direction,
            ease: gsap.Linear.easeNone,
            roundProps: ["y"],
            onComplete: this.onCentAnimationComplete,
            callbackScope: this,
            onCompleteParams: [index],
        });
    };
    RollingNumberView.prototype.setFinalValue = function () {
        this._currValue = this._finalValue;
        var value = this._finalValue;
        var len = this._digits.length;
        if (value > Math.pow(10, len) - 1) {
            value = value % Math.pow(10, len);
        }
        var strValue = value.toString();
        var strLen = strValue.length;
        for (var i = len - 1; i >= 0; i--) {
            this._digits[i].y = this._startPosition;
            this._digits[i].visible = true;
            if (i >= strLen) {
                this._digits[i].children[0].text = "0";
                // TODO is it needed
                if (!this._config.currency || i > this._config.numberOfCents) {
                    this._digits[i].visible = false;
                }
            }
            else {
                this._digits[i].children[0].text = strValue.charAt(strLen - 1 - i);
            }
        }
        this.alpha = 1;
    };
    RollingNumberView.prototype.onCentAnimationComplete = function (index) {
        this._digitTweens[index] = null;
        var digitSprite = this._digits[index];
        if (this._oldDirection === -1) {
            digitSprite.y = this._startPosition;
            digitSprite.children[0].text = digitSprite.children[1].text;
        }
        if (index === 0) {
            this._currValue = this._currValue - this._oldDirection;
            if (this._oldDirection === 1) {
                // let limit = this._config.currency && !this._config.isDiamondPlay ?
                // this._config.numberofCents+1 : 1;
                var limit = this._config.currency ? this._config.numberofCents + 1 : 1;
                for (var i = this._digits.length - 1; i >= limit; i--) {
                    if (!this._digits[i].visible) {
                        continue;
                    }
                    // if (parseInt((this._digits[i].children[0] as PIXI.Text).text) == 0) {
                    //     this._digits[i].visible = false;
                    //     break;
                    // } else {
                    //     break;
                    // }
                }
            }
            this._oldDirection = this._direction;
            if (this._currValue !== this._finalValue) {
                this.animateDigit(0);
            }
        }
    };
    RollingNumberView.prototype.onFailSafeTweenUpdate = function () {
        this._finalValue = this._dummyObject.value;
        this.setFinalValue();
    };
    RollingNumberView.prototype.onFadeOutComplete = function () {
        if (this._failSafeTween) {
            this._finalValue = this._dummyObject.finalValue;
        }
        this.killTweens();
        this.setFinalValue();
    };
    RollingNumberView.prototype.createNumbers = function () {
        var digitsLength = this._digits.length;
        for (var i = 0; i < digitsLength; i++) {
            this.removeChild(this._digits[i]);
        }
        this._digits.length = 0;
        if (this._dotTextField != null) {
            this.removeChild(this._dotTextField);
            this._dotTextField = null;
        }
        this._config.style = {
            align: 'left',
            fill: this._config.color ? this._config.color : 0xffffff,
            fontFamily: this._config.font ? this._config.font : "Arial",
            fontSize: this._config.size ? this._config.size : 23,
            fontWeight: this._config.bold ? this._config.bold : 'bolder',
        };
        if (this._config.currency && this._config.numberOfCents != 0) {
            this._dotTextField = new PIXI.Text(".", this._config.style);
        }
        var textField = new PIXI.Text("0", this._config.style);
        this._startPosition = (this._config.bounds.height - textField.height) / 2 + this._config.topPadding;
        var count = this._config.digitsCount;
        var singleDigitWidth = textField.width + this._config.horizontalSpacing;
        var dotSymbolWidth = this._dotTextField != null ? this._dotTextField.width + this._config.horizontalSpacing : 0;
        var properCountOfNumbers = count - (this._config.currency ? this._config.numberOfCents : 0);
        var numberOfSeparators = Math.floor((properCountOfNumbers - 1) / 3);
        var totalSeparatorsWidth = this._config.currency ? numberOfSeparators * this._config.separatorSize : 0;
        var calculatedWidth = count * singleDigitWidth + (totalSeparatorsWidth - this._config.horizontalSpacing) + dotSymbolWidth;
        var startX = this._config.bounds.width - calculatedWidth;
        var sepCounter = properCountOfNumbers;
        var sepCount = 0;
        for (var i = 0; i < properCountOfNumbers; i++) {
            var cont = new PIXI.Container();
            for (var j = 0; j < 2; j++) {
                var txt = new PIXI.Text(j.toString(), this._config.style);
                cont.addChild(txt);
                if (j === 1) {
                    txt.position.y = this._config.bounds.height;
                }
            }
            if (sepCounter % 3 === 0 && i != 0 && this._config.currency) {
                sepCount++;
            }
            cont.position.x = startX + i * singleDigitWidth + sepCount * this._config.separatorSize;
            this._digits.unshift(cont);
            this.addChild(cont);
            sepCounter--;
        }
        if (this._dotTextField) {
            this._dotTextField.x = startX + properCountOfNumbers * singleDigitWidth + sepCount * this._config.separatorSize;
            this._dotTextField.y = this._startPosition;
            this.addChild(this._dotTextField);
            for (var i = 0; i < this._config.numberOfCents; i++) {
                var cont = new PIXI.Container();
                for (var j = 0; j < 2; j++) {
                    var txt = new PIXI.Text(j.toString(), this._config.style);
                    cont.addChild(txt);
                    if (j === 1) {
                        txt.position.y = this._config.bounds.height;
                    }
                }
                cont.position.x = startX + properCountOfNumbers * singleDigitWidth + i * singleDigitWidth +
                    dotSymbolWidth + sepCount * this._config.separatorSize;
                this.addChild(cont);
                this._digits.unshift(cont);
            }
        }
    };
    return RollingNumberView;
}(PIXI.Container));
exports.RollingNumberView = RollingNumberView;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var RollingBitmapNumberView = /** @class */ (function (_super) {
    __extends(RollingBitmapNumberView, _super);
    function RollingBitmapNumberView(config) {
        var _this = _super.call(this) || this;
        _this._config = config;
        _this._finalCentValue = _this._finalValue = 0;
        _this._currValue = 0;
        _this._finalValue = 0;
        _this._delta = 0;
        _this._direction = 0;
        _this._oldDirection = 0;
        _this._timePerCent = 0;
        _this._dummyObject = { value: 0 };
        if (!_this._config.separatorSize) {
            _this._config.separatorSize = 8;
        }
        if (_this._config.topPadding === undefined) {
            _this._config.topPadding = 0;
        }
        if (_this._config.denomination === undefined) {
            _this._config.denomination = 1;
        }
        if (_this._config.horizontalSpacing === undefined) {
            _this._config.horizontalSpacing = 0;
        }
        _this._currency = _this._config.currency;
        _this._digits = [];
        _this._digitTweens = [];
        _this.createNumbers();
        _this.createMask();
        return _this;
    }
    RollingBitmapNumberView.prototype.isAnimating = function () {
        var len = this._digitTweens.length;
        for (var i = 0; i < len; i++) {
            if (this._digitTweens[i]) {
                return true;
            }
        }
        if (this._failSafeTween) {
            return true;
        }
        return false;
    };
    RollingBitmapNumberView.prototype.denomination = function (value) {
        this._config.denomination = value;
        this.currency(this._config.currency);
    };
    RollingBitmapNumberView.prototype.killTweens = function () {
        var len = this._digitTweens.length;
        for (var i = 0; i < len; i++) {
            if (this._digitTweens[i]) {
                this._digitTweens[i].kill();
            }
            this._digitTweens[i] = null;
        }
        if (this._failSafeTween) {
            this._failSafeTween.kill();
        }
        this._failSafeTween = null;
    };
    RollingBitmapNumberView.prototype.currency = function (val) {
        this._config.currency = val;
        this.killTweens();
        this.createNumbers();
        this.setValue(this._finalCentValue);
    };
    RollingBitmapNumberView.prototype.setBounds = function (value) {
        this._config.bounds = { width: value.width, height: value.height };
        this.killTweens();
        this.createNumbers();
        this.createMask();
        this.setFinalValue();
    };
    RollingBitmapNumberView.prototype.setValue = function (value, animating, time) {
        if (time === void 0) { time = 0; }
        this._finalCentValue = value;
        if (this._config.isDiamondPlay) {
            value = Math.floor(value / 100);
        }
        else if (!this._config.currency) {
            value = Math.floor(value / this._config.denomination);
        }
        else if (this._config.numberOfCents === 0) {
            value = Math.floor(value / 100);
        }
        this._finalValue = value;
        this._totalTime = time;
        if (animating) {
            this._delta = this._currValue - value;
            if (this._delta === 0) {
                return;
            }
            var prevDir = this._direction;
            this._direction = this._delta < 0 ? -1 : 1;
            this._oldDirection = this._direction;
            this._timePerCent = Math.abs(time / this._delta);
            var anim = this.isAnimating();
            if (anim) {
                if (prevDir !== this._direction) {
                    this._oldDirection = prevDir;
                }
                if (this._failSafeTween) {
                    this._failSafeTween.kill();
                    this._failSafeTween = null;
                    this._finalValue = this._dummyObject.value;
                    this.setFinalValue();
                    this._finalValue = value;
                    this.animateDigit(0);
                }
            }
            else {
                this.animateDigit(0);
            }
        }
        else {
            var anim = this.isAnimating();
            if (anim) {
                gsap.TweenMax.to(this, 0.3, {
                    alpha: 0,
                    ease: gsap.Linear.easeNone,
                    onComplete: this.onFadeOutComplete.bind(this),
                });
            }
            else {
                this.setFinalValue();
            }
        }
    };
    RollingBitmapNumberView.prototype.complete = function () {
        if (this.isAnimating) {
            gsap.TweenMax.to(this, 0.3, { alpha: 0, ease: gsap.Linear.easeNone, onComplete: this.onFadeOutComplete.bind(this) });
        }
    };
    RollingBitmapNumberView.prototype.createMask = function () {
        if (this._storedMask == null) {
            this._storedMask = new PIXI.Graphics();
            this.addChild(this._storedMask);
            if (!this._config.debug) {
                this.mask = this._storedMask;
            }
        }
        this._storedMask.clear();
        this._storedMask.beginFill(0xff0000, 0.5);
        this._storedMask.drawRect(0, 0, this._config.bounds.width, this._config.bounds.height);
        this._storedMask.endFill();
    };
    RollingBitmapNumberView.prototype.animateDigit = function (index) {
        if (index === this._digits.length) {
            return;
        }
        if (this._digitTweens[index]) {
            this._digitTweens[index].totalTime(this._digitTweens[index].totalDuration());
        }
        // TODO: should be < 0.020 test it
        if (this._timePerCent < 0.04 || this._direction === 1) {
            this._dummyObject.value = this._currValue;
            this._dummyObject.finalValue = this._finalValue;
            this._failSafeTween = gsap.TweenMax.to(this._dummyObject, this._totalTime, {
                value: this._finalValue, ease: gsap.Linear.easeNone, roundProps: ["value"],
                onUpdate: this.onFailSafeTweenUpdate, onUpdateScope: this, onCompleteScope: this, onComplete: function () {
                    this._failSafeTween = null;
                },
            });
            return;
        }
        var digitSprite = this._digits[index];
        var currDigit = Number(digitSprite.children[0].text);
        var nextDigit = currDigit - this._direction;
        if (this._direction === -1) {
            if (nextDigit > 9) {
                nextDigit = 0;
            }
            digitSprite.children[1].text = nextDigit.toString();
            digitSprite.visible = true;
        }
        else {
            // TODO: reverse animation should be checked
            if (nextDigit < 0) {
                nextDigit = 9;
            }
            digitSprite.y = -this._config.bounds.height;
            digitSprite.children[1].text = currDigit.toString();
            digitSprite.children[0].text = nextDigit.toString();
        }
        if ((this._direction === -1 && currDigit === 9) || (this._direction === 1 && currDigit === 0)) {
            this.animateDigit(index + 1);
        }
        this._digitTweens[index] = gsap.TweenMax.to(digitSprite, this._timePerCent, {
            y: digitSprite.y + this._config.bounds.height * this._direction,
            ease: gsap.Linear.easeNone,
            roundProps: ["y"],
            onComplete: this.onCentAnimationComplete,
            callbackScope: this,
            onCompleteParams: [index],
        });
    };
    RollingBitmapNumberView.prototype.setFinalValue = function () {
        this._currValue = this._finalValue;
        var value = this._finalValue;
        var len = this._digits.length;
        if (value > Math.pow(10, len) - 1) {
            value = value % Math.pow(10, len);
        }
        var strValue = value.toString();
        var strLen = strValue.length;
        for (var i = len - 1; i >= 0; i--) {
            this._digits[i].y = this._startPosition;
            this._digits[i].visible = true;
            if (i >= strLen) {
                this._digits[i].children[0].text = "0";
                // TODO is it needed
                if (!this._config.currency || i > this._config.numberOfCents) {
                    this._digits[i].visible = false;
                }
            }
            else {
                this._digits[i].children[0].text = strValue.charAt(strLen - 1 - i);
            }
        }
        this.alpha = 1;
    };
    RollingBitmapNumberView.prototype.onCentAnimationComplete = function (index) {
        this._digitTweens[index] = null;
        var digitSprite = this._digits[index];
        if (this._oldDirection === -1) {
            digitSprite.y = this._startPosition;
            digitSprite.children[0].text = digitSprite.children[1].text;
        }
        if (index === 0) {
            this._currValue = this._currValue - this._oldDirection;
            if (this._oldDirection === 1) {
                // let limit = this._config.currency && !this._config.isDiamondPlay ?
                // this._config.numberofCents+1 : 1;
                var limit = this._config.currency ? this._config.numberofCents + 1 : 1;
                for (var i = this._digits.length - 1; i >= limit; i--) {
                    if (!this._digits[i].visible) {
                        continue;
                    }
                    // if (parseInt((this._digits[i].children[0] as PIXI.Text).text) == 0) {
                    //     this._digits[i].visible = false;
                    //     break;
                    // } else {
                    //     break;
                    // }
                }
            }
            this._oldDirection = this._direction;
            if (this._currValue !== this._finalValue) {
                this.animateDigit(0);
            }
        }
    };
    RollingBitmapNumberView.prototype.onFailSafeTweenUpdate = function () {
        this._finalValue = this._dummyObject.value;
        this.setFinalValue();
    };
    RollingBitmapNumberView.prototype.onFadeOutComplete = function () {
        if (this._failSafeTween) {
            this._finalValue = this._dummyObject.finalValue;
        }
        this.killTweens();
        this.setFinalValue();
    };
    RollingBitmapNumberView.prototype.createNumbers = function () {
        var digitsLength = this._digits.length;
        for (var i = 0; i < digitsLength; i++) {
            this.removeChild(this._digits[i]);
        }
        this._digits.length = 0;
        if (this._dotTextField != null) {
            this.removeChild(this._dotTextField);
            this._dotTextField = null;
        }
        if (this._config.currency && this._config.numberOfCents != 0) {
            this._dotTextField = new PIXI.extras.BitmapText(".", this._config.style);
        }
        var textField = new PIXI.extras.BitmapText("0", this._config.style);
        this._startPosition = Math.round((this._config.bounds.height - textField.textHeight) / 2 + this._config.topPadding);
        var count = this._config.digitsCount;
        var singleDigitWidth = textField.textWidth + this._config.horizontalSpacing;
        var dotSymbolWidth = this._dotTextField != null ? this._dotTextField.textWidth + this._config.horizontalSpacing : 0;
        var properCountOfNumbers = count - (this._config.currency ? this._config.numberOfCents : 0);
        var numberOfSeparators = Math.floor((properCountOfNumbers - 1) / 3);
        var totalSeparatorsWidth = this._config.currency ? numberOfSeparators * this._config.separatorSize : 0;
        var calculatedWidth = count * singleDigitWidth + (totalSeparatorsWidth - this._config.horizontalSpacing) + dotSymbolWidth;
        var startX = this._config.bounds.width - calculatedWidth;
        var sepCounter = properCountOfNumbers;
        var sepCount = 0;
        for (var i = 0; i < properCountOfNumbers; i++) {
            var cont = new PIXI.Container();
            for (var j = 0; j < 2; j++) {
                var txt = new PIXI.extras.BitmapText(j.toString(), this._config.style);
                cont.addChild(txt);
                if (j === 1) {
                    txt.position.y = this._config.bounds.height;
                }
            }
            if (sepCounter % 3 === 0 && i != 0 && this._config.currency) {
                sepCount++;
            }
            cont.position.x = startX + i * singleDigitWidth + sepCount * this._config.separatorSize;
            cont.position.x = Math.round(cont.position.x);
            this._digits.unshift(cont);
            this.addChild(cont);
            sepCounter--;
        }
        if (this._dotTextField) {
            this._dotTextField.x = Math.round(startX + properCountOfNumbers * singleDigitWidth + sepCount * this._config.separatorSize);
            this._dotTextField.y = this._startPosition;
            this.addChild(this._dotTextField);
            for (var i = 0; i < this._config.numberOfCents; i++) {
                var cont = new PIXI.Container();
                for (var j = 0; j < 2; j++) {
                    var txt = new PIXI.extras.BitmapText(j.toString(), this._config.style);
                    cont.addChild(txt);
                    if (j === 1) {
                        txt.position.y = this._config.bounds.height;
                    }
                }
                cont.position.x = startX + properCountOfNumbers * singleDigitWidth + i * singleDigitWidth +
                    dotSymbolWidth + sepCount * this._config.separatorSize;
                this.addChild(cont);
                this._digits.unshift(cont);
                cont.position.x = Math.round(cont.position.x);
            }
        }
    };
    return RollingBitmapNumberView;
}(PIXI.Container));
exports.RollingBitmapNumberView = RollingBitmapNumberView;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Device_1 = __webpack_require__(2);
var ScrollView = /** @class */ (function (_super) {
    __extends(ScrollView, _super);
    function ScrollView() {
        var _this = _super.call(this) || this;
        _this._posY = 0;
        _this._lastPosY = 0;
        _this._startPosY = 0;
        _this._startTouchPosY = 0;
        _this._scrollSpeed = 0;
        _this.createMask(0, 0);
        _this.on(Device_1.Device.resolveEvent("mousedown"), _this.onMouseDown, _this);
        _this.on(Device_1.Device.resolveEvent("mousemove"), _this.onMouseMove, _this);
        _this.on(Device_1.Device.resolveEvent("mouseup"), _this.onMouseUp, _this);
        _this.interactive = true;
        PIXI.ticker.shared.add(_this.animateScroll, _this);
        return _this;
    }
    Object.defineProperty(ScrollView.prototype, "contentView", {
        get: function () {
            return this._contentView;
        },
        set: function (view) {
            if (this._contentView != view) {
                if (this._contentView != null)
                    this.removeChild(this._contentView);
                this._contentView = view;
                if (this._contentView != null)
                    this.addChild(this._contentView);
            }
        },
        enumerable: true,
        configurable: true
    });
    ScrollView.prototype.setSize = function (width, height) {
        this.createMask(width, height);
    };
    ScrollView.prototype.dispose = function () {
        this._stopAnimation = true;
        this.off(Device_1.Device.resolveEvent("mousedown"), this.onMouseDown, this);
        this.off(Device_1.Device.resolveEvent("mousemove"), this.onMouseMove, this);
        this.off(Device_1.Device.resolveEvent("mouseup"), this.onMouseUp, this);
        PIXI.ticker.shared.remove(this.animateScroll, this);
    };
    ScrollView.prototype.createMask = function (width, height) {
        if (this._maskSize == null)
            this._maskSize = new PIXI.Point();
        this._maskSize.set(width, height);
        var mask;
        if (this.mask instanceof PIXI.Graphics)
            mask = this.mask;
        else
            mask = new PIXI.Graphics();
        mask.clear();
        mask.beginFill(0xff0000);
        mask.drawRect(0, 0, width, height);
        mask.endFill();
        this.mask = mask;
    };
    ScrollView.prototype.animateScroll = function () {
        if (this._stopAnimation)
            return;
        if (!this._touchDown && this._contentView) {
            this._scrollSpeed *= ScrollView.DECAY;
            var bouncing = 0;
            var y = this._posY;
            // calculate a bouncing when the text moves over the canvas size
            if (y > 0) {
                bouncing = -y * ScrollView.BOUNCING_SPRINGESS;
                this._scrollSpeed *= ScrollView.BOUNCING_DECAY;
            }
            else if (y + this._contentView.height < this._maskSize.y) {
                bouncing = (this._maskSize.y - this._contentView.height - y) *
                    ScrollView.BOUNCING_SPRINGESS;
                this._scrollSpeed *= ScrollView.BOUNCING_DECAY;
            }
            this._posY = y + this._scrollSpeed + bouncing;
            this._contentView.position.set(0, this._posY);
        }
    };
    ScrollView.prototype.onMouseDown = function (event) {
        if (!this._contentView || this._contentView.height <= this._maskSize.y)
            return;
        this._scrollSpeed = 0;
        this._touchDown = true;
        var coords = event.data.getLocalPosition(this);
        this._lastPosY = coords.y;
        this._startPosY = this._posY;
        this._startTouchPosY = coords.y;
    };
    ScrollView.prototype.onMouseMove = function (event) {
        if (this._touchDown) {
            var coords = event.data.getLocalPosition(this);
            var decay = 1;
            var y = this._startPosY + (coords.y - this._startTouchPosY);
            if (y > 0 || y + this._contentView.height < this._maskSize.y)
                decay = ScrollView.DRAG_DECAY;
            this._posY = this._posY + (coords.y - this._lastPosY) * decay;
            this._scrollSpeed = ((coords.y - this._lastPosY) * ScrollView.SPEED_SPRINGNESS);
            this._lastPosY = coords.y;
            this._contentView.position.set(0, this._posY);
        }
    };
    ScrollView.prototype.onMouseUp = function (event) {
        this._touchDown = false;
    };
    ScrollView.DECAY = 0.97;
    ScrollView.BOUNCING_DECAY = 0.8;
    ScrollView.SPEED_SPRINGNESS = 0.8;
    ScrollView.BOUNCING_SPRINGESS = 0.15;
    ScrollView.DRAG_DECAY = 0.4;
    return ScrollView;
}(PIXI.Container));
exports.ScrollView = ScrollView;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(43);
var Group = PIXI.display.Group;
var Layer = PIXI.display.Layer;
var layerGroup = /** @class */ (function () {
    function layerGroup() {
    }
    return layerGroup;
}());
var LayerManagerClass = /** @class */ (function () {
    function LayerManagerClass() {
        this.groups = [];
        this.groupIndex = 0;
    }
    LayerManagerClass.prototype.findGroupByName = function (groupName) {
        var len = this.groups.length;
        for (var i = 0; i < len; i++) {
            if (this.groups[i].name === groupName) {
                return this.groups[i];
            }
        }
        return null;
    };
    Object.defineProperty(LayerManagerClass, "Instance", {
        get: function () {
            return this.instance || (this.instance = new this());
        },
        enumerable: true,
        configurable: true
    });
    LayerManagerClass.prototype.getLayer = function (groupName) {
        var lGroup = this.findGroupByName(groupName);
        if (lGroup) {
            return this.findGroupByName(groupName).layer;
        }
        return null;
    };
    Object.defineProperty(LayerManagerClass.prototype, "stage", {
        get: function () {
            return this._stage;
        },
        set: function (stage) {
            this._stage = stage;
        },
        enumerable: true,
        configurable: true
    });
    LayerManagerClass.prototype.createGroups = function () {
        var _this = this;
        var groups = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            groups[_i] = arguments[_i];
        }
        groups.forEach(function (group) {
            _this.addGroup(group);
        });
    };
    LayerManagerClass.prototype.addGroup = function (groupName, sorting) {
        if (!this._stage) {
            throw new Error('Stage not set!');
        }
        var lGroup = new layerGroup();
        if (this.findGroupByName(groupName)) {
            if (false) {}
            return;
        }
        lGroup.index = this.groupIndex;
        lGroup.name = groupName;
        lGroup.pixiGroup = new Group(this.groupIndex, sorting);
        lGroup.layer = new Layer(lGroup.pixiGroup);
        this._stage.addChild(lGroup.layer);
        this.groups.push(lGroup);
        this.groupIndex++;
    };
    /**
     *
     * @param dObject
     * @param groupName
     * @param recursive
     */
    LayerManagerClass.prototype.addToGroup = function (dObject, groupName, recursive) {
        var _this = this;
        if (recursive === void 0) { recursive = false; }
        var group;
        if (typeof groupName === 'string') {
            group = this.findGroupByName(groupName);
        }
        else {
            group = groupName;
        }
        if (!group) {
            if (false) {}
            return;
        }
        if (recursive && typeof dObject.children !== 'undefined') {
            dObject.children.forEach(function (dObject) {
                _this.addToGroup(dObject, group);
            });
        }
        dObject.parentGroup = group.pixiGroup;
    };
    LayerManagerClass.prototype.addToParentGroup = function (dObject) {
        var group;
        group = dObject.parent.parentGroup;
        if (!group) {
            if (false) {}
            return;
        }
        dObject.parentGroup = group;
    };
    LayerManagerClass.prototype.setGroupSorting = function (groupName, sorting) {
        var group;
        if (typeof groupName === 'string') {
            group = this.findGroupByName(groupName);
        }
        else {
            group = groupName;
        }
        if (!group) {
            if (false) {}
            return;
        }
        group.pixiGroup.enableSort = !!sorting;
        if (typeof sorting === 'function') {
            group.pixiGroup.on('sort', sorting);
        }
    };
    return LayerManagerClass;
}());
var LayerManager = LayerManagerClass.Instance;
exports.LayerManager = LayerManager;


/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__43__;

/***/ })
/******/ ]);
});
//# sourceMappingURL=egt-library.js.map